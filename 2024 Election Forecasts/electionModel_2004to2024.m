%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Program for simulating the original SDE and ODE models introduced in 
%%% "Forecasting elections using compartmental
%%% models of infection" by Alexandria Volkening, Daniel F. Linder,
%%% Mason A. Porter, and Grzegorz A. Rempala (SIAM Review, 2020,
%%% https://epubs.siam.org/doi/10.1137/19M1306658), adapted and extended
%%% for the 2020-2024 elections, as well as the 2004, 2008, and 2014
%%% elections in "How pollster history and time affect U.S.
%%% election forecasts under a compartmental modeling approach" by Ryan
%%% Branstetter, Samuel Chian, Joseph Cromp, William L. He, Christopher M.
%%% Lee, Mangqi Liu, Emma Mansell, Manas Paranjape, Thanmaya Pattanashetty,
%%% Alexia Rodrigues, and Alexandria Volkening, 2024, in prep.
%%%
%%% Original authors (for SIAM Review 2020 article): Alexandria 
%%% Volkening, Daniel F. Linder, Mason A. Porter, and Grzegorz A. Rempala
%%%
%%% 2024 authors: Joe Cromp, Thanmaya Pattanashay, Alexia Rodrigues,
%%% and Alexandria Volkening
%%%
%%% 2022 authors: Ryan Branstetter, Mengqi Liu, Manas Paranjape, and
%%% Alexandria Volkening
%%%
%%% 2020 authors: Samuel Chian, William L. He, Christopher M. Lee,
%%% Alexandria Volkening, Daniel F. Linder, Mason A. Porter, and 
%%% Grzegorz A. Rempala
%%%
%%% Contact: avolkening@purdue.edu
%%%
%%% If you use this code, please cite the references above, as well as the
%%% GitLab repository
%%% https://gitlab.com/alexandriavolkening/forecasting-elections-using-compartmental-models
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Purpose: This code simulates elections using parameters fit to polling
%%% data (through parameterFitting2004to2024.R). 
%%% Note that this code relies on polling data and initial conditions 
%%% formatted with formattingPolls2004to2016.m or
%%% formattingPolls2018to2024.m, as well as parameters generated with
%%% parameterFitting2004to2024.R.
%%%
%%% Note: The most recent version of this program is streamlined to
%%% included all prior versions, as well as elections from 2004 to 2008 and
%%% 2014. First see "2024 Election Forecasts" as dicussed below for the
%%% most up-to-date versions of all files. We maintain the original folders
%%% to account for real-time forecasting.
%%%
%%% For 2012-2018 elections, see the "Original Programs" subfolder at 
%%% https://gitlab.com/alexandriavolkening/forecasting-elections-using-compartmental-models
%%% for the original remaining files and directions needed to run this code.
%%%
%%% For the 2020 elections, see the "2020 Election Forecasts" subfolder at
%%% https://gitlab.com/alexandriavolkening/forecasting-elections-using-compartmental-models
%%% for the original remaining files and directions needed to run this code.
%%%
%%% For the 2024 elections, see the "2024 Election Forecasts" subfolder at
%%% https://gitlab.com/alexandriavolkening/forecasting-elections-using-compartmental-models
%%% for the original remaining files and directions needed to run this code.
%%%
%%% Before running this program to simulate elections, first run the 
%%% appropriate formattingPolls and parameterFitting codes to generate the
%%% files (initial conditions, model parameters) called by this program. 
%%%
%%% Inputs:
%%% election: string containing the election year and 's','p', or 'g'
%%% depending on election type
%%% numMonths: a number between 7-11 indicating how many data points were
%%% used for parameter fitting per region. This should be the same value as
%%% output by the formattingPolls programs
%%% dateAndTime: the date of the forecast (should be the day of
%%% polling-data download, not the date of posting, in the case of
%%% real-time forecasting). This is only relevant for real-time forecasts.
%%% dateAndTime should be in the form 'mm-dd-yy'.
%%% biasOn: a string of either 'Bias' or '', depending on what model
%%% version one wants to run. We only use '' in our real time forecasts.
%%% Dem2024cond: a string of either 'BidenHarris' or '', depending on how
%%% one wants to handle the special case of Biden being replaced with
%%% Harris in the 2024 elections. In all other elections, Dem2024cond
%%% should always be ''.
%%%
%%% Outputs:
%%% a Results[...].mat file in ModelDataFiles
%%%
%%% For example, to simulate opinion dynamics during the 2020 president
%%% elections and produce a final forecast with our partisan-lean model,
%%% one would use:
%%% electionModel2004to2024('2020p',11,'11-02-20','Bias','')
%%% To simulate opinion dynamics during the 2024 president elections and
%%% produce a forecast on Aug 26 with our baseline model, one would
%%% use either:
%%% electionModel_2004to2024('2024p',9,'08-26-24','','BidenHarris')
%%% electionModel_2004to2024('2024p',9,'08-26-24','','')
%%% depending on how one's choice of how to account for the changed in
%%% candidates from Biden to Harris (see formattingPolls_2018to2024.m)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [] = electionModel_2004to2024(election, numMonths, dateAndTime, biasOn, Dem2024cond)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Preliminaries. Establishing folder and file names to save results, and
%%% setting some other simulation and model parameters
folderName = './ModelDataFiles';    % name of folder (within the current folder) where we will access data and save reults
dt = 0.1;                           % time step (in days) for our ODEs and SDEs
sigma = 0.0015;                     % noise strength in our SDEs
nrand = 10000;                      % number of simulations (set to 1 with sigma = 0.0015 to try out the code)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Specifying the simulation time. We assume 30-day months; simulating for
%%% 308 days corresponds to an election day of Nov. 8 based on our
%%% simplifications, for example.
if contains(election,'2024')
    time = 305;
elseif contains(election, '2022')
    time = 308;
elseif contains(election, '2020')
    time = 303;
elseif contains(election, '2018')
    time = 306;
elseif contains(election, '2016')
    time = 308;
elseif contains(election, '2014')
    time = 304;
elseif contains(election, '2012')
    time = 306;
elseif contains(election, '2008')
    time = 304;
elseif contains(election, '2004')
    time = 302;
else
    error('Please include 2004, 2008, 2012, 2014, 2016, 2018, 2020, 2022, or 2024 in your file name, to indicate the election year')
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Setting up election-specific variables: population sizes, and and 
%%% some demographic information (used to correlate noise in some
%%% settings). Running this code requires first running the programs to
%%% format data and fit parameters. We format the demographic data for use
%%% when correlating noise. See .xlsx files for data sources. Our naming
%%% conventions are different for past and real-time forecasting.
if contains(election,'2024')
    corrData = readtable([folderName '/dataset_correlations' Dem2024cond election '_' num2str(numMonths) '_' dateAndTime '.xlsx']);
else    % elections strictly before 2024
    corrData = readtable([folderName '/dataset_correlations' election '_' num2str(numMonths) '.xlsx']);
end
numM = size(corrData,1);
total = table2array(corrData(:,3)) + table2array(corrData(:,5));
HispanicPercent = table2array(corrData(:,5))./total;
NHBlackPercent = table2array(corrData(:,4))./total;
woCollegePercent = (100-table2array(corrData(:,6)))/100;

%%% Number of voters per state or superstate.
NN = table2array(corrData(:,7))';

%%% Total number of voters
N = sum(NN);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Initializations. Note that S + R + D = 1 for each state, so we track
%%% only the number of Republicans and undecided/other voters per state, as
%%% the remaining voters are Democratic.
numTimeSteps = time/dt;                    % number of time steps to simulate the model for

S = zeros(numTimeSteps+1,numM,nrand);      % Undecided/other voters
R = zeros(numTimeSteps+1,numM,nrand);      % Republican voters
mu = zeros(1,numM);                        % covariance for noise
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Initial conditions - these are copied from the first line of the
%%% .csv files (['RepPercentages' election '.csv'] and
%%% ['DemPercentages' election '.csv'] generated by the formatting poll
%%% data code. biasOn controls whether we access the results from our
%%% original methods or methods accounting for pollster bias.
if contains(election,'2024')
    formattedRepPolls = csvread([folderName '/RepPercentages' biasOn Dem2024cond election '_' num2str(numMonths) '_' dateAndTime '.csv'],0,1);
    formattedDemPolls = csvread([folderName '/DemPercentages' biasOn Dem2024cond election '_' num2str(numMonths) '_' dateAndTime '.csv'],0,1);
else    % elections strictly before 2024
    formattedRepPolls = csvread([folderName '/RepPercentages' biasOn election '_' num2str(numMonths) '.csv'],0,1);
    formattedDemPolls = csvread([folderName '/DemPercentages' biasOn election '_' num2str(numMonths) '.csv'],0,1);
end
RepIC = formattedRepPolls(1,:)/100;
DemIC = formattedDemPolls(1,:)/100;
for i = 1:nrand
    R(1,:,i) = RepIC;
    S(1,:,i) = 1 - RepIC - DemIC;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Specifying model parameters (must have run the parameter fitting
%%% program first). Same as above, biasOn is either empty or 'Bias',
%%% determining whether we access parameters fit using our original methods
%%% or after accounting for pollster bias.
if contains(election,'2024')
    numbers = csvread([folderName '/modelParameters' biasOn Dem2024cond election '_' num2str(numMonths) '_' dateAndTime '.csv'],1,1);
else    % elections strictly before 2024
    numbers = csvread([folderName '/modelParameters' biasOn election '_' num2str(numMonths) '.csv'],1,1);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Specifying our parameters. Note that we convert our parameters to units
%%% of 1/day assuming 30-day months.
gamma_R = numbers(numM*2+1,1:numM)/30;          % Republican turnover
gamma_D = numbers(numM*2+2,1:numM)/30;          % Democratic turnover
beta_R = numbers(1:numM,1:numM)/30;             % Republican transmission
beta_D = numbers(numM+1:numM*2,1:numM)/30;      % Democratic transmission
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Calculating the covariance to use for our noise. We correlate on the 
%%% fraction of Hispanic individuals, the fraction of Non-Hispanic Black 
%%% individuals, and the fraction of individuals without a college 
%%% education in each state.
J_H = zeros(numM,numM);
J_B = zeros(numM,numM);
J_E = zeros(numM,numM);

for i = 1:numM
    for j =1:numM
        J_H(i,j) = min(HispanicPercent(i),HispanicPercent(j))./(max(HispanicPercent(i),HispanicPercent(j)));
        J_B(i,j) = min(NHBlackPercent(i),NHBlackPercent(j))./(max(NHBlackPercent(i),NHBlackPercent(j)));
        J_E(i,j) = min(woCollegePercent(i),woCollegePercent(j))./(max(woCollegePercent(i),woCollegePercent(j)));
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Main for loop used to simulate nrand elections. We suggest nrand =
%%% 10000 unless errror checking
for randi = 1:nrand

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% For each simulated election, we choose one Jaccard index uniformly
    %%% at random to use the covariance of our noise.
    randomNum = rand(1);
    if randomNum <= 1/3
        covariance = J_E;
    elseif randomNum > 2/3
        covariance = J_B;
    else
        covariance = J_H;
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% This for loop simulates a single election. We use that
    %%% S + D + R = 1 for each state to reduce the number of equations to 
    %%% two per state.
    for time_step = 2:numTimeSteps+1

        %%% Correlated random noise
        dWt1 = sqrt(dt)*mvnrnd(mu,covariance);
        dWt2 = sqrt(dt)*mvnrnd(mu,covariance);

        %%% Uncorrelated random noise -- uncomment the two lines below to
        %%% simulate elections with uncorrelated random noise
        %dWt1 = sqrt(dt)*randn(1,numM);
        %dWt2 = sqrt(dt)*randn(1,numM);

        [rhsS,rhsR] = electionModelRHS(NN,N,S(time_step-1,:,randi),R(time_step-1,:,randi),gamma_R,gamma_D,beta_R,beta_D,numM);
        S(time_step,:,randi) = S(time_step-1,:,randi) + rhsS*dt + sigma*dWt1;
        R(time_step,:,randi) = R(time_step-1,:,randi) + rhsR*dt + sigma*dWt2;

    end     %%% end of for loop simulating one election
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end     %%% end of main for loop simulating nrand elections
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Final processing to produce some images and report results in
%%% visualizeElectionForecasts_2004to2024.m

%%% First we calculate our marginForecast on election day (the percentage 
%%% point margin lead of Republicans over Democrats) for each state or 
%%% superstate per simulation. Note that this is negative for a given state
%%% if a Democrat wins, and positive if a Republican wins (0 indicates a
%%% tie).
marginForecast = squeeze((R(numTimeSteps+1,:,:)- 1 + S(numTimeSteps+1,:,:) + R(numTimeSteps+1,:,:))*100);

%%% Here we calculate, for each state or superstate, the fraction of
%%% simulated elections in which the Republican candidate won. This is
%%% used to categorize states as "Likely Rep.", "Lean Dem.", etc.
chanceRWinState = zeros(1,numM);
chanceRTie = zeros(1,numM);
if nrand > 1
    for i = 1:numM
        
        %%% Placeholder
        temporary = marginForecast(i,:);
        
        %%% Number of simulations in which the Republican candidate won
        %%% (note you need to divide by nrand to express this as a
        %%% percent).
        chanceRWinState(i) = sum(temporary > 0);
        
        %%% Number of simulations in which the Republican and Democrat
        %%% candidates tied.
        chanceRTie(i) = sum(temporary == 0);
        
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Saving data and simulation conditions. Note that we only save the final
%%% election results rather than the whole trajectory of opinion dynamics.
finalR = squeeze(R(time/dt+1,:,:));
finalS = squeeze(S(time/dt+1,:,:));
meanR = mean(R,3);
meanD = mean(1-R-S,3);
stdR = std(R,[],3);
stdD = std(1-R-S,[],3);
if contains(election,'2024')
    save([folderName '/Results' biasOn Dem2024cond election '_' num2str(numMonths) '_' dateAndTime '.mat'], 'finalR','finalS','sigma','nrand','dt','time', 'chanceRWinState','chanceRTie',...
    'meanR', 'meanD','stdR','stdD','numMonths','dateAndTime');
else    % elections strictly before 2024
    save([folderName '/Results' biasOn election '_' num2str(numMonths) '.mat'], 'finalR','finalS','sigma','nrand','dt','time', 'chanceRWinState','chanceRTie',...
        'meanR', 'meanD','stdR','stdD','numMonths','dateAndTime');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


end     %%% end of main function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Helper funciton holding the righthand side of our model differential
%%% equations
function [rhsS,rhsR] = electionModelRHS(NN,N,S,R,gamma_R,gamma_D,beta_R,beta_D,numM)
    rhsS = zeros(1,numM);
    rhsR = zeros(1,numM);
    
    for i = 1:numM
        rhsS(i) = gamma_R(i)*R(i) + gamma_D(i)*(1-S(i)-R(i)) -...
            (S(i)/N)*(NN.*R)*beta_R(1:numM,i) -...
            (S(i)/N)*(NN.*(1-S-R))*beta_D(1:numM,i);
        rhsR(i) = -gamma_R(i)*R(i) + (S(i)/N)*(NN.*R)*beta_R(1:numM,i);
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


