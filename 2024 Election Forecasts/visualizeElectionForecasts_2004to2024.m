%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Program for creating visualizations of our presidential, senatorial,
%%% and gubernatorial forecasts for elections from 2004 to 2024. See 
%%% https://c-r-u-d.gitlab.io/2024/ for 2024 U.S. forecasts that use this
%%% code.
%%%
%%% Contact person: Alexandria Volkening (avolkening@purdue.edu)
%%%
%%% 2024 Authors: Joseph Cromp, Thanmaya Pattanashetty, Alexia Rodrigues,
%%% and Alexandria Volkening
%%%
%%% 2022 Authors: Ryan Branstetter, Mengqi Liu, Manas Paranjape, and
%%% Alexandria Volkening
%%%
%%% 2020 Authors: Samuel Chian, William L. He, Christopher M. Lee, and
%%% Alexandria Volkening
%%%
%%% Command used to visualize our election forecasts:
%%% visualizeElectionForecasts(folderName, election, date)
%%%
%%% Input:
%%% election: string with election year; s, g, or p for election type; and
%%% _<numMonths> where numMonths is an integer (expected to be between 6
%%% and 11, inclusive) indicating the number of months of averaged polling
%%% data that we used for parameter fitting.
%%% date: string of the form 'mm-dd-yy', only used for real-time forecasts,
%%% corresponding the date of data download (this is the date associated
%%% with all of the data files)
%%% postDate: string of the form 'mmm d', only used for real-time
%%% forecasting, indicating the date of posting the forecast on our
%%% website. This is likely different than date, the date of data download.
%%% An example would be 'Aug 26'
%%% biasOn: 'Bias' or '', a string indicating the model version run
%%% Dem2024cond: 'BidenHarris' or '', a string that specifies our choice of
%%% how to handle Biden being replaced by Harris, only relevant for the
%%% 2024 presidential elections
%%%
%%% Output:
%%% Visualizations of our election forecasts
%%%
%%% To run:
%%% For example, to generate images for the 2024 presidential elections
%%% with the BidenHarris condition and our baseline model on Aug 26, use "
%%% visualizeElectionForecasts_2004to2024('2024p_9', '08-26-24', '', 'BidenHarris', 'Aug 26')
%%% As another example, to generate forecast images of the 2022 senatorial
%%% elections using our partisan-lead model on the eve of the election,
%%% use:
%%% visualizeElectionForecasts_2004to2024('2022s_11', '', 'Bias', '','')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [] = visualizeElectionForecasts_2004to2024(election, date, biasOn, Dem2024cond, postDate)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Images (and, in the case of real-time forecasting, alt-text files) will
%%% be saved in the folder
mkdir('ModelOutput')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Confirming that biasOn and Dem2024cond have been input as expected
if ~strcmp(biasOn,'Bias') && ~isempty(biasOn)
    error('biasOn must be the string Bias or an empty string. This specifies what version of the model to use.')
end
if ~isempty(Dem2024cond) && ~contains(election, '2024p')
    error('The special Biden Harris condition only applies for the 2024 presidential election. Please input Dem2024cond as an empty string.')
elseif ~strcmp(Dem2024cond,'BidenHarris') && ~isempty(Dem2024cond) && contains(pollsFile, 'presPolls2024')
    error('Dem2024cond should be either the string BidenHarris or an empty string for the 2024 presidential election. For all other elections, it should be an empty string.')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Some initial processing for figure titles
%%% The election type is either p, s, or g (for presidential, senatorial
%%% or gubernatorial)
electionType = election(5);
if strcmp(biasOn, 'Bias') == 1
    biasName = ' using our adapted model';
else
    biasName = '';
end
if strcmp(Dem2024cond, 'BidenHarris')
    Dem2024condName = ' (Biden or Harris polls)';
else
    Dem2024condName = ' (Harris polls only)';
end
if strcmp(electionType, 'p') == 1           % presidential
    if contains(election,'2024') == 1
        electionTitle = [postDate ' forecast of the ' election(1:4) ' presidential election' biasName Dem2024condName];
    else
        electionTitle = ['Forecast of the ' election(1:4) ' presidential election' biasName];
    end
    %%% Accessing the states and superstates that we forecast for this
    %%% election and, as appropriate, their electoral votes.
    electionData = readtable(['./ModelDataFiles/dataset_correlations' biasOn Dem2024cond election '_' date '.xlsx']);
elseif strcmp(electionType, 's') == 1       % senatorial
    if contains(election,'2024')
        electionTitle = [postDate ' forecast of the ' election(1:4) ' senatorial elections' biasName];
    else 
        electionTitle = ['Forecast of the ' election(1:4) ' senatorial elections' biasName];
    end
    %%% Accessing the states and superstates that we forecast for this
    %%% election and, as appropriate, their electoral votes.
    electionData = readtable(['./ModelDataFiles/dataset_correlations' biasOn election '_' date '.xlsx']);
elseif strcmp(electionType, 'g') == 1       % gubernatorial
    if contains(election,'2024')
        electionTitle = [postDate ' forecast of the ' election(1:4) ' gubernatorial elections' biasName];
    else
        electionTitle = ['Forecast of the ' election(1:4) ' gubernatorial elections' biasName];
    end
    %%% Accessing the states and superstates that we forecast for this
    %%% election and, as appropriate, their electoral votes.
    electionData = readtable(['./ModelDataFiles/dataset_correlations' biasOn election '_' date '.xlsx']);
else
    error('Please make sure that your election variable is correct.')
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Loading results (note that finalR is an nrand-long matrix
%%% containing the final Republican fractions that we forecast;
%%% finalS is an nrand-long matrix containing the final other fractions
%%% that we forecast; nrand is the number of simulated elections,
%%% usually 10,000; and you can find the Democratic fractions by using
%%% that finalD = 1 - finalR - finalS)
if contains(election,'2024')
    load(['./ModelDataFiles/Results' biasOn Dem2024cond election '_' date '.mat'],'finalR','finalS')
else
    load(['./ModelDataFiles/Results' biasOn election '.mat'],'finalR','finalS')
end

%%% Pulling out the specific data that is useful for image processing
stateNames = table2cell(electionData(:,1));
stateAbbrev = table2cell(electionData(:,2));
if strcmp(electionType, 'p')
    %%% For presidential elections, we pull out the electoral votes per
    %%% state or superstate
    stateElectoralVotes = table2array(electionData(:,8));
end
numM = length(stateAbbrev);      % number of regions forecast
%%% Adding spaces into the state names as appropriate and designating
%%% special elections with an asterisk
stateNames = strrep(stateNames, 'DistrictOfColumbia', 'District of Columbia');
stateNames = strrep(stateNames, 'NewHampshire', 'New Hampshire');
stateNames = strrep(stateNames, 'NorthDakota', 'North Dakota');
stateNames = strrep(stateNames, 'NewJersey', 'New Jersey');
stateNames = strrep(stateNames, 'NewMexico', 'New Mexico');
stateNames = strrep(stateNames, 'NewYork', 'New York');
stateNames = strrep(stateNames, 'NorthCarolina', 'North Carolina');
stateNames = strrep(stateNames, 'SouthCarolina', 'South Carolina');
stateNames = strrep(stateNames, 'SouthDakota', 'South Dakota');
stateNames = strrep(stateNames, 'RhodeIsland', 'Rhode Island');
stateNames = strrep(stateNames, 'WestVirginia', 'West Virginia');
stateNames = strrep(stateNames, 'Special', '*');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Calculating the endpoints of the range in which the middle 80 percent
%%% of our simulate elections lie, as well as our forecast vote margins
percent10 = prctile((finalR - 1 + finalS + finalR)*100, 10, 2);
percent90 = prctile((finalR - 1 + finalS + finalR)*100, 90, 2);
finalD = 1 - finalR - finalS;
modelForecast = 100*mean(finalR - finalD,2);        % vote margin is positive if a Republican victory and negative if a Democratic victory
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Creating vote-margin bar graphs with 80% confidence intervals
set(groot,{'DefaultAxesXColor','DefaultAxesYColor','DefaultAxesZColor'},{'k','k','k'})
figure('units','normalized','outerposition',[0 0 1 1])
modelForecastDem = modelForecast;
modelForecastDem(modelForecast > 0) = 0;
modelForecastRep = modelForecast;
modelForecastRep(modelForecast < 0) = 0;
barh(1:numM, flipud(modelForecastRep), 'FaceColor', [236/255,65/255,37/255], 'EdgeColor','none');
bh.FaceColor = 'flat';
hold on
barh(1:numM, flipud(modelForecastDem), 'FaceColor',  [20/255,49/255,244/255], 'EdgeColor','none');
for i = 1:numM
    plot([percent10(i), percent90(i)],[numM-i+1, numM-i+1],'Color', [247/255 204/255 73/255], 'LineWidth',3)
    hold on
end
tickCutOff = ceil(max(abs([percent10;percent90]))/10)*10;
xlim([-tickCutOff tickCutOff])
xticks(-tickCutOff:10:tickCutOff)
title(electionTitle)
legend({'forecast (Rep. win)', 'forecast (Dem. win)', '80% of results'}, 'Location','southeast')
xlabel('margin lead by party (percentage points)')
ticklabels = get(gca,'XTickLabel');
xticklabels_new = cell(size(ticklabels));
for i = 1:(length(xticklabels)-1)/2
    xticklabels_new{i} = ['\color{blue} +' ticklabels{length(xticklabels)-i+1}];
    xticklabels_new{length(xticklabels)-i+1} = ['\color{red} +' ticklabels{length(xticklabels)-i+1}];
end
xticklabels_new{(length(xticklabels)-1)/2 + 1} = ['\color{black}' abs(ticklabels{(length(xticklabels)-1)/2 + 1})];
set(gca, 'XTickLabel', xticklabels_new);
yticks(1:1:numM)
yticklabels(flipud(stateNames))
set(gca,'FontSize',26,'FontName','Helvetica','LineWidth',2)
box('off')
saveas(gcf, ['./ModelOutput/voteMargins_' biasOn Dem2024cond election '-' postDate '.fig'])
saveas(gcf, ['./ModelOutput/voteMargins_' biasOn Dem2024cond election '-' postDate '.png'])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Creating an alt text file to accompany bar graphs during real-time
%%% forecasting (2024 in this case)
if contains(election,'2024')
    altText = "Description of bar graph diagram showing vote margins:";
    stateNames = strrep(stateNames, 'Red', 'the Red superstate');
    stateNames = strrep(stateNames, 'Blue', 'the Blue superstate');

    %%% Looping over regions forecast
    for i = 1:numM
        if modelForecast(i) < 0
            party = "Democratic";
        else
            party = "Republican";
        end

        %%% Adding a new line to altText (each line is a region forecast
        %%% followed by the party and vote margin). For example, we might say
        %%% "We forecast a Democratic outcome in California by a margin of 10
        %%% percentage points."
        altText = altText + newline + ...
            strcat("We forecast a ", party, " outcome in ", stateNames(i), ...
            " by a margin of ", string(abs(modelForecast(i))), " percentage points.");

    end %%% end of loop over regions forecast

    %%% Writing our alt text to a file
    fileID = fopen(['./ModelOutput/AltTextMargins' biasOn Dem2024cond election '-' postDate, '.txt'], 'w');
    fprintf(fileID, altText);
end     % end of if-statement on whether or not we are real-time forecasting
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Plotting distribution of electoral votes for presidential elections
if strcmp(electionType, 'p') == 1
    drawElectoralVotes(election, date, biasOn, biasName, Dem2024cond, Dem2024condName, postDate)
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% 2024 Senate only: Summarizing the chance of a Rep or Dem Senate win
if contains(election,'2024s')
    [chanceRepWinS, chanceDemWinS,chanceSplit] = SenateOutcomeDistribution(election, date, biasOn, biasName, postDate);
    disp(['The Democrats have a ' num2str(chanceDemWinS) '% chance of winning the Senate according to our model'])
    disp(['The Republicans have a ' num2str(chanceRepWinS) '% chance of winning the Senate according to our model'])
    disp(['There is a ' num2str(chanceSplit) '% of splitting the Senate according to our model'])
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



end     % end of main function



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Helper function for plotting the distribution of electoral votes in
%%% presidential elections, and, in the case of real-time forecasting of
%%% presidential elections (assumed 2024) accompanying alt-text files
function [] = drawElectoralVotes(election, dataDate, biasOn, biasName, Dem2024cond, Dem2024condName, postDate)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Loading model simulations
if contains(election,'2024')
    load(['./ModelDataFiles/Results' biasOn Dem2024cond election '_' dataDate],'finalR','finalS','nrand')
else
    load(['./ModelDataFiles/Results' biasOn election],'finalR','finalS','nrand')
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Accessing the electoral votes that each state or superstate has.
if contains(election,'2024')
    corrData = readtable(['./ModelDataFiles/dataset_correlations' Dem2024cond election '_' dataDate '.xlsx']);
else
    corrData = readtable(['./ModelDataFiles/dataset_correlations' election '_' dataDate '.xlsx']);
end
stateElectoralVotes = table2array(corrData(:,8));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Initializing an array for the number of simulations with a given
%%% result. Note that there are a max of 538 electoral votes possible,
%%% but we offset by 1 to account for EVs from 0 to 538. For example,
%%% electoralVotes(1) = number of simulations with 0 electoral votes
%%% for the Republican candidate, and electoralVotes(2) = number of
%%% simulations with 1 EV for the Republican candidate.
electoralVotes = zeros(539);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Loop through nrand (usually 10,000) simulated elections
for ii = 1:nrand
    
    %%% Obtaining the Rep and Dem votes for each simulation
    simulationR = finalR(:,ii);
    simulationD = 1 - finalR(:,ii) - finalS(:,ii);      % Since 1 = Dem + Rep + Other
    
    %%% Counting EVs when Rep wins greater percentage than Dem
    redVotes = sum((simulationR > simulationD) .* stateElectoralVotes);
    
    %%%% Incremementing counter for number of votes won. For example,
    %%%% if redVotes = 200, then we say that electoralVotes(201) should
    %%%% be increased by 1.
    electoralVotes(redVotes+1) = electoralVotes(redVotes+1) + 1;
end

%%% Dividing by the number of simulations and multipying by 100 to
%%% frame things as the percent of simulations with a given EV outcome.
electoralVotes = 100*electoralVotes./nrand;

%%% For graphing purposes, we break the electoral votes up into those
%%% for which the Republican candidate gets strictly less than 269 EVs
%%% (corresponding to a Dem victory) and those for which the Republican
%%% candidate gets strictly more than 269 EVs (corresponding to a Rep
%%% victory). We note that index 270 corresponds to 269 EVs in our
%%% matrix.
blueElectoralVotes = electoralVotes(1:269);         % Rep EVs associated with a Dem overall victory
redElectoralVotes = electoralVotes(271:539);        % Rep EVs associated with a Rep overall victory
tieElectoralVotes = electoralVotes(270);            % Rep EVs associatted with a tie

%%% Counting percentage of simulations in which a Rep won and the
%%% percentage of simulations in which a Dem won
chanceRepWin = sum(redElectoralVotes);
chanceDemWin = sum(blueElectoralVotes);
chanceTie = sum(tieElectoralVotes);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Creating distribution plot and, in the case of real-time forecasting,
%%% associated alt-text files
set(groot,{'DefaultAxesXColor','DefaultAxesYColor','DefaultAxesZColor'},{'k','k','k'})
figure('units','normalized','outerposition',[0 0 1 1])
bar(0:268,blueElectoralVotes,'EdgeColor','b','FaceColor','b','BarWidth',1)
hold on
bar(270:538,redElectoralVotes,'EdgeColor','r','FaceColor','r','BarWidth',1)
bar(269,tieElectoralVotes,'EdgeColor',[123 123 123]/255,'FaceColor',[123 123 123]/255,'BarWidth',1)
legend({['Democratic win: ' char(string(round(chanceDemWin,2))) '%'], ...
    ['Republican win: ' char(string(round(chanceRepWin,2))) '%']...
    ['Tie: ' char(string(round(chanceTie,2))) '%']})
title([postDate ': electoral-vote distribution' biasName Dem2024condName])
xlabel('number of Republican electoral votes')
ylabel('chance of this electoral-vote outcome (%)')
set(gca,'FontSize',26,'FontName','Helvetica','LineWidth',2)
box('off')
saveas(gcf, ['./ModelOutput/electoralVotes_' biasOn election '_' postDate '.fig'])
saveas(gcf, ['./ModelOutput/electoralVotes_' biasOn election '_' postDate '.png'])

%%% Writing the associated alt text file
if contains(election,'2024p')
    altText = "Histogram showing the distribution of electoral votes:";
    altText = altText + newline + strcat("Probability that the Democratic candidate will win: ", string(round(chanceDemWin,2)), " percent.");
    altText = altText + newline + strcat("Probability that the Republican candidate will win: ", string(round(chanceRepWin,2)), " percent.");
    fileID = fopen(['./ModelOutput/AltTextPresDistribution_' biasOn Dem2024cond election '_' postDate '.txt'], 'w');
    fprintf(fileID, altText);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end     % end of helper function for plotting electoral vote distributions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% 2024 election: Helper function for producing the overall chances of
%%% Senate outcomes in 2024
function [RepControl,DemControl,SplitSenate] = SenateOutcomeDistribution(election, dateOfData, biasOn, biasName, postDate)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Reading in the file with model results and setting some variables
load(['./ModelDataFiles/Results' biasOn election '_' dateOfData],'finalR','finalS', 'nrand')
finalD = 1 - finalR - finalS;

%%% Accessing the states and superstates that we forecast for this
%%% election and their Senate seats
electionData = readtable(['./ModelDataFiles/dataset_correlations' biasOn election '_' dateOfData '.xlsx']);

%%% Pulling out the specific data that is useful for image processing
seats = cell2mat(table2cell(electionData(:,8)));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Specifying the seats that are not up for election in 2024
DemSeats = 28;     
RepSeats = 38;      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Computing number of individually forecast seats that are missing
%%% polling data and not forecast
numMissing = 100 - DemSeats - RepSeats - sum(seats);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Computing the Republican seats forecast per simulated election and the
%%% associated Democratic seats
missingVals = numMissing*randi([0,1],[1,nrand]);
RepSeatsSim = RepSeats + sum((finalD < finalR).*seats) + missingVals;
DemSeatsSim = DemSeats + sum((finalD > finalR).*seats) + numMissing*(1-missingVals);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Calculating chance of Rep control
RepControl = 100*(sum(RepSeatsSim > 50))/nrand;
DemControl = 100*(sum(DemSeatsSim > 50))/nrand;
SplitSenate = 100*(sum(RepSeatsSim == 50))/nrand;   % a Senate tie is broken by the presidential election result
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


end  % end of helper function for providing Senate seat outcomes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

