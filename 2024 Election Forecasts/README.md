Code and data for forecasting the 2024 U.S. presidential, senatorial, and gubernatorial elections, reproducing or building on the forecasts posted at https://c-r-u-d.gitlab.io/2024/.

This folder contains updated 2024-election code (with options for earlier elections) that implements the compartmental model introduced in "Forecasting elections using compartmental models of infection" by Alexandria Volkening, Daniel F. Linder, Mason A. Porter, and Grzegorz A. Rempala (2020), in _SIAM Review_, Vol. 62, No. 4: 837-865 (https://epubs.siam.org/doi/abs/10.1137/19M1306658) and includes new flexibility for different model versions. Our updated programs that apply these methods to the 2020 U.S. elections are with additional collaborators Samuel Chian, William L. He, and Christopher M. Lee. Our updated programs that apply these methods to the 2022 U.S. elections are with additional collaborators Ryan T. Bransteter, Mengqi Liu, and Manas Paranjape. Our updated programs that apply these methods to the 2024 U.S. elections and extend them are with Joseph Cromp, Thanmaya Pattanashetty, and Alexia Rodrigues.

For questions or comments, please contact Alexandria Volkening at avolkening@purdue.edu.

To reproduce our forecasts of the 2024 elections, follow these steps:

(1) Download the appropriate polling data set (gubernatorial, senatorial, or presidential) from 538 at https://projects.fivethirtyeight.com/polls/. 538 aggregates polling data and their data are publicly available in the repository https://github.com/fivethirtyeight/data/tree/master/polls. We gratefully acknowledge the 538 team for making these data accessible and publicly available. Save these data with the date of download: for example, if you download the data on September 1, 2024, save the presidential data as dataset_presPolls2024_09-01-24.xlsx in a folder called 'Data'.

(2) Format the polling data (sort the polls by state, take the mean of the polls by month, etc) to obtain one data point (representing the percent of people inclined to vote for a Democrat, inclined to vote for a Republican, and undecided/other) for each 30-day interval spanning backward from the election day. To format the polling data, for the 2024 senatorial elections with polling data downloaded on August 26, for example, use: 
[populations, regions, numMonths, numRegions] = formattingPolls_2018to2024('dataset_senPolls2024_08-26-24','08-26-24','','')
in Matlab. This produces one Excel document, containing the data we will use to correlate election outcomes later, and two csv files, containing the formatted polling data (all should be saved in a folder called 'ModelDataFiles', which you may need to create first).

(3) Run parameterFitting_2004to2024.R to determine our model parameters. First adjust time to the appropriate value (time equals numMonths output in step (2)) and date to the appropriate value (e.g., "08-26-24" in our example). Uncomment the appropriate lines in the code, depending on what election you are forecasting. During real-time forecasting, Replace Pop and stateNames with the values output in step (2) for the "populations" and "regions" variable as necessary. Replace numM with value of "numRegions" from the last step. The result of this step is one csv file, containing the model parameters, saved in the folder 'ModelDataFiles'.

(4) Run electionModel_2004to2024.m to simulate our model of election dynamics. For example, to forecast the gubernatorial elections on August 26, run:
electionModel_2004to2024('2024g',9,'08-26-24','','')
in Matlab. Change the final input to '2024p' or '2024s' to forecast the presidential or senatorial elections. This produces one .mat file containing the results of 10,000 simulated stochastic elections (saved in the folder 'ModelDataFiles').

(5) Run visualizeElectionForecasts_2004to2024.m to produce some images and alt text showing our forecasts (like those on our website here: https://c-r-u-d.gitlab.io/2024/). For example, to show forecasts of the gubernatorial elections in our current example, run:
visualizeElectionForecasts_2004to2024('2024g_9', '08-26-24', '', '', '')
in Matlab. The results will be saved in 'ModelOutput' (you may need to create this folder first). 

Steps (1)-(5) must be done in order. Note that Steps (1), (2), and (5) are quick (< 5 minutes). Step (3) can take between about 4 hours and about 3 days, depending on how many states are forecast individually. Step (4) takes a few hours. You many need to create the folders 'ModelOutput' and 'ModelDataFiles' in the folder containing the code that you download here, as these folders will be filled with results from steps (2)-(5).

See https://gitlab.com/alexandriavolkening/forecasting-elections-using-compartmental-models/-/tree/master/Original%20Programs for the programs that implement the same model to forecast the 2012-2018 U.S. elections, https://gitlab.com/alexandriavolkening/forecasting-elections-using-compartmental-models/-/tree/master/2020%20Election%20Forecasts for the programs that implement the model for the 2020 U.S. elections, and https://gitlab.com/alexandriavolkening/forecasting-elections-using-compartmental-models/-/tree/master/2022%20Election%20Forecasts for the associated programs for the 2022 U.S. elections.

