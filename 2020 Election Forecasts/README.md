Code and data for forecasting the 2020 U.S. presidential, senatorial, and gubernatorial elections, reproducing or building on the forecasts posted at https://modelingelectiondynamics.gitlab.io/2020-forecasts/index.html. 

This folder contains updated 2020-election code that implements the compartmental model introduced in "Forecasting elections using compartmental models of infection" by Alexandria Volkening, Daniel F. Linder, Mason A. Porter, and Grzegorz A. Rempala (2020), in _SIAM Review_, Vol. 62, No. 4: 837-865 (https://epubs.siam.org/doi/abs/10.1137/19M1306658). Our updated programs that apply these methods to the 2020 U.S. elections are with additional collaborators Samuel Chian, William L. He, and Christopher M. Lee.

For questions or comments, please contact Alexandria Volkening at alexandria.volkening@northwestern.edu.

To reproduce our forecasts of the 2020 elections, follow these steps:

(1) Download the appropriate polling data set (gubernatorial, senatorial, or presidential) from FiveThirtyEight at https://projects.fivethirtyeight.com/polls/. FiveThirtyEight aggregates polling data and their data is publicly available under a CC BY 4.0 license (https://creativecommons.org/licenses/by/4.0/). Save this data with the date of download: for example, if you download the data on 28 September 2020, save the data as gub_polls_2020-09-28.xlsx, sen_polls_2020-09-28.xlsx, or pres_polls_2020-09-28.xlsx.

(2) Format the polling data (filter out national polls, sort the polls by state, take the mean of the polls by month, etc) to obtain one data point (representing the percent of people inclined to vote for a Democrat, inclined to vote for a Republican, and undecided/other) for each 30-day interval spanning backward from the election day. To format the polling data, for the 2020 presidential elections with polling data downloaded on 28 September, for example, use: 
[populations, regions, numMonths] = formatting538polls_2020('pres_polls_2020-09-28','09/28/20')
in Matlab. This produces one Excel document, containing the data we will use to correlate election outcomes later, and two .csv files, containing the formatted polling data.

(3) Run parameterFitting2020.R to determine our model parameters. First adjust time (at line 44) to the appropriate value (time equals numMonths output in step (2)) and date (at line 46) to the appropriate value (e.g., "09-28" in our example). Uncomment the appropriate lines from line 63 to 85 in the code, depending on if you are forecasting the presidential, senatorial, or gubernatorial elections. Replace Pop and stateNames with the values output in step (2) for the "populations" and "regions" variable as necessary. Replace numM with the number of states in the "regions" variable. The result of this step is one .csv file, containing the model parameters.

(4) Run electionModel2020.m to simulate our model of election dynamics. First change numMonths at line 80 and dateAndTime at line 81 as necessary. (In our example, numMonths = 10 and dateAndTime = '09-28'). To forecast the presidential elections, run:
electionModel2020(303,10000,0.0015,'2020p')
in Matlab. Change the final input to '2020s' or '2020g' to forecast the senatorial or gubernatorial elections, respectively. This produces one .mat file containing the results of 10,000 simulated stochastic elections.

(5) Run visualizeElectionForecasts.m to produce some images showing our forecasts (like those on our website here: https://modelingelectiondynamics.gitlab.io/2020-forecasts/index.html). For example, to show forecasts of the presidential elections in our current example, run:
visualizeElectionForecasts('Elections', '2020p_10', '09-28')
in Matlab. 

Steps (1)-(5) must be done in order. Note that Steps (1), (2), and (5) are quick (< 5 minutes). Step (3) can take between about 8 hours and about 3 days, depending on how many states are forecast individually. Step (4) takes a few hours.

See https://gitlab.com/alexandriavolkening/forecasting-elections-using-compartmental-models/-/tree/master/Original%20Programs for the programs that implement the same model to forecast the 2012-2018 U.S. elections. 

