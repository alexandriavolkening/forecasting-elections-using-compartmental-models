%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Purpose: Graph the electoral votes won in each of the nrand simulations
%%% run for a given election and month. This image-processing program
%%% produces the electoral vote (EV) graphs at
%%% https://modelingelectiondynamics.gitlab.io/2020-forecasts/index.html
%%%
%%% Command used to generate electoral-vote distributions:
%%% drawElectoralVotes(folderName, election, date)
%%%
%%% Input:
%%% folderName is the name of the folder containing the simulation results,
%%% election is the election year and month,
%%% date is the date of data collection
%%%
%%% Output:
%%% A graph of the distribution of EVs for the Republican candidate based
%%% on nrand (usually 10,000) model simulations.
%%%
%%% To run, for example:
%%% drawElectoralVotes('Elections', '2020p_10', '09-21')
%%%
%%% Author: William L. He (2020)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [] = drawElectoralVotes(folderName,election,date)

%%% Loading results
load(['./' folderName '/Results' election '_' date],'finalR','finalS','nrand')

%%% Accessing the electoral votes each state or superstate has.
corrData = readtable('/dataset_correlations2020p.xlsx');
stateElectoralVotes = table2array(corrData(:,8));

%%% Initializing an array for the number of simulations with a given
%%% result. Note that there are a max of 538 electoral votes possible,
%%% but we offset by 1 to account for EVs from 0 to 538. For example,
%%% electoralVotes(1) = number of simulations with 0 electoral votes
%%% for the Republican candidate, and electoralVotes(2) = number of
%%% simulations with 1 EV for the Republican candidate.
electoralVotes = zeros(539);

%%% Loop through nrand (usually 10,000) simulated elections
for ii = 1:nrand
    
    %%% Obtaining the Rep and Dem votes for each simulation
    simulationR = finalR(:,ii);
    simulationD = 1 - finalR(:,ii) - finalS(:,ii);      % Since 1 = Dem + Rep + Other
    
    %%% Counting EVs when Rep wins greater percentage than Dem
    redVotes = sum((simulationR > simulationD) .* stateElectoralVotes);
    
    %%%% Incremementing counter for number of votes won. For example,
    %%%% if redVotes = 200, then we say that electoralVotes(201) should
    %%%% be increased by 1.
    electoralVotes(redVotes+1) = electoralVotes(redVotes+1) + 1;
end

%%% Dividing by the number of simulations and multipying by 100 to
%%% frame things as the percent of simulations with a given EV outcome.
electoralVotes = 100*electoralVotes./nrand;

%%% For graphing purposes, we break the electoral votes up into those
%%% for which the Republican candidate gets strictly less than 269 EVs
%%% (corresponding to a Dem victory) and those for which the Republican
%%% candidate gets strictly more than 269 EVs (corresponding to a Rep
%%% victory). We note that index 270 corresponds to 269 EVs in our
%%% matrix.
blueElectoralVotes = electoralVotes(1:269);         % Rep EVs associated with a Dem overall victory
redElectoralVotes = electoralVotes(271:539);        % Rep EVs associated with a Rep overall victory
tieElectoralVotes = electoralVotes(270);            % Rep EVs associatted with a tie

%%% Counting percentage of simulations in which a Rep won and the
%%% percentage of simulations in which a Dem won
chanceRepWin = sum(redElectoralVotes);
chanceDemWin = sum(blueElectoralVotes);
chanceTie = sum(tieElectoralVotes);

%%% Drawing graph
set(groot,{'DefaultAxesXColor','DefaultAxesYColor','DefaultAxesZColor'},{'k','k','k'})
figure('units','normalized','outerposition',[0 0 1 1])
bar(0:268,blueElectoralVotes,'EdgeColor','b','FaceColor','b','BarWidth',1)
hold on
bar(270:538,redElectoralVotes,'EdgeColor','r','FaceColor','r','BarWidth',1)
bar(269,tieElectoralVotes,'EdgeColor',[123 123 123]/255,'FaceColor',[123 123 123]/255,'BarWidth',1)
legend({['Democratic win: ' char(string(round(chanceDemWin,2))) '%'], ...
    ['Republican win: ' char(string(round(chanceRepWin,2))) '%']...
    ['Tie: ' char(string(round(chanceTie,2))) '%']})
title(['Electoral Vote Distribution for ', election(1:4), ' Forecast'])
xlabel('Number of Republican Electoral Votes')
ylabel('Chance of this Electoral-Vote Outcome (%)')
set(gca,'FontSize',26)
box('off')
saveas(gcf, ['electoralVotes_' election '-' date '.fig'])
saveas(gcf, ['electoralVotes_' election '-' date '.png'])

end     % end of main function
