%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Program for formatting polling data from FiveThirtyEight for 2020
%%% presidential, senatorial, and gubernatorial elections. See
%%% https://modelingelectiondynamics.gitlab.io/2020-forecasts/index.html
%%% for 2020 U.S. forecasts based on this code.
%%%
%%% 2020
%%%
%%% Authors: Samuel Chian, William L. He, Christopher M. Lee, and
%%% Alexandria Volkening (alexandria.volkening@northwestern.edu)
%%%
%%% Formats data as described in "Forecasting elections using compartmental
%%% models of infection" by Alexandria Volkening, Daniel F. Linder,
%%% Mason A. Porter, and Grzegorz A. Rempala (2020, to appear,
%%% https://arxiv.org/abs/1811.01831) for prior elections before 2020.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Purpose: This code produces one data point per month (representing the
%%% percent of Republican, Democratic, and undecided/other voters) in each
%%% of our swing/individual states and superstates. It also defines the
%%% file we use for correlating noise in our SDE model.
%%%
%%% Input
%%% (1) pollsFile, the file with the data from FiveThirtyEight. This file
%%% can be downloaded from https://projects.fivethirtyeight.com/polls/. Our
%%% code works for presidential, senatorial, and gubernatorial data files
%%% from FiveThirtyEight for 2020. For example, after renaming the file, we
%%% use pollsFile = 'pres_polls_2020-09-19';
%%% (2) forecastDate (as 'mm/dd/yy') containing the date of the intended
%%% forecast, in the sense that we use only polls completed on or before
%%% this date in our forecast.
%%%
%%%
%%% Output:
%%% Two .csv with the formatted polling data (means per month) for
%%% Republican and Democratic percentages, with the naming convention
%%% 'RepPercentages2020p_' num2str(numMonths) '_' pollsFile(17:end) '.csv'
%%% 'DemPercentages2020p_' num2str(numMonths) '_' pollsFile(17:end) '.csv'
%%% and one .xlsx file with the formatted data for correlating noise in our
%%% model (accounting for any superstates), titled
%%% "dataset_correlations2020.xlsx"
%%%
%%% We also output popSizesForParamFitting, which contains the voting-age
%%% population  sizes of any Red and Blue superstates, followed by the
%%% individually forecast states (individual states listed alphabetically),
%%% followed by the total in the U.S. This is used in the parameter fitting
%%% and election model code.
%%% We also output regionsForecast, a cell array containing the regions
%%% forecast (e.g., Red, Blue, Colorado, etc.)
%%% Finally, we output numMonths, which is used in parameter fitting
%%%
%%% To run:
%%% For example, to format the presdiential data on Sept. 21, use:
%%% [populations, regions, numMonths] = formatting538polls_2020('pres_polls_2020-09-21','09/22/20')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [popSizesForParamFitting, regionsForecast, numMonths] = formatting538polls_2020(pollsFile, forecastDate)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% We bin our polling data in 30-day bins to produce the data points that
%%% we use in our parameter fitting. To determine how many bins we will
%%% have, we use forecastDate. For example, if forecastDate falls between 0
%%% and 30 days from the election, inclusive, then we use 11 bins. If it
%%% falls within [60, 30) days of the election, we use 10 bins, etc.
electionDay = datenum('11/03/20', 'mm/dd/yy');
forecastDatenum = datenum(forecastDate, 'mm/dd/yy');
daysToElec = electionDay - forecastDatenum;     % a positive number indicating days from the election day and our intended forecast day (in terms of polling data used)
if daysToElec >= 0 && daysToElec <= 30
    numMonths = 11;
elseif daysToElec > 30 && daysToElec <= 60
    numMonths = 10;
elseif daysToElec > 60 && daysToElec <= 90
    numMonths = 9;
elseif daysToElec > 90 && daysToElec <= 120
    numMonths = 8;
elseif daysToElec > 120 && daysToElec <= 150
    numMonths = 7;
elseif daysToElec > 150 && daysToElec <= 180
    numMonths = 6;
elseif daysToElec > 180 && daysToElec <= 210
    numMonths = 5;
elseif daysToElec > 210 && daysToElec <= 240
    numMonths = 4;
elseif daysToElec > 240 && daysToElec <= 270
    numMonths = 3;
else
    error('Choose a later forecast date.')
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Opening the appropriate file that contains broad information about each
%%% election type (state races, population sizes, demographic info). We
%%% also define an election type parameter (p for presidential, s for
%%% senatorial, and g for gubernatorial).
if contains(pollsFile, 'pres')
    indexData = readtable('/dataset_index2020.xlsx','Sheet','2020p','Range','A1:K52');
    numRaces = 51;                            % 50 states + District of Columbia
    electionType = '2020p';
    electoralVotes =  table2array(indexData(:,11));
elseif contains(pollsFile, 'sen')
    indexData = readtable('/dataset_index2020.xlsx','Sheet','2020s','Range','A1:J36');
    numRaces = 35;                            % 35 races
    electionType = '2020s';
elseif contains(pollsFile, 'gub')
    indexData = readtable('/dataset_index2020.xlsx','Sheet','2020g','Range','A1:J12');
    numRaces = 11;                            % 11 races
    electionType = '2020g';
else
    error('Please include pres, sen, or gub in your file name, to indicate the election type')
end

%%% Extracting the states and their voting-age populations. See data
%%% sources in dataset_index2020.xlsx for details about the demographics
%%% below, as well as full references.
allStates = table2cell(indexData(:,1));         % state or district (District of Columbia) names (alphabetical)
allAbbrevs = table2cell(indexData(:,2));        % abbreviated names
allColors = table2cell(indexData(:,3));         % race colors (R for Red superstate, B for Blue superstate, U for swing/individually forecast)
votingAgePop = table2array(indexData(:,4));     % voting-age population size per state or district
allNHispanic = table2array(indexData(:,5));     % number of Non-Hispanic individuals per state
allBlack = table2array(indexData(:,6));         % number of Black, Non-Hispanic individuals per state
allHispanic = table2array(indexData(:,7));      % number of Hispanic individuals per state
allCollege = table2array(indexData(:,8));       % percent (e.g, 25.5) of individuals with a college education per state
mainRep = table2cell(indexData(:,9));           % last name of main Republican running in this race
mainDem = table2cell(indexData(:,10));          % last name of main Democrat running in this race (for Arkansas, we consider the Lib candidate Dem)
if strcmp(electionType, '2020p') || strcmp(electionType, '2020s')   % superstates are used
    abbrevs = ['Red'; 'Blue'; allAbbrevs(strcmp(allColors, 'U'))];
    states = ['Red'; 'Blue'; allStates(strcmp(allColors, 'U'))];
elseif strcmp(electionType, '2020g')   % superstates are not used
    abbrevs = allAbbrevs(strcmp(allColors, 'U'));
    states = allStates(strcmp(allColors, 'U'));
else
    error('Check the election type - this program is used for 2020 pres, sen, and gub forecasts.')
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Extracting data from FiveThirtyEight's spreadsheets for 2020 at
%%% https://projects.fivethirtyeight.com/polls/. This spreadsheet is given
%%% by the pollsFile variable passed to this function.

%%% Calculating number of rows in the FiveThirtyEight file and extracting
%%% the data (saved as a cell array) from this file
rawData = readtable([pollsFile '.xlsx'], 'DatetimeType' , 'datetime');


%%% Pulling out specific datasets that are useful for our model
questionId = table2cell(rawData(:,1));                  % column A (number)
pollId = table2cell(rawData(:,2));                      % column B (number)
cycle = table2cell(rawData(:,3));                       % column C (number)
state = table2cell(rawData(:,4));                       % column D (string, like Missouri, empty if national)
pollsterId = table2cell(rawData(:,5));                  % column E (number)
pollster = table2cell(rawData(:,6));                    % column F (string, with spaces)
pollsterRatingId = table2cell(rawData(:,10));           % column J (number)
pollster538grade = table2cell(rawData(:,12));           % column L (string, sometimes empty, like B/C)
pollSampleSize = table2cell(rawData(:,13));             % column M (number)
pollSampleType = table2cell(rawData(:,14));             % column N (string, like lv for likely voters)
pollMethod = table2cell(rawData(:,16));                 % column P (string, like Online)
office = table2cell(rawData(:,17));                     % column Q (string, e.g. U.S. President)
seat = table2cell(rawData(:,19));                       % column S (string, Class III for example, only relevant for Senate, related to special elections)

%%% For handling the dates of the polls and the election, you may need to
%%% tweak these lines below, depending on how Excel formats your data.
startDates = cellfun(@datenum, table2cell(rawData(:,20)),'UniformOutput',false);        % column T (dates of the form day/month/year transferred to datenums
endDates = cellfun(@datenum, table2cell(rawData(:,21)),'UniformOutput',false);          % column U (dates of the form day/month/year)
elecDates = cellfun(@datenum, table2cell(rawData(:,22)),'UniformOutput',false);         % column V (dates of the election, usually all the same)

notes = table2cell(rawData(:,30));                      % column AD (string, contains various notes, but the ones that we are interested indicate special elections)
candidate = table2cell(rawData(:,34));                  % column AH (string, Biden, Trump, etc.)
party = table2cell(rawData(:,37));                      % column AK (string, REP, DEM, LIB, potentially others)
percent = table2cell(rawData(:,38));                    % column AL (percent, like 53, for this candidate)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% For any states that have spaces in their names, we remove the space,
%%% and then we capitalize 'of' in District of Columbia.
state = strrep(state, ' ', '');
state = strrep(state, 'DistrictofColumbia', 'DistrictOfColumbia');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% For the Senate elections, there are two special races (Georgia and
%%% Arizona). These are indicated in the files as any class other than
%%% Class 2 seats. For special elections, we replace the state name with,
%%% for example ArizonaSpecial and the state abbreviation with AZS.
%%% Additionally, because the Arkansas Senate election features a Rep vs a
%%% Libertarian candidate (Harringon), we choose to consider Harrington as
%%% a Democrat and redefine this here. Note that Zuckerman (a progressive
%%% candidate for governor of Vermont) was already considered DEM in
%%% FiveThirtyEight's polling data.
if strcmp(electionType, '2020s')
    state(logical((strcmp(seat,'Class II') == 0).*(strcmp(state,'Arizona') == 1))) = {'ArizonaSpecial'};
    state(logical((strcmp(seat,'Class II') == 0).*(strcmp(state,'Georgia') == 1))) = {'GeorgiaSpecial'};
    
    party(logical(strcmp(candidate, 'Harrington').*strcmp(state,'Arkansas'))) = {'DEM'};
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% We immediately filter out national polls (polls with no state) and
%%% polls not corresponding to the 2020 cycle (e.g., the FiveThirtyEight
%%% Senate data, for example, contains earlier years as well)
logicalnotNational = ~strcmp(state,'');
[questionId, pollId, cycle, state, pollsterId, pollster, pollsterRatingId, pollster538grade, pollSampleSize, pollSampleType, pollMethod, office, startDates, endDates, elecDates, notes, candidate, party, percent] = filterOut(logicalnotNational, questionId, pollId, cycle, state, pollsterId, pollster, pollsterRatingId, pollster538grade, pollSampleSize, pollSampleType, pollMethod, office, startDates, endDates, elecDates, notes, candidate, party, percent);
logical2020 = (cell2mat(cycle) == 2020);
[questionId, pollId, cycle, state, pollsterId, pollster, pollsterRatingId, pollster538grade, pollSampleSize, pollSampleType, pollMethod, office, startDates, endDates, elecDates, notes, candidate, party, percent] = filterOut(logical2020, questionId, pollId, cycle, state, pollsterId, pollster, pollsterRatingId, pollster538grade, pollSampleSize, pollSampleType, pollMethod, office, startDates, endDates, elecDates, notes, candidate, party, percent);

%%% The special election for Georgia senator takes place on January 5,
%%% 2021, so we polls with an election day of Jan 5, 2021 from our dataset.
%%% This corresponds to a datevec of 738161
logicalJan5elec = ~(cell2mat(elecDates) == 738161);
[questionId, pollId, cycle, state, pollsterId, pollster, pollsterRatingId, pollster538grade, pollSampleSize, pollSampleType, pollMethod, office, startDates, endDates, elecDates, notes, candidate, party, percent] = filterOut(logicalJan5elec, questionId, pollId, cycle, state, pollsterId, pollster, pollsterRatingId, pollster538grade, pollSampleSize, pollSampleType, pollMethod, office, startDates, endDates, elecDates, notes, candidate, party, percent);

%%% Here we filter out any polls that are of Maine-CD 1, Maine-CD 2,
%%% Nebraska CD-1, or Nebraska CD-2. We only use general Maine and Nebraska
%%% polls
logicalnotCD = ~strcmp(state,'MaineCD-1') & ~strcmp(state,'MaineCD-2') & ~strcmp(state,'NebraskaCD-1') & ~strcmp(state,'NebraskaCD-2');
[questionId, pollId, cycle, state, pollsterId, pollster, pollsterRatingId, pollster538grade, pollSampleSize, pollSampleType, pollMethod, office, startDates, endDates, elecDates, notes, candidate, party, percent] = filterOut(logicalnotCD, questionId, pollId, cycle, state, pollsterId, pollster, pollsterRatingId, pollster538grade, pollSampleSize, pollSampleType, pollMethod, office, startDates, endDates, elecDates, notes, candidate, party, percent);

%%% For the Senate elections, we filter out Louisiana's polling data
%%% because it is has a primary/run-off election system (this line was
%%% added on October 28 2020 to account for new Louisiana polls)
if strcmp(electionType, '2020s') 
    logicalnotLA = ~strcmp(state,'Louisiana');
    [questionId, pollId, cycle, state, pollsterId, pollster, pollsterRatingId, pollster538grade, pollSampleSize, pollSampleType, pollMethod, office, startDates, endDates, elecDates, notes, candidate, party, percent] = filterOut(logicalnotLA, questionId, pollId, cycle, state, pollsterId, pollster, pollsterRatingId, pollster538grade, pollSampleSize, pollSampleType, pollMethod, office, startDates, endDates, elecDates, notes, candidate, party, percent);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Once we've removed any non-2020 election cycles, we check that the
%%% election date is 3 November 2020 for all the remaining polling data. We
%%% note that this may be a place where you have to slighly adjust how you
%%% handle the dates (datevec, datenum, etc) depending on your computer. We
%%% are using date numbers.
electionDay = datenum('11/03/20', 'mm/dd/yy');
dateCheck = cell2mat(elecDates) - electionDay;          % calculating the distance from the given to our expected election day
if sum(dateCheck ~= 0) > 0
    error('Some of the elections are not on 3 November 2020. Check your data handling.')
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% If we want to do produce earlier forecasts, we remove polls that occur
%%% after the date of interest from our data set. We include polls that
%%% occur on the date of interest. We use the end date of the poll for
%%% this. In our forecasts for 2012, 2016, and 2018, we used the midpoint
%%% of the start and end dates for this, in the SIREV article.

%%% Calculating the distance from the polling end date and the intended
%%% forecast date. If this is positive, it corresponds to a poll that
%%% finished after our intended forecast, so we remove it from our dataset
forecastDatenum = datenum(forecastDate, 'mm/dd/yy');
logicalBeforeForecast = (cell2mat(endDates) - forecastDatenum) <= 0;
[questionId, pollId, cycle, state, pollsterId, pollster, pollsterRatingId, pollster538grade, pollSampleSize, pollSampleType, pollMethod, office, startDates, endDates, elecDates, notes, candidate, party, percent] = filterOut(logicalBeforeForecast, questionId, pollId, cycle, state, pollsterId, pollster, pollsterRatingId, pollster538grade, pollSampleSize, pollSampleType, pollMethod, office, startDates, endDates, elecDates, notes, candidate, party, percent);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% For error checking, it is sometimes useful to now how many questions
%%% ask about a Dem, Rep, or other candidate. We expect to have largely
%%% questions involving Dem and Rep candidates.
numOtherParty = sum(~strcmp(party,'REP') & ~strcmp(party,'DEM'));
numDemParty = sum(strcmp(party,'DEM'));
numRepParty = sum(strcmp(party,'REP'));

%%% We focus on Rep vs. Dem. races, and consider everything else as other
%%% as a simplification, so we remove any rows that do not correspond to
%%% Dem or Rep candidates. For the special case of the Arkansas Senate
%%% election, which is a Rep vs a Lib (Harrington), we already redefined
%%% the Lib candidated as a Democrat above.
logicalDemRep = strcmp(party,'REP') | strcmp(party,'DEM');
[questionId, pollId, cycle, state, pollsterId, pollster, pollsterRatingId, pollster538grade, pollSampleSize, pollSampleType, pollMethod, office, startDates, endDates, elecDates, notes, candidate, party, percent] = filterOut(logicalDemRep, questionId, pollId, cycle, state, pollsterId, pollster, pollsterRatingId, pollster538grade, pollSampleSize, pollSampleType, pollMethod, office, startDates, endDates, elecDates, notes, candidate, party, percent);

%%% In case a question involves multiple Rep and Dem candidates, we focus
%%% only on those questions that involve 2 candidates, after we've removed
%%% other candidates
logicalTwoPartyQuestion = false(1,length(questionId));
for ii = 1:length(questionId)
    %%% If questionId shows up exactly twice in the file, it is a two-party
    %%% question
    if sum([questionId{:}] == questionId{ii}) == 2
        logicalTwoPartyQuestion(ii) = 1;
    end
end
logicalTwoPartyQuestion = logical(logicalTwoPartyQuestion);
[questionId, pollId, cycle, state, pollsterId, pollster, pollsterRatingId, pollster538grade, pollSampleSize, pollSampleType, pollMethod, office, startDates, endDates, elecDates, notes, candidate, party, percent] = filterOut(logicalTwoPartyQuestion, questionId, pollId, cycle, state, pollsterId, pollster, pollsterRatingId, pollster538grade, pollSampleSize, pollSampleType, pollMethod, office, startDates, endDates, elecDates, notes, candidate, party, percent);

%%% For error checking, we look at how many unique candidates there are. It
%%% is also useful to check that the number of polls for a Democrat and
%%% Republican are the same
uniqueCandidates = unique(candidate);
numPollsPerCandidate = zeros(1,length(uniqueCandidates));
for ii = 1:length(uniqueCandidates)
    numPollsPerCandidate(ii) = sum(strcmp(candidate,uniqueCandidates(ii)));
end
numDemParty = sum(strcmp(party,'DEM'));
numRepParty = sum(strcmp(party,'REP'));
if numDemParty ~= numRepParty
    error('Number of polls involving a Democrat is not equal to the number of polls involving a Republican; check your data.')
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% At this point, we have filtered out all 3-candidate polling data, so
%%% all data is Dem vs. Rep. Since 538 presents polls as Dem and Rep in the
%%% same column, we only need every other value of these variables.
questionId = questionId(1:2:end);
pollId = pollId(1:2:end);
cycle = cycle(1:2:end);
state = state(1:2:end);
pollsterId = pollsterId(1:2:end);
pollster = pollster(1:2:end);
pollsterRatingId = pollsterRatingId(1:2:end);
pollster538grade = pollster538grade(1:2:end);
pollSampleSize = pollSampleSize(1:2:end);
pollSampleType = pollSampleType(1:2:end);
pollMethod = pollMethod(1:2:end);
office = office(1:2:end);
startDates = startDates(1:2:end);
endDates = endDates(1:2:end);
elecDates = elecDates(1:2:end);
notes = notes(1:2:end);

%%% We also pull out the Rep and Dem candidates and percents
DemCand = candidate(1:2:end);
RepCand = candidate(2:2:end);
DemPerc = percent(1:2:end);
RepPerc = percent(2:2:end);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Now we filter out any polls that are not associated with the main Dem
%%% and Rep candidates associated with each race. For example, the 2020
%%% data from FiveThirtyEight contains Warren (from earlier in the year).
%%% As another example, the gubernatorial data also contains matchups
%%% between Sununu and a range of candidates.
mainVsmainLogical = false(length(RepPerc),1);
for ii = 1:numRaces
    mainVsmainLogical = logical(mainVsmainLogical + (strcmp(DemCand,mainDem(ii)) & strcmp(RepCand,mainRep(ii))));
end

questionId = questionId(mainVsmainLogical);
pollId = pollId(mainVsmainLogical);
cycle = cycle(mainVsmainLogical);
state = state(mainVsmainLogical);
pollsterId = pollsterId(mainVsmainLogical);
pollster = pollster(mainVsmainLogical);
pollsterRatingId = pollsterRatingId(mainVsmainLogical);
pollster538grade = pollster538grade(mainVsmainLogical);
pollSampleSize = pollSampleSize(mainVsmainLogical);
pollSampleType = pollSampleType(mainVsmainLogical);
pollMethod = pollMethod(mainVsmainLogical);
office = office(mainVsmainLogical);
startDates = startDates(mainVsmainLogical);
endDates = endDates(mainVsmainLogical);
elecDates = elecDates(mainVsmainLogical);
notes = notes(mainVsmainLogical);
candidate = candidate(mainVsmainLogical);
party = party(mainVsmainLogical);
RepPerc = RepPerc(mainVsmainLogical);
DemPerc = DemPerc(mainVsmainLogical);
RepCand = RepCand(mainVsmainLogical);
DemCand = DemCand(mainVsmainLogical);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Handling dates
%%% Formatting the poll start and end dates and calculating the number of
%%% days from the midpoint of each poll to the election day (Nov 3, 2020).
%%% Note that this is given as a negative number: in particular, datesMid =
%%% -30 means a poll's midpoint was 30 days before the election.
electionDay = repmat(datevec('11/03/20', 'mm/dd/yy'),1,length(startDates));
datesSM = (etime(datevec(cell2mat(startDates)),electionDay))./(60*60*24);       % calculating the distance from election day to the poll start date and converting to days
datesFM = (etime(datevec(cell2mat(endDates)),electionDay))./(60*60*24);         % same for poll end date (note these distances are negative)
datesMid = floor((datesSM + datesFM)/2);                                        % averaging to obtain the distance in days from the poll date to election day
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% We now separate out the polls by state and determine which states have
%%% polling data available.
setOfStates = unique(state);                    % set of unique state names that have polling data
numStatesWithPolls = length(setOfStates);       % number of states with polls <= number of races
logicalPolls = false(1, numRaces);              % if a race has polls available, logicalPolls = 1, otherwise = 0.
logicalIndiv = false(1, numRaces);              % if a race is forecast individaully. logicalIndiv = 1, otherwise 0.
%%% Looping through each state race (some will have polls, and some won't)
for ii = 1:numRaces
    RepPolls.(allStates{ii}) = RepPerc(strcmp(state,allStates(ii)));            % e.g., RepPoll.Minneosta = Republican percentages for polls in which the state = Minnesota
    DemPolls.(allStates{ii}) = DemPerc(strcmp(state,allStates(ii)));            % similarly, pulling out any Dem percentages for the given state or district
    dayCounts.(allStates{ii}) = datesMid(strcmp(state,allStates(ii)));          % similarly, pulling out the time stamps for the polls (a nonpositive number indicating - how many days until the election day)
    numPolls.(allStates{ii}) = length(RepPerc(strcmp(state,allStates(ii))));    % determining the number of polls this state has
    colors.(allStates{ii}) = allColors(ii);                                     % for superstate reasons, we associate each state with its color ('R' for red superstate, 'B' for blue superstate, 'U' for swing/individually forecast)
    logicalPolls(ii) = sum(strcmp(state,allStates(ii))) > 0;                    % a logical that tells you if this state has polling data (1) or not (0)
    logicalIndiv(ii) = strcmp(allColors(ii), 'U');                              % a logical that tells you if this state is forecast individually or not
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Now we take the polling data and bin it by month. Here we are moving
%%% toward obtaining our 1 data point per month for parameter fitting,
%%% where we approximate months as 30 days long, and we count back from the
%%% election day.

%%% Looping through the states that have polls (we create temporty
%%% parameters, symbolized by "curr" for ease).
for ii = 1:numStatesWithPolls
    currDates = dayCounts.(setOfStates{ii});                        % extracting the days from the election for state ii with pollign data (this is a nonpositive number)
    numPollsPerCurrState = numPolls.(setOfStates{ii});              % extracting the number of polls state ii has
    currRep = cell2mat(RepPolls.(setOfStates{ii}));                 % extracting the Republican percentages for state ii
    currDem = cell2mat(DemPolls.(setOfStates{ii}));                 % extracting the Democratic percentages for state ii
    
    identifyMonth = {[], [], [], [], [], [], [], [], [], [], []};   % we consider a max of 11 bins
    %%% Bins 1 and 11 are special cases, so we define them here. Note that
    %%% for our forecasts posted on our website prior to 20 Sept. 2020, we
    %%% used identifyMonth{1} = (currDates < -300).
    identifyMonth{1} = (currDates >= -330) & (currDates < -300);    % we define "January" (bin 1) as anything between 330 and 300 days before the election
    identifyMonth{11} = (currDates >= -30) & (currDates <= 0);      % "November" (bin 11) is anything between 0 and 30 days from the election
    %%% Looping over all other bins
    for jj = 2:10
        %%% For example, "Feb" (bin 2) is polls >= -300 and < -270 days,
        %%% "March" (bin 3) is polls >= -270 and < -240, "Oct" (bin 10) is
        %%% polls >= -60 and < -30...
        identifyMonth{jj} = (currDates >= -360 + 30*jj) & (currDates < -330 + 30*jj);
    end
    
    RepMonth = {[], [], [], [], [], [], [], [], [], [], []};        % matrix to hold the Republican data points each month for state ii
    DemMonth = {[], [], [], [], [], [], [], [], [], [], []};        % matrix to hold the Democratic data points each month for state ii
    for jj = 1:11
        %%% For state ii, we take the mean of the percentages across all
        %%% polls in that month.
        RepMonth{jj} = sum(currRep.*(identifyMonth{jj})) ./ sum(identifyMonth{jj});
        DemMonth{jj} = sum(currDem.*(identifyMonth{jj})) ./ sum(identifyMonth{jj});
    end
    
    %%% We only consider polls within 330 days of the election. Note that
    %%% in our forecasts posted on the website prior to 20 Sept. 2020 we
    %%% used earliestPollDate = min(currDates).
    earliestPollDate = min(currDates(currDates >= -330));
    
    %%% Calculating the most recent poll date for the number of months
    %%% considered. For example, if numMonths = 10, we only consider polls
    %%% prior to < -30 (e.g., we don't consider polls in bin 11).
    mostRecentPollDate = max(currDates(currDates < (numMonths - 11)*30 ));
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Additional processing for special cases
    %%% This portion of the code handles a special cases: (1) no polls for
    %%% a given state in the first months of the year; (2) no polls for a
    %%% state in the months leading up to election day; (3) no polls in
    %%% intermediate months; or (4) only one month with polls. In the
    %%% third case, we fill in any missing time points by interpolating. In
    %%% cases (1) and (2), we cannot use interpolation. Instead, in the
    %%% first case, we assign any early months missing polls the earliest
    %%% possible polling data (e.g. if Jan and Feb are missing polls but
    %%% March has polls, we assign Jan and Feb March's poll average).
    %%% Similarly, in the second case, we assign any late months missing
    %%% polls the latest possible polls (e.g. if Nov is missing polls but
    %%% Oct. has polls, we assign Nov Oct's polling data average). For case
    %%% (4), we assign all months the data value of the single month that
    %%% has polling data.
    if numPollsPerCurrState == 1     % only one bin with polling data (case (4) above)
        
        %%% If there is only one bin with polling data, we assign all bins
        %%% this data point
        onlyValueRep = max([RepMonth{1},RepMonth{2},RepMonth{3},RepMonth{4},RepMonth{5},RepMonth{6},RepMonth{7},RepMonth{8},RepMonth{9},RepMonth{10},RepMonth{11}]);
        for jj = 1:11
            RepMonth{jj} = onlyValueRep;
        end
        
        onlyValueDem = max([DemMonth{1},DemMonth{2},DemMonth{3},DemMonth{4},DemMonth{5},DemMonth{6},DemMonth{7},DemMonth{8},DemMonth{9},DemMonth{10},DemMonth{11}]);
        for jj = 1:11
            DemMonth{jj} = onlyValueDem;
        end
        
    else        % there is more than 1 poll - now we handle cases (1) and (2) above
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% Handles case (1) -- polls missing from the early months of the
        %%% election year. An example would be if Jan and Feb don't have
        %%% polls, but March does.
        
        %%% Earliest month with polling data (using floor gives desired
        %%% behavior of >= and <)
        earliestPollMonth = 12 + floor(earliestPollDate / 30) - (earliestPollDate == 0);
        
        %%% Assign the data from the earliest month for which we have data
        %%% to all prior months. For example, if earliestPollMonth = 2,
        %%% then assign assign this data to bin 1 as well
        for jj = 1:earliestPollMonth-1
            RepMonth{jj} = RepMonth{earliestPollMonth};
            DemMonth{jj} = DemMonth{earliestPollMonth};
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% Handle case (2) above -- recent polls missing close to the
        %%% election date. For example, this happens if we are in November,
        %%% but there are no polls in the last bin, so we fill in bin 11
        %%% with bin 10.
        
        %%% Latest month with polling data. For example, if the most recent
        %%% poll date is 5 days out from the election, then floor(-5/30) =
        %%% -1 and we have 12-1 = 11, so we know the last bin has data.
        latestPollMonth = 12 + floor(mostRecentPollDate / 30) - (mostRecentPollDate == 0);
        
        %%% Assign the data from the latest month for which we have data to
        %%% all following months
        for jj = latestPollMonth+1:11
            RepMonth{jj} = RepMonth{latestPollMonth};
            DemMonth{jj} = DemMonth{latestPollMonth};
        end
    end % end of if-else statement that handles cases (4), (1), and (2) in turn
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Now we handle case (3), polls missing in intermediate months -- we
    %%% interpolate the polling data to fill in these missing bins using
    %%% Matlab's interp1 function.
    
    formattedRepublicanData = cell2mat(RepMonth(1:numMonths));
    nanRepState = isnan(formattedRepublicanData);
    v = formattedRepublicanData(logical(1-nanRepState));
    xs = 1:numMonths;
    x = xs(logical(1-nanRepState));
    xq = 1:numMonths;
    vq = interp1(x,v,xq);
    formattedRepublicanData = vq;
    
    formattedDemocratData = cell2mat(DemMonth(1:numMonths));
    nanDemState = isnan(formattedDemocratData);
    v = formattedDemocratData(logical(1-nanDemState));
    xs = 1:numMonths;
    x = xs(logical(1-nanDemState));
    xq = 1:numMonths;
    vq = interp1(x,v,xq);
    formattedDemocratData = vq;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    formattedRepByState.(setOfStates{ii}) = formattedRepublicanData;
    formattedDemByState.(setOfStates{ii}) = formattedDemocratData;
    
end     % end loop over binning data into numMonths bins for each state
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% To prepare for selecting the states in our Red and Blue superstates (if
%%% included -- e.g., we have superstates in our 2020 Senate and president
%%% forecasts, but not our governor forecasts), we first collect the
%%% information (name, color, population sizes, formatted polling data)
%%% for the states with polling data
colorsSetOfStatesWithPolls = cell(numStatesWithPolls,1);                % initializing cell array to hold colors (R for red superstate, B for blue superstate, U for swing/individually forecast) of our states with polls
populationSetOfStatesWithPolls = votingAgePop(logicalPolls);            % population sizes of each state with polls
%%% Looping over states with polls, we fill in the color of each state (R
%%% for red superstate, B for blue supersate, U for swing/individually
%%% forecast)
for ii = 1:numStatesWithPolls
    colorsSetOfStatesWithPolls(ii) = colors.(setOfStates{ii});
end
%%% Calculating the voting-age populations of the states in the superstates
%%% that have polling data. This is only used for weighting the polling
%%% data -- we use the population of the full superstates (states and
%%% districts with or without polling data) for all other purposes in our
%%% work.
redSuperstatePop = sum(populationSetOfStatesWithPolls(strcmp(colorsSetOfStatesWithPolls, 'R')));     % total voting-age population of states in Red superstate that have polling data
blueSuperstatePop = sum(populationSetOfStatesWithPolls(strcmp(colorsSetOfStatesWithPolls, 'B')));    % total voting-age population of states in Blue superstate that have polling data

totalRedSuperPop = sum(votingAgePop(strcmp(allColors, 'R')));           % total voting-age population in Red superstate (regardless of if the states have polling data or not)
totalBlueSuperPop = sum(votingAgePop(strcmp(allColors, 'B')));          % total voting-age population in Blue superstate (regardless of if the states have polling data or not)
individualPop = populationSetOfStatesWithPolls(strcmp(colorsSetOfStatesWithPolls, 'U'));                      % voting-age population sizes for individually forecast states with polls
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Building the superstate polling data points by weighting the polls for
%%% each state in them by the voting-age population size of tht state.
formattedRepByState.('Red') = 0;
formattedDemByState.('Red') = 0;
formattedRepByState.('Blue') = 0;
formattedDemByState.('Blue') = 0;
for ii = 1:numStatesWithPolls
    if strcmp(colorsSetOfStatesWithPolls(ii),'R') == 1
        formattedRepByState.('Red') = formattedRepByState.('Red') + formattedRepByState.(setOfStates{ii})*populationSetOfStatesWithPolls(ii);
        formattedDemByState.('Red') = formattedDemByState.('Red') + formattedDemByState.(setOfStates{ii})*populationSetOfStatesWithPolls(ii);
    end
    if strcmp(colorsSetOfStatesWithPolls(ii),'B') == 1
        formattedRepByState.('Blue') = formattedRepByState.('Blue') + formattedRepByState.(setOfStates{ii})*populationSetOfStatesWithPolls(ii);
        formattedDemByState.('Blue') = formattedDemByState.('Blue') + formattedDemByState.(setOfStates{ii})*populationSetOfStatesWithPolls(ii);
    end
end

%%% To complete the weighted mean, dividing by the voting-age population
%%% size of the states in the superstates with polling data.
formattedRepByState.('Red') = formattedRepByState.('Red')/redSuperstatePop;
formattedDemByState.('Red') = formattedDemByState.('Red')/redSuperstatePop;
formattedRepByState.('Blue') = formattedRepByState.('Blue')/blueSuperstatePop;
formattedDemByState.('Blue') = formattedDemByState.('Blue')/blueSuperstatePop;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Next we handle the individually forecast states, and combine both the
%%% superstates and the individually forecast states to arrive at our
%%% binned polling data points that we use for parameter fitting.
statesIndiv = setOfStates(strcmp(colorsSetOfStatesWithPolls, 'U'));            % names of states forecast individually with polling data

%%% Adding superstates in as needed
if strcmp(electionType, '2020p') || strcmp(electionType, '2020s')       % elections that use superstates
    finalFormattedRepData = [formattedRepByState.('Red')', formattedRepByState.('Blue')'];
    finalFormattedDemData = [formattedDemByState.('Red')', formattedDemByState.('Blue')'];
    popSizesForParamFitting = [totalRedSuperPop, totalBlueSuperPop];
    regionsForecast = {'Red', 'Blue'};
else    % 2020g does not have superstates for our forecasts
    finalFormattedRepData = [];
    finalFormattedDemData = [];
    popSizesForParamFitting = [];
    regionsForecast = {};
end
%%% Adding in each individually forecast state
for ii = 1:length(statesIndiv)
    finalFormattedRepData = [finalFormattedRepData, formattedRepByState.(string(statesIndiv(ii)))'];
    finalFormattedDemData = [finalFormattedDemData, formattedDemByState.(string(statesIndiv(ii)))'];
end

%%% Population sizes used for parameter fitting in R. These are also used
%%% in electionModel with totalPop removed.
totalPop = sum([popSizesForParamFitting, individualPop']);
popSizesForParamFitting = [popSizesForParamFitting, individualPop',totalPop];
popSizesForParamFitting = regexprep(num2str(popSizesForParamFitting),'\s+',',');

%%% Finalizing the regions forecast, for use later on.
regionsForecast = [regionsForecast, statesIndiv'];
regionsForecast = cellfun(@(x) ['"' x '"'], regionsForecast, 'UniformOutput', false);
regionsForecast = convertStringsToChars(join(string(regionsForecast),','));

%%% Appending a left column counting from 1 to numMonths.
finalFormattedRepData = [(1:numMonths)',finalFormattedRepData];
finalFormattedDemData = [(1:numMonths)',finalFormattedDemData];


%%% We now write this formatted polling data to two .csv files that are
%%% used by parameterFitting.R to fit parameters.
dlmwrite(['RepPercentages' electionType '_' num2str(numMonths) '_' pollsFile(17:end) '.csv'], finalFormattedRepData, 'delimiter', ',', 'precision', 6)
dlmwrite(['DemPercentages' electionType '_' num2str(numMonths) '_' pollsFile(17:end) '.csv'], finalFormattedDemData, 'delimiter', ',', 'precision', 6)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Generating .xlsx document that includes demographic info to be used for
%%% correlating noise in our SDE model later on. Here we append superstate
%%% info with individual states. For the gubernatorial elections (which
%%% have no superstates, we filter out the Red and Blue superstates before
%%% writing to the .xlsx file by requiring the population be  > 0). This is
%%% specific to a given forecast, since the number of states with polls may
%%% change in time.
popFormatted = [sum(votingAgePop(strcmp(allColors,'R'))); sum(votingAgePop(strcmp(allColors,'B'))); votingAgePop(logical((logicalPolls' == 1).*strcmp(allColors,'U')))];
popHFormatted = [sum(allHispanic(strcmp(allColors,'R'))); sum(allHispanic(strcmp(allColors,'B'))); allHispanic(logical((logicalPolls' == 1).*strcmp(allColors,'U')))];
popNHFormatted = [sum(allNHispanic(strcmp(allColors,'R'))); sum(allNHispanic(strcmp(allColors,'B'))); allNHispanic(logical((logicalPolls' == 1).*strcmp(allColors,'U')))];
popBNHFormatted = [sum(allBlack(strcmp(allColors,'R'))); sum(allBlack(strcmp(allColors,'B'))); allBlack(logical((logicalPolls' == 1).*strcmp(allColors,'U')))];
popEFormatted = [mean(allCollege(strcmp(allColors,'R'))); mean(allCollege(strcmp(allColors,'B'))); allCollege(logical((logicalPolls' == 1).*strcmp(allColors,'U')))];

delete(['dataset_correlations' electionType '.xlsx'])     % clearing old excel file of correlations to replace with our new version
if strcmp(electionType, '2020p')   % superstates are used and electoral college is applicable
    electoralVotesFormatted = [sum(electoralVotes(strcmp(allColors,'R'))); sum(electoralVotes(strcmp(allColors,'B'))); electoralVotes(logical((logicalPolls' == 1).*strcmp(allColors,'U')))];
    corrData = [states(logical([1, 1, logicalPolls(logicalIndiv)])), abbrevs(logical([1, 1, logicalPolls(logicalIndiv)])), num2cell([popNHFormatted(popFormatted>0), popBNHFormatted(popFormatted>0), popHFormatted(popFormatted>0), popEFormatted(popFormatted>0), popFormatted(popFormatted > 0), electoralVotesFormatted(popFormatted > 0)])];
    writecell([{'Region', 'Abbreviation',  'NH, total', 'NH, Black', 'H, total', 'PercentCollege', 'Voting-age pop', 'Electoral votes'}; corrData], ['dataset_correlations' electionType '.xlsx'])
elseif strcmp(electionType, '2020s')   % superstates are used
    corrData = [states(logical([1, 1, logicalPolls(logicalIndiv)])), abbrevs(logical([1, 1, logicalPolls(logicalIndiv)])), num2cell([popNHFormatted(popFormatted>0), popBNHFormatted(popFormatted>0), popHFormatted(popFormatted>0), popEFormatted(popFormatted>0), popFormatted(popFormatted > 0)])];
    writecell([{'Region', 'Abbreviation',  'NH, total', 'NH, Black', 'H, total', 'PercentCollege', 'Voting-age pop'}; corrData], ['dataset_correlations' electionType '.xlsx'])
else    % 2020g
    corrData = [states(logicalPolls' == 1), abbrevs(logicalPolls' == 1), num2cell([popNHFormatted(popFormatted>0), popBNHFormatted(popFormatted>0), popHFormatted(popFormatted>0), popEFormatted(popFormatted>0), popFormatted(popFormatted > 0)])];
    writecell([{'Region', 'Abbreviation',  'NH, total', 'NH, Black', 'H, total', 'PercentCollege', 'Voting-age pop'}; corrData], ['dataset_correlations' electionType '.xlsx'])
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end     % end of main function



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Helper function for filtering out data that is not relevant for us. For
%%% example, we remove the national polls from our data set. This function
%%% takes a logical (1 or 0) and cell arrays, and returns the cell arrays
%%% with the values corresponding to 0 for the logical removed.
function [questionId, pollId, cycle, state, pollsterId, pollster, pollsterRatingId,...
    pollster538grade, pollSampleSize, pollSampleType, pollMethod, office, startDates, endDates,...
    elecDates, notes, candidate, party, percent] = filterOut(logicalToRemove, questionId, pollId, ...
    cycle, state, pollsterId, pollster, pollsterRatingId, pollster538grade, pollSampleSize, pollSampleType,...
    pollMethod, office, startDates, endDates, elecDates, notes, candidate, party, percent)
questionId = questionId(logicalToRemove);
pollId = pollId(logicalToRemove);
cycle = cycle(logicalToRemove);
state = state(logicalToRemove);
pollsterId = pollsterId(logicalToRemove);
pollster = pollster(logicalToRemove);
pollsterRatingId = pollsterRatingId(logicalToRemove);
pollster538grade = pollster538grade(logicalToRemove);
pollSampleSize = pollSampleSize(logicalToRemove);
pollSampleType = pollSampleType(logicalToRemove);
pollMethod = pollMethod(logicalToRemove);
office = office(logicalToRemove);
startDates = startDates(logicalToRemove);
endDates = endDates(logicalToRemove);
elecDates = elecDates(logicalToRemove);
notes = notes(logicalToRemove);
candidate = candidate(logicalToRemove);
party = party(logicalToRemove);
percent = percent(logicalToRemove);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
