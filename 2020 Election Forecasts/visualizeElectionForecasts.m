%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Purpose: Create visualizations of the 2020 election results as shown at
%%% https://modelingelectiondynamics.gitlab.io/2020-forecasts/index.html
%%%
%%% Command used to visualize our election forecasts:
%%% visualizeElectionForecasts(folderName, election, date)
%%%
%%% Input:
%%% folderName is the name of the folder containing the simulation results,
%%% election is the election year and month, date is the date of data 
%%% collection
%%%
%%% Output:
%%% Visualizations of our election forecasts
%%%
%%% To run, for example:
%%% visualizeElectionForecasts('Elections', '2020p_10', '09-21')
%%%
%%% 2020
%%%
%%% Authors: William L. He, Samuel Chian, Christopher M. Lee, and
%%% Alexandria Volkening (alexandria.volkening@northwestern.edu)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [] = visualizeElectionForecasts(folderName, election, date)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Some initial processing for figure titles
%%% The election type is either p, s, or g (for presidential, senatorial
%%% or gubernatorial)
electionType = election(5);
if strcmp(electionType, 'p') == 1           % presidential
    electionTitle = [election(1:4) ' Presidential Forecasts on ' date];
    %%% Accessing the states and superstates taht we forecast for this
    %%% election and, as appropriate, their electoral votes.
    electionData = readtable('/dataset_correlations2020p.xlsx');
elseif strcmp(electionType, 's') == 1       % senatorial
    electionTitle = [election(1:4) ' Senatorial Forecasts on ' date];
    %%% Accessing the states and superstates taht we forecast for this
    %%% election and, as appropriate, their electoral votes.
    electionData = readtable('/dataset_correlations2020s.xlsx');
elseif strcmp(electionType, 'g') == 1       % gubernatorial
    electionTitle = [election(1:4) ' Gubernatorial Forecasts on ' date];
    %%% Accessing the states and superstates taht we forecast for this
    %%% election and, as appropriate, their electoral votes.
    electionData = readtable('/dataset_correlations2020g.xlsx');
else
    error('Please make sure that your election variable is correct.')
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Loading results (note that finalR is an nrand-long matrix
%%% containing the final Republican fractions that we forecast;
%%% finalS is an nrand-long matrix containing the final other fractions
%%% that we forecast; nrand is the number of simulated elections,
%%% usually 10,000; and you can find the Democratic fractions by using
%%% that finalD = 1 - finalR - finalS)
load(['./' folderName '/Results' election '_' date],'finalR','finalS','nrand')

%%% Pulling out the specific data that is useful for image processing
stateNames = table2cell(electionData(:,1));
stateAbbrev = table2cell(electionData(:,2));
if strcmp(electionType, 'p')
    %%% For presidential elections, we pull out the electoral votes per
    %%% state or superstate
    stateElectoralVotes = table2array(electionData(:,8));
end
numM = length(stateAbbrev);      % number of regions forecast

%%% Adding spaces into the state names as appropriate and designating
%%% special elections with an asterisk
stateNames = strrep(stateNames, 'DistrictOfColumbia', 'District of Columbia');
stateNames = strrep(stateNames, 'NewHampshire', 'New Hampshire');
stateNames = strrep(stateNames, 'NorthDakota', 'North Dakota');
stateNames = strrep(stateNames, 'NewJersey', 'New Jersey');
stateNames = strrep(stateNames, 'NewMexico', 'New Mexico');
stateNames = strrep(stateNames, 'NewYork', 'New York');
stateNames = strrep(stateNames, 'NorthCarolina', 'North Carolina');
stateNames = strrep(stateNames, 'SouthCarolina', 'South Carolina');
stateNames = strrep(stateNames, 'SouthDakota', 'South Dakota');
stateNames = strrep(stateNames, 'RhodeIsland', 'Rhode Island');
stateNames = strrep(stateNames, 'WestVirginia', 'West Virginia');
stateNames = strrep(stateNames, 'Special', '*');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Calculating the endpoints of the range in which the middle 80 percent
%%% of our simulate elections lie, as well as our forecast vote margins
percent10 = prctile((finalR - 1 + finalS + finalR)*100, 10, 2);
percent90 = prctile((finalR - 1 + finalS + finalR)*100, 90, 2);
finalD = 1 - finalR - finalS;
modelForecast = 100*mean(finalR - finalD,2);        % vote margin is positive if a Republican victory and negative if a Democratic victory
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Creating vote-margin bar graphs with 80% confidence intervals
set(groot,{'DefaultAxesXColor','DefaultAxesYColor','DefaultAxesZColor'},{'k','k','k'})
figure('units','normalized','outerposition',[0 0 1 1])
modelForecastDem = modelForecast;
modelForecastDem(modelForecast > 0) = 0;
modelForecastRep = modelForecast;
modelForecastRep(modelForecast < 0) = 0;
bh = barh(1:numM, flipud(modelForecastRep), 'FaceColor', [236/255,65/255,37/255], 'EdgeColor','none');
bh.FaceColor = 'flat';
hold on
barh(1:numM, flipud(modelForecastDem), 'FaceColor',  [20/255,49/255,244/255], 'EdgeColor','none');
for i = 1:numM
    plot([percent10(i), percent90(i)],[numM-i+1, numM-i+1],'Color', [247/255 204/255 73/255], 'LineWidth',3)
    hold on
end
tickCutOff = ceil(max(abs([percent10;percent90]))/10)*10;
xlim([-tickCutOff tickCutOff])
xticks(-tickCutOff:10:tickCutOff)
title(electionTitle)
legend({'Model forecast (Rep. win)', 'Model forecast (Dem. win)', '80% of results'}, 'Location','southeast')
xlabel('Margin lead by party (percentage points)')
ticklabels = get(gca,'XTickLabel');
xticklabels_new = cell(size(ticklabels));
for i = 1:(length(xticklabels)-1)/2
    xticklabels_new{i} = ['\color{blue} +' ticklabels{length(xticklabels)-i+1}];
    xticklabels_new{length(xticklabels)-i+1} = ['\color{red} +' ticklabels{length(xticklabels)-i+1}];
end
xticklabels_new{(length(xticklabels)-1)/2 + 1} = ['\color{black}' abs(ticklabels{(length(xticklabels)-1)/2 + 1})];
set(gca, 'XTickLabel', xticklabels_new);
yticks(1:1:numM)
yticklabels(flipud(stateNames))
set(gca,'FontSize',26,'FontName','Helvetica')
saveas(gcf, ['voteMargins_' election '-' date '.fig'])
saveas(gcf, ['voteMargins_' election '-' date '.png'])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Plotting distribution of electoral votes for presidential elections
if strcmp(electionType, 'p') == 1
    drawElectoralVotes(folderName, election, date)
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




end     % end of main function