%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Data-formatting code for "Forecasting elections using compartmental
%%% models of infection" by Alexandria Volkening, Daniel F. Linder,
%%% Mason A. Porter, and Grzegorz A. Rempala (2020), to appear in SIAM
%%% Review
%%%
%%% If you use this code, please cite the reference above and email
%%% Alexandria Volkening at alexandria_volkening@alumni.brown.edu and
%%% alexandria.volkening@northwestern.edu.
%%%
%%% Copyright 2020 Alexandria Volkening, Daniel F. Linder, Mason A. Porter,
%%% and Grzegorz A. Rempala
%%% Licensed under the Apache License, Version 2.0 (the "License"); you may
%%% not use this file except in compliance with the License. You may obtain
%%% a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
%%% Unless required by applicable law or agreed to in writing, software
%%% distributed under the License is distributed on an "AS IS" BASIS,
%%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
%%% implied. See the License for the specific language governing 
%%% permissions and limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Purpose: This code formats the polling data that we obtained from 
%%% HuffPost Pollster (https://elections.huffingtonpost.com/pollster, 2018. 
%%% Last accessed: 02-11-2018) for 2012 and 2016 using their Pollster API 
%%% v2 (https://elections.huffingtonpost.com/pollster/api/v2, 2018. Data 
%%% provided under a CC BY-NC-SA 3.0 license, 
%%% https://creativecommons.org/licenses/by-nc-sa/ 3.0/deed.en_US). Last 
%%% accessed: 02-11-2018) and that we collected by hand from 
%%% RealClearPolitics 
%%% (https://www.realclearpolitics.com/epolls/latest_polls/elections/, 
%%% 2018. Last accessed: 22-12-2018).
%%%
%%% This code creates two .csv files (RepPercentages[].csv and 
%%% DemPercentages[].csv, where [] contains one of the following:
%%% 2018s, 2018g, 2016s, 2016g, 2016p, 2012s, 2012g, 2012p
%%% based on what is passed to this function in the 'election' variable).
%%% These files contain the polling data in the format we use to fit 
%%% model parameters (binned by 30-day increment by state or superstate).
%%% These files are needed to run parameterFitting.R.
%%%
%%% Commands used to generate formatted poll data in Nov for each election
%%%
%%% 2018 Senate elections:
%%% formattingPollData(11, '2018s')
%%%
%%% As a demo to check the code, note that the files resulting from the
%%% above line have already been saved in this folder under
%%% RepPercentages2018s_demo.csv and DemPercentages2018s_demo.csv.
%%% 
%%% 2018 governor elections:
%%% formattingPollData(11, '2018g')
%%%
%%% As a demo to check the code, note that the files resulting from the
%%% above line have already been saved in this folder under
%%% RepPercentages2018g_demo.csv and DemPercentages2018g_demo.csv.
%%%
%%% 2016 Senate elections:
%%% formattingPollData(11, '2016s')
%%%
%%% 2016 governor elections:
%%% formattingPollData(11, '2016g')
%%%
%%% 2016 president elections:
%%% formattingPollData(11, '2016p')
%%%
%%% 2012 Senate elections:
%%% formattingPollData(11, '2012s')
%%%
%%% 2012 governor elections:
%%% formattingPollData(11, '2012g')
%%%
%%% 2012 president elections:
%%% formattingPollData(11, '2012p')
%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [] = formattingPollData(numMonths, election)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Establishing values related to the number of state races for each
%%% election. Note that numRed and numBlue (the number of states in our Red
%%% and Blue superstates, respectively) are different in some cases than
%%% the values reported in Table S1. This is because some
%%% states included in these superstates have no pulling data, and we need
%%% to ignore these states when formatting the polling data here.
if strcmp(election, '2018s') == 1       % 2018 Senate election
    
    numElecs = 34;                                          % number of state races considered in our polling data file (note that we have already excluded CA)
    numRed = 3-1*(numMonths < 11);                          % number of states in the Red superstate (excluding states without polling data)
    numBlue = 13-1*(numMonths < 11)-3*(numMonths < 10) - 2*(numMonths < 9) - 2*(numMonths < 8);     % number of states in the Blue superstate (excluding states without polling data)
    numM = 16 - 1*(numMonths < 8);                                              % number of states and superstates that we forecast
    numIndividual = numM - 2;                               % number of states forecast individually
    numOut = numElecs - numRed - numBlue - numIndividual;   % number of states that we leave out (due to lack of polling data or, in the case of NM, due to the polling data including 3 candidates with large vote shares)
    
    %%% Superstate and state races that we forecast
    if numMonths >= 8
        states = ["Red","Blue","AZ","FL","IN","MNS","MO","MT","NV","NJ","ND","OH","TN","TX","WV","WI"];
    else
        states = ["Red","Blue","AZ","FL","IN","MO","MT","NV","NJ","ND","OH","TN","TX","WV","WI"];
    end
    
    %%% Importing information about state races (state abbreviations,
    %%% population sizes, number of polls, and state color). Note that
    %%% state color is 'R' if a state is assigned to our Red superstate,
    %%% 'B' if assigned to our Blue superstate, and 'U' otherwise.
    [stateNumData, stateTextData] = xlsread('/dataset_Index.xlsx',1,'A2:F35');  % page 1 of the index file contains 2018s data
    
elseif strcmp(election, '2018g') == 1   % 2018 governor election
    
    numElecs = 36;                                          % number of state races
    numRed = 9;                                             % number of states in the Red superstate (excluding states without polling data)
    numBlue = 10;                                           % number of states in the Blue superstate (excluding states without polling data)
    numM = 15;                                              % number of states and superstates that we forecast
    numIndividual = numM - 2;                               % number of states forecast individually
    numOut = numElecs - numRed - numBlue - numIndividual;   % number of states left out due to lack of polling data
    
    %%% Superstate and state races that we forecast
    states = ["Red","Blue","AK","CT","FL","GA","IA","KS","ME","NV","OH","OK","OR","SD","WI"];
    
    %%% Importing information about state races
    [stateNumData, stateTextData] = xlsread('/dataset_Index.xlsx',2,'A2:F37');  % page 2 of the index file contains 2018g data
    
elseif strcmp(election, '2016s') == 1   % 2016 Senate election
    
    numElecs = 33;                                          % number of state races considered in our polling data file (note that we have already excluded CA)
    numRed = 13;                                            % number of states in the Red superstate (excluding states without polling data)
    numBlue = 8;                                            % number of states in the Blue superstate (excluding states without polling data)
    numM = 14;                                              % number of states and superstates that we forecast
    numIndividual = numM - 2;                               % number of states forecast individually
    numOut = numElecs - numRed - numBlue - numIndividual;   % number of states left out due to lack of polling data
    
    %%% Superstate and state races that we forecast
    states = ["Red","Blue","AZ","FL","IL","IN","LA","MO","NV","NH","NC","OH","PA","WI"];
    
    %%% Importing information about state races
    [stateNumData, stateTextData] = xlsread('/dataset_Index.xlsx',3,'A2:F34');  % page 3 of the index file contains 2016s data
    
elseif strcmp(election, '2016g') == 1   % 2016 governor election
    
    numElecs = 12;                                          % number of state races considered in our polling data file
    numRed = 0;                                             % no Red superstate
    numBlue = 0;                                            % no Blue superstate
    numM = 12;                                              % number of states that we forecast
    numIndividual = numM;                                   % all states are forecast individually here
    numOut = numElecs - numRed - numBlue - numIndividual;   % all states have polling data here
    
    %%% Superstate and state races that we forecast
    states = ["DE","IN","MO","MT","NH","NC","ND","OR","UT","VT","WA","WV"];
    
    %%% Importing information about state races
    [stateNumData, stateTextData] = xlsread('/dataset_Index.xlsx',4,'A2:F13');  % page 4 of the index file contains 2016g data
    
elseif strcmp(election, '2016p') == 1   % 2016 president election
    
    numElecs = 51;                                          % number of state races (+ DC)
    numRed = 23;                                            % number of states in Red superstate
    numBlue = 16;                                           % number of states in Blue superstate
    numM = 14;                                              % number of states and superstates that we forecast
    numIndividual = numM - 2;                               % number of states that we forecast individually
    numOut = numElecs - numRed - numBlue - numIndividual;   % all states have polling data here
    
    %%% Superstate and state races that we forecast
    states = ["Red","Blue","CO","FL","IA","MI","MN","NV","NH","NC","OH","PA","VA","WI"];
    
    %%% Importing information about state races
    [stateNumData, stateTextData] = xlsread('/dataset_Index.xlsx',5,'A2:F52');  % page 5 of the index file contains 2016p data
    
elseif strcmp(election, '2012s') == 1   % 2012 Senate election
    
    numElecs = 28;                                          % number of state races (excluding ME and VT)
    numRed = 4;                                             % number of states in Red superstate                                 
    numBlue = 11;                                           % number of states in Blue superstate
    numM = 15;                                              % number of states and superstates that we forecast
    numIndividual = numM - 2;                               % number of states that we forecast individually
    numOut = numElecs - numRed - numBlue - numIndividual;   % all states have polling data here
    
    %%% Superstate and state races that we forecast
    states = ["Red","Blue","AZ","CT","FL","IN","MA","MO","MT","NV","ND","OH","PA","VA","WI"];
    
    %%% Importing information about state races
    [stateNumData, stateTextData] = xlsread('/dataset_Index.xlsx',6,'A2:F29');  % page 6 of the index file contains 2012s data
    
    
elseif strcmp(election, '2012g') == 1   % 2012 governor election
    
    numElecs = 9;                                           % number of state races considered in our polling data file
    numRed = 0;                                             % no Red superstate
    numBlue = 0;                                            % no Blue superstate
    numM = 9;                                               % number of states that we forecast
    numIndividual = numM;                                   % all states are forecast individually here
    numOut = numElecs - numRed - numBlue - numIndividual;   % all states have polling data here

    %%% Superstate and state races that we forecast
    states = ["IN","MO","MT","NH","NC","ND","UT","VT","WA"];
    
    %%% Importing information about state races
    [stateNumData, stateTextData] = xlsread('/dataset_Index.xlsx',7,'A2:F10');  % page 7 of the index file contains 2012g data
    
    
elseif strcmp(election, '2012p') == 1   % 2012 president election

    numElecs = 51;                                          % number of state races (+ DC)
    numRed = 19;                                            % number of states in Red superstate (excluding 4 states without polling data)
    numBlue = 15;                                           % number of states in Blue superstate (excluding 1 state without polling data)
    numM = 14;                                              % number of states and superstates that we forecast
    numIndividual = numM - 2;                               % number of states that we forecast individually
    numOut = numElecs - numRed - numBlue - numIndividual;   % all states have polling data here
    
    %%% Superstate and state races that we forecast
    states = ["Red","Blue","CO","FL","IA","MI","MN","NV","NH","NC","OH","PA","VA","WI"];
    
    %%% Importing information about state races
    [stateNumData, stateTextData] = xlsread('/dataset_Index.xlsx',8,'A2:F52');  % page 8 of the index file contains 2012p data
    
    
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Initializing some arrays so they do not grow in size (note that we
%%% initialize with 500 by numElecs arrays because no states have more than
%%% 500 polls; we later index by the number of polls per state, so that the
%%% extra values have no influence).
dates = 4000*ones(500,numElecs);      % matrix to hold the days until the election for different polls (we initialize all the values to 4000 for indexing purposes so that they do not fall into our monthly bins later on)
RepPercent = zeros(500,numElecs);     % matrix to hold the percentage projected to vote Republican for each poll and state
DemPercent = zeros(500,numElecs);     % matrix to hold the percentage projected to vote Democrat for each poll and state
stateNames = strings(1,numElecs);     % matrix to hold abbreviated state names in the appropriate order

%%% Parameters and values taken from data file
statesAbbr = stateTextData(:,2);      % abbreviated state name
numPolls = stateNumData(:,2);         % number of polls for given state
stateColor = stateTextData(:,5);      % state color ('R' or 'B') if it belongs in a superstate ('U' otherwise)
population = stateNumData(:,4);       % voting-age population size for given state
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% This if-else statement is used to access the polling data for each
%%% election. We collect the beginning and end date of each poll, transform
%%% these dates to a distance in days from the election day (note that
%%% these distances are negative and extend backward from 0), and save them
%%% in our dates matrix. We also collect the Republican and Democrat poll
%%% percentages and save them in the red and blue arrays, respectively. We
%%% also keep track of the states associated with this data in order in
%%% stateNames.
if strcmp(election, '2018s') == 1       % 2018 Senate election
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% This for loop goes through the polling data by state in the excel
    %%% file and assigns each poll a time point (the number of days from
    %%% that poll to the appropriate election day).
    for i = 1:numElecs
        
        %%% If-else statement to buypass states that do not have any polls
        if i == 5 ||  i == 17  || i == 34 || i == 20
            %%% Hawaii, Nebraska, and Wyoming have no polls in our data set
            %%% for the 2018 Senate election. We also filter out NM (index
            %%% 20) because our polling data for this state included 3
            %%% candidates projected to have large shares of the vote.
        elseif (i == 13 || i == 29) && numMonths < 11
            %%% Mississippi does not have any polls outside of one month
            %%% from election day; neither does Vermont
        elseif (i == 8 || i == 25 || i == 31) && numMonths < 10
            %%% MD, RI, WA do not have any polls outside of two months from
            %%% election day
        elseif (i == 2 || i == 11) && numMonths < 9
            %%% CT and MN do not have any polls outside of two months from
            %%% election day
        elseif (i == 3 || i == 7 || i == 12) && numMonths < 8
            %%% DE and ME have no polls outside of 3 months from the
            %%% election; neither does Minnesota special
        else
            curr_state = statesAbbr(i);
            stateNames(i) = char(curr_state);
            [resultsN, ~] = xlsread('dataset_SenatePolls2018.xlsx',i,'A2:F500');
            RepPercent(1:numPolls(i),i) = resultsN(1:numPolls(i),1);       % Republican percentage projected by poll
            DemPercent(1:numPolls(i),i) = resultsN(1:numPolls(i),2);      % Democrate percentage projected by poll
            endDates = resultsN(1:numPolls(i),5);                   % poll end date
            startDates = resultsN(1:numPolls(i),6);                 % poll start date
            startDatesT = datevec(startDates(1:numPolls(i)) + datenum('30-Dec-1899'));
            endDatesT = datevec(endDates(1:numPolls(i)) + datenum('30-Dec-1899'));
            electionDay = repmat(datevec('11/06/18', 'mm/dd/yy'),1,numPolls(i));    % election day
            datesSM = (etime(startDatesT,electionDay))./(60*60*24);         % calculating the distance from election day to the poll start date and converting to days
            datesFM = (etime(endDatesT,electionDay))./(60*60*24);           % same for poll end date (note these distances are negative)
            datesMid = floor((datesSM + datesFM)/2);                        % averaging to obtain the distance in days from the poll date to election day
            numPolls(i) = sum(datesMid < (numMonths - 11)*30);             % number of polls in our date range
            dates(1:numPolls(i),i) = datesMid(datesMid < (numMonths - 11)*30);          % poll dates in our date range
            resultsNR = resultsN(:,1);
            resultsND = resultsN(:,2);
            RepPercent(1:numPolls(i),i) = resultsNR(datesMid < (numMonths - 11)*30);       % Republican percentage projected by poll in our date range
            DemPercent(1:numPolls(i),i) = resultsND(datesMid < (numMonths - 11)*30);      % Democrate percentage projected by poll in our date range
        end
    end
    
elseif strcmp(election, '2018g') == 1   % 2018 governor election
    
    for i = 1:numElecs
        
        %%% If-else statement to buypass states that do not have any polls
        if i == 1 ||  i == 11  || i == 21 || i == 36
            %%% Alabama, Nebraska, Idaho, and Wyoming have no polls in our
            %%% data set for the 2018 governor election.
        else
            curr_state = statesAbbr(i);
            stateNames(i) = char(curr_state);
            [resultsN, ~] = xlsread('/dataset_GovernorPolls2018.xlsx',i,'A2:F500');
            RepPercent(1:numPolls(i),i) = resultsN(1:numPolls(i),1);
            DemPercent(1:numPolls(i),i) = resultsN(1:numPolls(i),2);
            endDates = resultsN(1:numPolls(i),5);
            startDates = resultsN(1:numPolls(i),6);
            startDatesT = datevec(startDates(1:numPolls(i)) + datenum('30-Dec-1899'));
            endDatesT = datevec(endDates(1:numPolls(i)) + datenum('30-Dec-1899'));
            electionDay = repmat(datevec('11/06/18', 'mm/dd/yy'),1,numPolls(i));
            datesSM = (etime(startDatesT,electionDay))./(60*60*24);
            datesFM = (etime(endDatesT,electionDay))./(60*60*24);
            dates(1:numPolls(i),i) = floor((datesSM + datesFM)/2);
        end
    end
    
elseif strcmp(election, '2016s') == 1   % 2016 Senate election
    
    %%% Note that all states in our polling data set have polls for the
    %%% 2016 Senate, 2016 governor, 2016 president, 2012 Senate, and 2012
    %%% governor races, so no if-else statement is included here.
    for i = 1:numElecs
        curr_state = statesAbbr(i);
        stateNames(i) = char(curr_state);
        [resultsN, ~] = xlsread('/dataset_SenatePolls2016.xlsx',i,'A2:F500');
        RepPercent(1:numPolls(i),i) = resultsN(1:numPolls(i),1);
        DemPercent(1:numPolls(i),i) = resultsN(1:numPolls(i),2);
        endDates = resultsN(1:numPolls(i),5);
        startDates = resultsN(1:numPolls(i),6);
        startDatesT = datevec(startDates(1:numPolls(i)) + datenum('30-Dec-1899'));
        endDatesT = datevec(endDates(1:numPolls(i)) + datenum('30-Dec-1899'));
        electionDay = repmat(datevec('11/08/16', 'mm/dd/yy'),1,numPolls(i));
        datesSM = (etime(startDatesT,electionDay))./(60*60*24);
        datesFM = (etime(endDatesT,electionDay))./(60*60*24);
        dates(1:numPolls(i),i) = floor((datesSM + datesFM)/2);
    end

elseif strcmp(election, '2016g') == 1   % 2016 governor election
    
    for i = 1:numElecs
        curr_state = statesAbbr(i);
        stateNames(i) = char(curr_state);
        [resultsN, ~] = xlsread('/dataset_GovernorPolls2016.xlsx',i,'A2:F500');
        RepPercent(1:numPolls(i),i) = resultsN(1:numPolls(i),1);
        DemPercent(1:numPolls(i),i) = resultsN(1:numPolls(i),2);
        endDates = resultsN(1:numPolls(i),5);
        startDates = resultsN(1:numPolls(i),6);
        startDatesT = datevec(startDates(1:numPolls(i)) + datenum('30-Dec-1899'));
        endDatesT = datevec(endDates(1:numPolls(i)) + datenum('30-Dec-1899'));
        electionDay = repmat(datevec('11/08/16', 'mm/dd/yy'),1,numPolls(i));
        datesSM = (etime(startDatesT,electionDay))./(60*60*24);
        datesFM = (etime(endDatesT,electionDay))./(60*60*24);
        dates(1:numPolls(i),i) = floor((datesSM + datesFM)/2);
    end
    
elseif strcmp(election, '2016p') == 1   % 2016 president election
    
    for i = 1:numElecs
        curr_state = statesAbbr(i);
        stateNames(i) = char(curr_state);
        [resultsN, ~] = xlsread('/dataset_PresidentPolls2016.xlsx',i,'A2:F500');
        RepPercent(1:numPolls(i),i) = resultsN(1:numPolls(i),1);
        DemPercent(1:numPolls(i),i) = resultsN(1:numPolls(i),2);
        endDates = resultsN(1:numPolls(i),5);
        startDates = resultsN(1:numPolls(i),6);
        startDatesT = datevec(startDates(1:numPolls(i)) + datenum('30-Dec-1899'));
        endDatesT = datevec(endDates(1:numPolls(i)) + datenum('30-Dec-1899'));
        electionDay = repmat(datevec('11/08/16', 'mm/dd/yy'),1,numPolls(i));
        datesSM = (etime(startDatesT,electionDay))./(60*60*24);
        datesFM = (etime(endDatesT,electionDay))./(60*60*24);
        dates(1:numPolls(i),i) = floor((datesSM + datesFM)/2);
    end
    
elseif strcmp(election, '2012s') == 1   % 2012 Senate election
    
    for i = 1:numElecs
        curr_state = statesAbbr(i);
        stateNames(i) = char(curr_state);
        [resultsN, ~] = xlsread('/dataset_SenatePolls2012.xlsx',i,'A2:F500');
        RepPercent(1:numPolls(i),i) = resultsN(1:numPolls(i),1);
        DemPercent(1:numPolls(i),i) = resultsN(1:numPolls(i),2);
        endDates = resultsN(1:numPolls(i),5);
        startDates = resultsN(1:numPolls(i),6);
        startDatesT = datevec(startDates(1:numPolls(i)) + datenum('30-Dec-1899'));
        endDatesT = datevec(endDates(1:numPolls(i)) + datenum('30-Dec-1899'));
        electionDay = repmat(datevec('11/06/12', 'mm/dd/yy'),1,numPolls(i));
        datesSM = (etime(startDatesT,electionDay))./(60*60*24);
        datesFM = (etime(endDatesT,electionDay))./(60*60*24);
        dates(1:numPolls(i),i) = floor((datesSM + datesFM)/2);
    end
    
elseif strcmp(election, '2012g') == 1   % 2012 governor election
    
    for i = 1:numElecs
        curr_state = statesAbbr(i);
        stateNames(i) = char(curr_state);
        [resultsN, ~] = xlsread('/dataset_GovernorPolls2012.xlsx',i,'A2:F500');
        RepPercent(1:numPolls(i),i) = resultsN(1:numPolls(i),1);
        DemPercent(1:numPolls(i),i) = resultsN(1:numPolls(i),2);
        endDates = resultsN(1:numPolls(i),5);
        startDates = resultsN(1:numPolls(i),6);
        startDatesT = datevec(startDates(1:numPolls(i)) + datenum('30-Dec-1899'));
        endDatesT = datevec(endDates(1:numPolls(i)) + datenum('30-Dec-1899'));
        electionDay = repmat(datevec('11/06/12', 'mm/dd/yy'),1,numPolls(i));
        datesSM = (etime(startDatesT,electionDay))./(60*60*24);
        datesFM = (etime(endDatesT,electionDay))./(60*60*24);
        dates(1:numPolls(i),i) = floor((datesSM + datesFM)/2);
    end
    
elseif strcmp(election, '2012p') == 1   % 2012 president election
    
    for i = 1:numElecs
        %%% If-else statement to buypass states that do not have any polls
        if (i == 2 || i == 9 || i == 18 || i == 26 || i == 51) && strcmp(election,'2012p')
            %%% Alaska, Delaware, Missouri, Kentucky, and Wyoming have no
            %%% polls in our data set for the 2012 presidential election.
        else
            curr_state = statesAbbr(i);
            stateNames(i) = char(curr_state);
            [resultsN, ~] = xlsread('/dataset_PresidentPolls2012.xlsx',i,'A2:F500');
            RepPercent(1:numPolls(i),i) = resultsN(1:numPolls(i),1);
            DemPercent(1:numPolls(i),i) = resultsN(1:numPolls(i),2);
            endDates = resultsN(1:numPolls(i),5);
            startDates = resultsN(1:numPolls(i),6);
            startDatesT = datevec(startDates(1:numPolls(i)) + datenum('30-Dec-1899'));
            endDatesT = datevec(endDates(1:numPolls(i)) + datenum('30-Dec-1899'));
            electionDay = repmat(datevec('11/06/12', 'mm/dd/yy'),1,numPolls(i));
            datesSM = (etime(startDatesT,electionDay))./(60*60*24);
            datesFM = (etime(endDatesT,electionDay))./(60*60*24);
            dates(1:numPolls(i),i) = floor((datesSM + datesFM)/2);
        end
    end
    
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Matrix initializations (used for collecting states into those that
%%% appear in our Red superstate, those that appear in our Blue superstate,
%%% and those that we forecast individually)
totalPopRed = 0;                                    % variable to hold the number of voters in our Red superstate (for the states with polling data)
totalPopBlue = 0;                                   % same for the Blue superstate
redPolls = zeros(1,numRed);                         % matrix to hold the number of polls per state in the Red superstate
counterR = 1;                                       % indexing
counterB = 1;                                       % indexing
bluePolls = zeros(1,numBlue);                       % matrix to hold the number of polls per state in the Blue superstate
blueSize = zeros(1,numBlue);                        % matrix to hold the number of voters per state in the Blue superstate
redSize = zeros(1,numRed);                          % matrix to hold the number of voters per state in the Red superstate
Red = -4000*ones(1000,3,numRed);                    % 3D array to hold the poll results (date, percent Rep, and percent Dem) for the states in the Red superstate
Blue = -4000*ones(1000,3,numBlue);                  % same for the Blue superstate
Individual = -4000*ones(1000,3,numIndividual);      % same format for the states forecast individually
individualPollNumbers = zeros(1,numIndividual);     % number of polls for each state in the Individual array
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Data formatting. For each state race in our index excel document, we
%%% pull out the states that will appear in the Red superstate and Blue
%%% superstate. We also filter out states for which there is no polling
%%% data and collect the states that we will forecast individually.
for i = 1:numElecs
    
    currColor = char(stateColor(i));    % state color ('R' for states in the Red superstate, 'B' for states in the Blue superstate, and 'U' for everything else
    currState = stateNames(i);          % abbreviated state name
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Collecting the states for our superstates - if superstates are
    %%% used, we assign those states a color ('R' for Red and 'B' for Blue)
    if strcmp(currColor,'R') == 1       % collecting states in our Safe Red superstate
        
        %%% For some elections (namely 2018 Senate, 2018 governor, and 2012
        %%% president), some of the states in our excel file of polling
        %%% data do not have any polls. We filter these states out using
        %%% the indices in the excel file.
        if (i == 5 ||  i == 17  || i == 34 || i == 20) && strcmp(election,'2018s')
            %%% Hawaii, Nebraska, and Wyoming have no polls in our data set
            %%% for the 2018 Senate election. We also filter out NM (index
            %%% 20) because our polling data for this state included 3
            %%% candidates projected to have large shares of the vote.
        elseif (i == 13 || i == 29) && numMonths < 11 && strcmp(election,'2018s')
            %%% Mississippi does not have any polls outside of one month
            %%% from election day; neither do Vermont or Washington
        elseif (i == 8 || i == 25 || i == 31) && numMonths < 10 && strcmp(election,'2018s')
            %%% MD, RI, WA do not have any polls outside of two months from
            %%% election day
        elseif (i == 2 || i == 11) && numMonths < 9 && strcmp(election,'2018s')
            %%% CT and MN do not have any polls outside of two months from
            %%% election day
        elseif (i == 3 || i == 7 || i == 12) && numMonths < 8 && strcmp(election,'2018s')
            %%% DE and ME have no polls outside of 3 months from the
            %%% election; neither does Minnesota special
        elseif (i == 1 ||  i == 11  || i == 21 || i == 36) && strcmp(election,'2018g')
            %%% Alabama, Nebraska, Idaho, and Wyoming have no polls in our
            %%% data set for the 2018 governor election.
        elseif (i == 2 || i == 9 || i == 18 || i == 26 || i == 51) && strcmp(election,'2012p')
            %%% Alaska, Delaware, Missouri, Kentucky, and Wyoming have no
            %%% polls in our data set for the 2012 presidential election.
        else
            totalPopRed = totalPopRed + population(i);
            redPolls(counterR) = numPolls(i);
            Red(1:numPolls(i),:,counterR) = [dates(1:numPolls(i),i), RepPercent(1:numPolls(i),i), DemPercent(1:numPolls(i),i)];
            redSize(counterR) = population(i);
            counterR = counterR + 1;
        end
        
    elseif strcmp(currColor,'B') == 1       % collecting states in our Safe Blue superstate
        
        if (i == 5 ||  i == 17  || i == 34 || i == 20) && strcmp(election,'2018s')
            %%% Hawaii, Nebraska, and Wyoing have no polls in our data set
            %%% for the 2018 Senate election. We also filter out NM (index
            %%% 20) because our polling data for this state included 3
            %%% candidates projected to have large shares of the vote.
        elseif (i == 13 || i == 29) && numMonths < 11 && strcmp(election,'2018s')
            %%% Mississippi does not have any polls outside of one month
            %%% from election day; neither do Vermont or Washington
        elseif (i == 8 || i == 25 || i == 31) && numMonths < 10 && strcmp(election,'2018s')
            %%% MD, RI, WA do not have any polls outside of two months from
            %%% election day
        elseif (i == 2 || i == 11) && numMonths < 9 && strcmp(election,'2018s')
            %%% CT and MN do not have any polls outside of two months from
            %%% election day
        elseif (i == 3 || i == 7 || i == 12) && numMonths < 8 && strcmp(election,'2018s')
            %%% DE and ME have no polls outside of 3 months from the
            %%% election; neither does Minnesota special
        elseif (i == 1 ||  i == 11  || i == 21 || i == 36) && strcmp(election,'2018g')
            %%% Alabama, Nebraska, Idaho, and Wyoming have no polls in our
            %%% data set for the 2018 governor election.
        elseif (i == 2 || i == 9 || i == 18 || i == 26 || i == 51) && strcmp(election,'2012p')
            %%% Alaska, Delaware, Missouri, Kentucky, and Wyoming have no
            %%% polls in our data set for the 2012 presidential election
        else
            bluePolls(counterB) = numPolls(i);
            Blue(1:numPolls(i),:,counterB) = [dates(1:numPolls(i),i), RepPercent(1:numPolls(i),i), DemPercent(1:numPolls(i),i)];
            blueSize(counterB) = population(i);
            counterB = counterB + 1;
            totalPopBlue = totalPopBlue + population(i);
        end
        
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Collecting the states we forecst individually, listed alphabetically
    for j = 1:numIndividual
        if strcmp(currState, states(j+(numM-numIndividual))) == 1
            alphabeticalIndex = find(states == currState);
            Individual(1:numPolls(i),:,alphabeticalIndex-(numM-numIndividual)) = [dates(1:numPolls(i),i), RepPercent(1:numPolls(i),i), DemPercent(1:numPolls(i),i)];
            individualPollNumbers(alphabeticalIndex-(numM-numIndividual)) = numPolls(i);
        end
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Saving the poll dates, Republican percentages, and Democrat percentages
%%% in order of: states in our Red superstate, states in our Blue
%%% superstate, and remaining states forecast individually listed
%%% alphabetically by name,
orderedDates = [squeeze(Red(:,1,1:numRed)),squeeze(Blue(:,1,1:numBlue)),squeeze(Individual(:,1,1:numIndividual))];
orderedRep = [squeeze(Red(:,2,1:numRed)),squeeze(Blue(:,2,1:numBlue)),squeeze(Individual(:,2,1:numIndividual))];
orderedDem = [squeeze(Red(:,3,1:numRed)),squeeze(Blue(:,3,1:numBlue)),squeeze(Individual(:,3,1:numIndividual))];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Next we want to bin the dates assigned to polls by month (assuming
%%% months of 30 days extending backward from the election). Note that
%%% identifyNov selects dates within 30 days of election (so it spans some
%%% of Oct. and Nov.) - the same naming convention applies to datesOct,
%%% datesSep, etc.
%%% This portion of the code produces logicals (vectors of 1's and 0's)
%%% that we use to assign the polls to each bin in time so that we can
%%% average across each bin to obtain one polling data point per 30-day
%%% increment.
identifyNov = (orderedDates >= -30).*(orderedDates <= 0);      % within 30 days from election day (Replace <= 0 with < - 15 to simulate Governor 2018 forecasts 2 weeks out)
identifyOct = (orderedDates >= -60).*(orderedDates < -30);     % 30-60 days from election day
identifySep = (orderedDates >= -90).*(orderedDates < -60);     % 60-90 days from election day
identifyAug = (orderedDates >= -120).*(orderedDates < -90);    % 90-120 days from election day
identifyJul = (orderedDates >= -150).*(orderedDates < -120);   % 120-150 days from election day
identifyJun = (orderedDates >= -180).*(orderedDates < -150);   % 150-180 days from election day
identifyMay = (orderedDates >= -210).*(orderedDates < -180);   % 180-210 days from election day
identifyApr = (orderedDates >= -240).*(orderedDates < -210);   % 210-240 days from election day
identifyMar = (orderedDates >= -270).*(orderedDates < -240);   % 240-270 days from election day
identifyFeb = (orderedDates >= -300).*(orderedDates < -270);   % 270-300 days from election day
if strcmp(election,'2012p') == 1
    %%% Special case mentioned in Methods for states in the 2012
    %%% presidential election (we use polls between 400 and 300 days for
    %%% our first bin rather than 300 to 330 days out)
    identifyJan = (orderedDates >= -400).*(orderedDates < -300);   % 300-400 days from election day
else
    identifyJan = (orderedDates >= -330).*(orderedDates < -300);   % 300-330 days from election day
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Binning polls by month and taking the average within each 30-day
%%% period. For exmaple, to average the Republican polling data in
%%% November, we multiply the Republican percentages by our Nov logicals
%%% from the previous step to select only polls held within 30 days of the
%%% election (the remaining values are set to 0). We then sum up these
%%% values and divide by the number of polls in the November bin to get the
%%% mean.
RepNov = sum(orderedRep.*(identifyNov))./sum(identifyNov);
RepOct = sum(orderedRep.*(identifyOct))./sum(identifyOct);
RepSep = sum(orderedRep.*(identifySep))./sum(identifySep);
RepAug = sum(orderedRep.*(identifyAug))./sum(identifyAug);
RepJul = sum(orderedRep.*(identifyJul))./sum(identifyJul);
RepJun = sum(orderedRep.*(identifyJun))./sum(identifyJun);
RepMay = sum(orderedRep.*(identifyMay))./sum(identifyMay);
RepApr = sum(orderedRep.*(identifyApr))./sum(identifyApr);
RepMar = sum(orderedRep.*(identifyMar))./sum(identifyMar);
RepFeb = sum(orderedRep.*(identifyFeb))./sum(identifyFeb);
RepJan = sum(orderedRep.*(identifyJan))./sum(identifyJan);

DemNov = sum(orderedDem.*(identifyNov))./sum(identifyNov);
DemOct = sum(orderedDem.*(identifyOct))./sum(identifyOct);
DemSep = sum(orderedDem.*(identifySep))./sum(identifySep);
DemAug = sum(orderedDem.*(identifyAug))./sum(identifyAug);
DemJul = sum(orderedDem.*(identifyJul))./sum(identifyJul);
DemJun = sum(orderedDem.*(identifyJun))./sum(identifyJun);
DemMay = sum(orderedDem.*(identifyMay))./sum(identifyMay);
DemApr = sum(orderedDem.*(identifyApr))./sum(identifyApr);
DemMar = sum(orderedDem.*(identifyMar))./sum(identifyMar);
DemFeb = sum(orderedDem.*(identifyFeb))./sum(identifyFeb);
DemJan = sum(orderedDem.*(identifyJan))./sum(identifyJan);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Additional processing to obtain 11 data points per state.
%%% This portion of the code handles a few special cases: (1) no polls for
%%% a given state in the first months of the year; (2) no polls for a given
%%% state in the months leading up to election day; (3) no polls in some
%%% intermediate months; or (4) only one month with polling data. In the
%%% third case, we fill in any missing time points by interpolating. In the
%%% first and second cases, we cannot use interpolation. Instead, in the
%%% first case, we assign any early months missing polls the earliest
%%% possible polling data (e.g. if Jan and Feb are missing polls but March
%%% has polls, we assign Jan and Feb March's polling data average).
%%% Similarly, in the second case, we assign any late months missing polls
%%% the latest possible polling data (e.g. if Nov is missing polls but Oct
%%% has polls, we assign Nov Oct's polling data average). For case (4), we
%%% assign all months the data value of the single month that has polling
%%% data.
formattedRepublicanData = zeros(numMonths,numElecs);
formattedDemocratData = zeros(numMonths,numElecs);
numPollsPerState = [redPolls,bluePolls,individualPollNumbers];


%%% For loop to go through every election by state (leaving out any states
%%% not forecasted)
for i = 1:numElecs-numOut
    
    %%% Time points for the polling data of the current state
    currDates = orderedDates(1:numPollsPerState(i),i);
    %%% We consider the earliest polling data within 330 days of the
    %%% election. We also calculate the most recent poll, closest to the
    %%% election day. (Note that currDates are all negative, and the more
    %%% negative, the further from the election day.
    if strcmp(election,'2012p') == 1 && sum(currDates >= -330) == 0
        %%% Special case mentioned in Methods for states in the 2012
        %%% presidential election with no polling data within 330 days of
        %%% the election but polls within 400 days.
        earliestPollDate = min(currDates(currDates >= -400));
    else
        earliestPollDate = min(currDates(currDates >= -330));
    end
    %%% Calculating the most recent poll date for the number of months
    %%% considered
    mostRecentPollDate = max(currDates(currDates < (numMonths - 11)*30 ));   %to simulate Governor 2018 forecasts about 2 weeks out, replace (numMonths-11)*30 with -15
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% This if-else statement handles cases (1), (2), and (4) above.
    if numPollsPerState(i) == 1     % only one bin with polling data (case (4) above)
        
        %%% If there is only one bin with polling data, we assign all bins
        %%% this data point
        onlyValueRep = max([RepNov(i),RepOct(i),RepSep(i),RepAug(i),RepJul(i),RepJun(i),RepMay(i),RepApr(i),RepMar(i),RepFeb(i),RepJan(i)]);
        RepNov(i) = onlyValueRep;
        RepOct(i) = onlyValueRep;
        RepSep(i) = onlyValueRep;
        RepAug(i) = onlyValueRep;
        RepJul(i) = onlyValueRep;
        RepJun(i) = onlyValueRep;
        RepMay(i) = onlyValueRep;
        RepApr(i) = onlyValueRep;
        RepMar(i) = onlyValueRep;
        RepFeb(i) = onlyValueRep;
        RepJan(i) = onlyValueRep;
        
        onlyValueDem = max([DemNov(i),DemOct(i),DemSep(i),DemAug(i),DemJul(i),DemJun(i),DemMay(i),DemApr(i),DemMar(i),DemFeb(i),DemJan(i)]);
        DemNov(i) = onlyValueDem;
        DemOct(i) = onlyValueDem;
        DemSep(i) = onlyValueDem;
        DemAug(i) = onlyValueDem;
        DemJul(i) = onlyValueDem;
        DemJun(i) = onlyValueDem;
        DemMay(i) = onlyValueDem;
        DemApr(i) = onlyValueDem;
        DemMar(i) = onlyValueDem;
        DemFeb(i) = onlyValueDem;
        DemJan(i) = onlyValueDem;
        
    else        % there is more than 1 poll - now we handle cases (1) and (2) above
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% If-else statement to handle case (1) -- polls missing from the
        %%% early months of the election year.
        if earliestPollDate >= - 30 && earliestPollDate <= 0        % no polls prior to 30 days before the election
            %%% If there are no polls prior to 30 days before the election,
            %%% we assign the November data to all other bins.
            RepOct(i) = RepNov(i);
            RepSep(i) = RepOct(i);
            RepAug(i) = RepSep(i);
            RepJul(i) = RepAug(i);
            RepJun(i) = RepJul(i);
            RepMay(i) = RepJun(i);
            RepApr(i) = RepMay(i);
            RepMar(i) = RepApr(i);
            RepFeb(i) = RepMar(i);
            RepJan(i) = RepFeb(i);
            
            DemOct(i) = DemNov(i);
            DemSep(i) = DemOct(i);
            DemAug(i) = DemSep(i);
            DemJul(i) = DemAug(i);
            DemJun(i) = DemJul(i);
            DemMay(i) = DemJun(i);
            DemApr(i) = DemMay(i);
            DemMar(i) = DemApr(i);
            DemFeb(i) = DemMar(i);
            DemJan(i) = DemFeb(i);
            
        elseif earliestPollDate >= - 60 && earliestPollDate < -30   % polls in the first 30 days, but no polls prior to 60 days before the election
            %%% If there are polls in the first two bins (30 days prior,
            %%% and between 30 and 60 days prior, to the election), we
            %%% assign the October data to all other bins.
            RepSep(i) = RepOct(i);
            RepAug(i) = RepSep(i);
            RepJul(i) = RepAug(i);
            RepJun(i) = RepJul(i);
            RepMay(i) = RepJun(i);
            RepApr(i) = RepMay(i);
            RepMar(i) = RepApr(i);
            RepFeb(i) = RepMar(i);
            RepJan(i) = RepFeb(i);
            
            DemSep(i) = DemOct(i);
            DemAug(i) = DemSep(i);
            DemJul(i) = DemAug(i);
            DemJun(i) = DemJul(i);
            DemMay(i) = DemJun(i);
            DemApr(i) = DemMay(i);
            DemMar(i) = DemApr(i);
            DemFeb(i) = DemMar(i);
            DemJan(i) = DemFeb(i);
            
        elseif earliestPollDate >= - 90 && earliestPollDate < -60   % same format and process as above for other bins extending backward from election day
            RepAug(i) = RepSep(i);
            RepJul(i) = RepAug(i);
            RepJun(i) = RepJul(i);
            RepMay(i) = RepJun(i);
            RepApr(i) = RepMay(i);
            RepMar(i) = RepApr(i);
            RepFeb(i) = RepMar(i);
            RepJan(i) = RepFeb(i);
            
            DemAug(i) = DemSep(i);
            DemJul(i) = DemAug(i);
            DemJun(i) = DemJul(i);
            DemMay(i) = DemJun(i);
            DemApr(i) = DemMay(i);
            DemMar(i) = DemApr(i);
            DemFeb(i) = DemMar(i);
            DemJan(i) = DemFeb(i);
        elseif earliestPollDate >= - 120 && earliestPollDate < -90
            RepJul(i) = RepAug(i);
            RepJun(i) = RepJul(i);
            RepMay(i) = RepJun(i);
            RepApr(i) = RepMay(i);
            RepMar(i) = RepApr(i);
            RepFeb(i) = RepMar(i);
            RepJan(i) = RepFeb(i);
            
            DemJul(i) = DemAug(i);
            DemJun(i) = DemJul(i);
            DemMay(i) = DemJun(i);
            DemApr(i) = DemMay(i);
            DemMar(i) = DemApr(i);
            DemFeb(i) = DemMar(i);
            DemJan(i) = DemFeb(i);
        elseif earliestPollDate >= - 150 && earliestPollDate < -120
            RepJun(i) = RepJul(i);
            RepMay(i) = RepJun(i);
            RepApr(i) = RepMay(i);
            RepMar(i) = RepApr(i);
            RepFeb(i) = RepMar(i);
            RepJan(i) = RepFeb(i);
            
            DemJun(i) = DemJul(i);
            DemMay(i) = DemJun(i);
            DemApr(i) = DemMay(i);
            DemMar(i) = DemApr(i);
            DemFeb(i) = DemMar(i);
            DemJan(i) = DemFeb(i);
        elseif earliestPollDate >= - 180 && earliestPollDate < -150
            RepMay(i) = RepJun(i);
            RepApr(i) = RepMay(i);
            RepMar(i) = RepApr(i);
            RepFeb(i) = RepMar(i);
            RepJan(i) = RepFeb(i);
            
            DemMay(i) = DemJun(i);
            DemApr(i) = DemMay(i);
            DemMar(i) = DemApr(i);
            DemFeb(i) = DemMar(i);
            DemJan(i) = DemFeb(i);
        elseif earliestPollDate >= - 210 && earliestPollDate < -180
            RepApr(i) = RepMay(i);
            RepMar(i) = RepApr(i);
            RepFeb(i) = RepMar(i);
            RepJan(i) = RepFeb(i);
            
            DemApr(i) = DemMay(i);
            DemMar(i) = DemApr(i);
            DemFeb(i) = DemMar(i);
            DemJan(i) = DemFeb(i);
        elseif earliestPollDate >= - 240 && earliestPollDate < -210
            RepMar(i) = RepApr(i);
            RepFeb(i) = RepMar(i);
            RepJan(i) = RepFeb(i);
            
            DemMar(i) = DemApr(i);
            DemFeb(i) = DemMar(i);
            DemJan(i) = DemFeb(i);
        elseif earliestPollDate >= - 270 && earliestPollDate < -240
            RepFeb(i) = RepMar(i);
            RepJan(i) = RepFeb(i);
            
            DemFeb(i) = DemMar(i);
            DemJan(i) = DemFeb(i);
        elseif earliestPollDate >= - 300 && earliestPollDate < -270
            RepJan(i) = RepFeb(i);
            
            DemJan(i) = DemFeb(i);
        end     % end of if-else statement that handles case (1)
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% This if-else statement handles case (2) above -- recent polls
        %%% missing close to the election date.
        if mostRecentPollDate >= -30 && mostRecentPollDate <= 0         % most recent poll is within 30 days of the election
            
            %%% No adjustments needed.
            
        elseif mostRecentPollDate >= -60 && mostRecentPollDate < -30    % most recent poll is between 30 and 60 days of the election
            %%% If there are no polls within 30 days of the election but
            %%% there are polls within 60 days of the election, set the
            %%% data point for November equal to the data point for
            %%% October.
            RepNov(i) = RepOct(i);
            
            DemNov(i) = DemOct(i);
            
        elseif mostRecentPollDate >= -90 && mostRecentPollDate < -60    % most recent poll is between 60 and 90 days of the election
            %%% Similarly, if there are no polls within 60 days of the 
            %%% election but there are polls within 90 days, set the data
            %%% points for November and October equal to the data point for
            %%% September.
            RepOct(i) = RepSep(i);
            RepNov(i) = RepOct(i);
            
            DemOct(i) = DemSep(i);
            DemNov(i) = DemOct(i);
            
        elseif mostRecentPollDate >= -120 && mostRecentPollDate < -90   % same format and process as above for other bins extending backward from election day
            RepSep(i) = RepAug(i);
            RepOct(i) = RepSep(i);
            RepNov(i) = RepOct(i);
            
            DemSep(i) = DemAug(i);
            DemOct(i) = DemSep(i);
            DemNov(i) = DemOct(i);
        elseif mostRecentPollDate >= -150 && mostRecentPollDate < -120
            RepAug(i) = RepJul(i);
            RepSep(i) = RepAug(i);
            RepOct(i) = RepSep(i);
            RepNov(i) = RepOct(i);

            DemAug(i) = DemJul(i);
            DemSep(i) = DemAug(i);
            DemOct(i) = DemSep(i);
            DemNov(i) = DemOct(i);
        elseif mostRecentPollDate >= -180 && mostRecentPollDate < -150
            RepJul(i) = RepJun(i);
            RepAug(i) = RepJul(i);
            RepSep(i) = RepAug(i);
            RepOct(i) = RepSep(i);
            RepNov(i) = RepOct(i);

            DemJul(i) = DemJun(i);
            DemAug(i) = DemJul(i);
            DemSep(i) = DemAug(i);
            DemOct(i) = DemSep(i);
            DemNov(i) = DemOct(i);
        elseif mostRecentPollDate >= -210 && mostRecentPollDate < -180
            RepJun(i) = RepMay(i);
            RepJul(i) = RepJun(i);
            RepAug(i) = RepJul(i);
            RepSep(i) = RepAug(i);
            RepOct(i) = RepSep(i);
            RepNov(i) = RepOct(i);

            DemJun(i) = DemMay(i);
            DemJul(i) = DemJun(i);
            DemAug(i) = DemJul(i);
            DemSep(i) = DemAug(i);
            DemOct(i) = DemSep(i);
            DemNov(i) = DemOct(i);
        elseif mostRecentPollDate >= -240 && mostRecentPollDate < -210
            RepMay(i) = RepApr(i);
            RepJun(i) = RepMay(i);
            RepJul(i) = RepJun(i);
            RepAug(i) = RepJul(i);
            RepSep(i) = RepAug(i);
            RepOct(i) = RepSep(i);
            RepNov(i) = RepOct(i);

            DemMay(i) = DemApr(i);
            DemJun(i) = DemMay(i);
            DemJul(i) = DemJun(i);
            DemAug(i) = DemJul(i);
            DemSep(i) = DemAug(i);
            DemOct(i) = DemSep(i);
            DemNov(i) = DemOct(i);
        elseif mostRecentPollDate >= -270 && mostRecentPollDate < -240
            RepApr(i) = RepMar(i);
            RepMay(i) = RepApr(i);
            RepJun(i) = RepMay(i);
            RepJul(i) = RepJun(i);
            RepAug(i) = RepJul(i);
            RepSep(i) = RepAug(i);
            RepOct(i) = RepSep(i);
            RepNov(i) = RepOct(i);

            DemApr(i) = DemMar(i);
            DemMay(i) = DemApr(i);
            DemJun(i) = DemMay(i);
            DemJul(i) = DemJun(i);
            DemAug(i) = DemJul(i);
            DemSep(i) = DemAug(i);
            DemOct(i) = DemSep(i);
            DemNov(i) = DemOct(i);
        elseif mostRecentPollDate >= -300 && mostRecentPollDate < -270
            RepMar(i) = RepFeb(i);
            RepApr(i) = RepMar(i);
            RepMay(i) = RepApr(i);
            RepJun(i) = RepMay(i);
            RepJul(i) = RepJun(i);
            RepAug(i) = RepJul(i);
            RepSep(i) = RepAug(i);
            RepOct(i) = RepSep(i);
            RepNov(i) = RepOct(i);

            DemMar(i) = DemFeb(i);
            DemApr(i) = DemMar(i);
            DemMay(i) = DemApr(i);
            DemJun(i) = DemMay(i);
            DemJul(i) = DemJun(i);
            DemAug(i) = DemJul(i);
            DemSep(i) = DemAug(i);
            DemOct(i) = DemSep(i);
            DemNov(i) = DemOct(i);
        elseif mostRecentPollDate >= -330 && mostRecentPollDate < -300
            RepFeb(i) = RepJan(i);
            RepMar(i) = RepFeb(i);
            RepApr(i) = RepMar(i);
            RepMay(i) = RepApr(i);
            RepJun(i) = RepMay(i);
            RepJul(i) = RepJun(i);
            RepAug(i) = RepJul(i);
            RepSep(i) = RepAug(i);
            RepOct(i) = RepSep(i);
            RepNov(i) = RepOct(i);
            
            DemFeb(i) = DemJan(i);
            DemMar(i) = DemFeb(i);
            DemApr(i) = DemMar(i);
            DemMay(i) = DemApr(i);
            DemJun(i) = DemMay(i);
            DemJul(i) = DemJun(i);
            DemAug(i) = DemJul(i);
            DemSep(i) = DemAug(i);
            DemOct(i) = DemSep(i);
            DemNov(i) = DemOct(i);
        elseif mostRecentPollDate >= -400 && mostRecentPollDate < -300  && sum(currDates >= -300) == 0    % special case for 2012p noted in Methods
            RepFeb(i) = RepJan(i);
            RepMar(i) = RepFeb(i);
            RepApr(i) = RepMar(i);
            RepMay(i) = RepApr(i);
            RepJun(i) = RepMay(i);
            RepJul(i) = RepJun(i);
            RepAug(i) = RepJul(i);
            RepSep(i) = RepAug(i);
            RepOct(i) = RepSep(i);
            RepNov(i) = RepOct(i);
            
            DemFeb(i) = DemJan(i);
            DemMar(i) = DemFeb(i);
            DemApr(i) = DemMar(i);
            DemMay(i) = DemApr(i);
            DemJun(i) = DemMay(i);
            DemJul(i) = DemJun(i);
            DemAug(i) = DemJul(i);
            DemSep(i) = DemAug(i);
            DemOct(i) = DemSep(i);
            DemNov(i) = DemOct(i);
        end % end of if-else statement that handles case (2)
        
    end % end of if-else statement that handles cases (1), (2), and (4) in turn
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Now we handle case (3), polls missing in intermediate months -- we
    %%% interpolate the polling data to fill in these missing bins using
    %%% Matlab's interp1 function.
    if numMonths == 10
        formattedRepublicanData(1:numMonths,i) = [RepJan(i),RepFeb(i),RepMar(i),RepApr(i),RepMay(i),RepJun(i),...
            RepJul(i),RepAug(i),RepSep(i),RepOct(i)];
    else
        formattedRepublicanData(1:numMonths,i) = [RepJan(i),RepFeb(i),RepMar(i),RepApr(i),RepMay(i),RepJun(i),...
        RepJul(i),RepAug(i),RepSep(i),RepOct(i),RepNov(i)];
    end
    tempRep = formattedRepublicanData(1:numMonths,i);
    nanRepState = isnan(tempRep(1:numMonths,:));
    v = tempRep(logical(1-nanRepState));
    xs = 1:numMonths;
    x = xs(logical(1-nanRepState));
    xq = 1:numMonths;
    vq = interp1(x,v,xq);
    formattedRepublicanData(1:numMonths,i) = vq;
    
    if numMonths == 10
        formattedDemocratData(1:numMonths,i) = [DemJan(i),DemFeb(i),DemMar(i),DemApr(i),DemMay(i),DemJun(i),...
            DemJul(i),DemAug(i),DemSep(i),DemOct(i)];
    else
        formattedDemocratData(1:numMonths,i) = [DemJan(i),DemFeb(i),DemMar(i),DemApr(i),DemMay(i),DemJun(i),...
        DemJul(i),DemAug(i),DemSep(i),DemOct(i),DemNov(i)];
    end
    tempDem = formattedDemocratData(1:numMonths,i);
    nanDemState = isnan(tempDem(1:numMonths,:));
    v = tempDem(logical(1-nanDemState));
    xs = 1:numMonths;
    x = xs(logical(1-nanDemState));
    xq = 1:numMonths;
    vq = interp1(x,v,xq);
    formattedDemocratData(1:numMonths,i) = vq;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
end     % end of for loop that fills out any bins missing a polling data point
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% As the final step, we average across the states in the Safe Red
%%% superstate and the Safe Blue superstate to arrive at one data point per
%%% bin for each of our superstates. Note that we weight by the voting-age
%%% population size of the states in the superstates.
if numM - numIndividual == 0        % no superstates
    formattedRepublicanData = formattedRepublicanData(1:numMonths,numRed+numBlue+1:numElecs-numOut);
    formattedDemocratData = formattedDemocratData(1:numMonths,numRed+numBlue+1:numElecs-numOut);
else        % superstates
    formattedRepublicanData = [sum(formattedRepublicanData(1:numMonths,1:numRed).*redSize/totalPopRed,2),sum(formattedRepublicanData(1:numMonths,numRed+1:numRed+numBlue).*blueSize/totalPopBlue,2),formattedRepublicanData(1:numMonths,numRed+numBlue+1:numElecs-numOut)];
    formattedDemocratData = [sum(formattedDemocratData(1:numMonths,1:numRed).*redSize/totalPopRed,2),sum(formattedDemocratData(1:numMonths,numRed+1:numRed+numBlue).*blueSize/totalPopBlue,2),formattedDemocratData(1:numMonths,numRed+numBlue+1:numElecs-numOut)];
end

%%% Appending a left column counting from 1 to numMonths
formattedRepublicanData = [(1:numMonths)',formattedRepublicanData];
formattedDemocratData = [(1:numMonths)',formattedDemocratData];

%%% We now write this formatted polling data to two .csv files that are
%%% used by parameterFitting.R to fit parameters.
dlmwrite(['RepPercentages' election '.csv'], formattedRepublicanData, 'delimiter', ',', 'precision', 6)
dlmwrite(['DemPercentages' election '.csv'], formattedDemocratData, 'delimiter', ',', 'precision', 6)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%






