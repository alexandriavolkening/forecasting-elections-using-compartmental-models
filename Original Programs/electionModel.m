%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Model code associated with "Forecasting elections using compartmental
%%% models of infection" by Alexandria Volkening, Daniel F. Linder, 
%%% Mason A. Porter, and Grzegorz A. Rempala (2020) to appear in SIAM
%%% Review
%%%
%%% If you use this code, please cite the reference above and email
%%% Alexandria Volkening at alexandria_volkening@alumni.brown.edu and
%%% alexandria.volkening@northwestern.edu.
%%%
%%% Copyright 2020 Alexandria Volkening, Daniel F. Linder, Mason A. Porter,
%%% and Grzegorz A. Rempala
%%% Licensed under the Apache License, Version 2.0 (the "License"); you may
%%% not use this file except in compliance with the License. You may obtain
%%% a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
%%% Unless required by applicable law or agreed to in writing, software
%%% distributed under the License is distributed on an "AS IS" BASIS,
%%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
%%% implied. See the License for the specific language governing 
%%% permissions and limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Purpose: This code simulates elections using parameters fit to polling
%%% data (through the parameterFitting.R code). Note that this code relies
%%% on polling data and initial conditions formatted with 
%%% formattingPollData.m and model parameters generated with 
%%% parameterFitting.R.  To demo this code, use the option election = 
%%% '2016s_demo'. For all other elections, before running this 
%%% code to simulate elections, first run formattingPollData.m and then 
%%% parameterFitting.R to generate the files (initial conditions and model 
%%% parameters) called by this code. Note that the value for the election
%%% parameter passed to electionModel.m must match the value passed to 
%%% formattingPollData.m and  must be the same election considered in
%%% parameterFitting.R.
%%%
%%% Commands used to simulate each election:
%%%
%%% Demo of 2016 Senate elections (using files we have already generated
%%% using formattingPollData.m and parameterFitting.R):
%%% electionModel(308,1,0,'2016s_demo')
%%%
%%% In all other cases, before running this code, you must first run
%%% formattingPollData.m and then parameterFitting.R to generate the files
%%% called by this code (initial conditions and model parameters):
%%%
%%% 2018 Senate elections:
%%% electionModel(306,10000,0.0015,'2018s')       % To forecast the 2018 Senate Elections in July, follow the directions in reproducingDataFiguresTables.txt to adjust the code. 
%%% 
%%% 2018 governor elections:
%%% electionModel(306,10000,0.0015,'2018g')
%%%
%%% 2016 Senate elections:
%%% electionModel(308,1,0,'2016s')
%%%
%%% 2016 governor elections:
%%% electionModel(308,1,0,'2016g')
%%%
%%% 2016 president elections:
%%% electionModel(308,1,0,'2016p')                % without noise
%%% electionModel(308,10000,0.0015,'2016p')       % with noise
%%%
%%% 2012 Senate elections:
%%% electionModel(306,1,0,'2012s')
%%%
%%% 2012 governor elections:
%%% electionModel(306,1,0,'2012g')
%%%
%%% 2012 president elections:
%%% electionModel(306,1,0,'2012p')
%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [] = electionModel(time, nrand, sigma, election)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Preliminaries. Establishing folder and file names to save results.
folderName = 'Elections';   % name of folder (within the current folder) that results will be saved in
mkdir(folderName);          % creating the folder
dt = 0.1;                   % time step (in days) for ODEs and SDEs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% numM is the number of states and superstates used to simulate a given
%%% election. 
if strcmp(election, '2018s') == 1      % 2018 Senate election
    numM = 16;          % If forecasting the Senate races on July 8, replace this with numM = 15;
elseif strcmp(election, '2018g') == 1  % 2018 governor election
    numM = 15;
elseif strcmp(election, '2016s') == 1 || strcmp(election, '2016s_demo') == 1  % 2016 Senate election
    numM = 14;
elseif strcmp(election, '2016g') == 1   % 2016 governor election
    numM = 12;
elseif strcmp(election, '2016p') == 1   % 2016 president election
    numM = 14;
elseif strcmp(election, '2012s') == 1   % 2012 Senate election
    numM = 15;
elseif strcmp(election, '2012g') == 1   % 2012 governor election
    numM = 9;
elseif strcmp(election, '2012p') == 1   % 2012 president election
    numM = 14;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Initializations. Note that S + R + D = 1 for each state, so we track
%%% only the number of Republicans and undecided/other voters per state, as
%%% the remaining voters are Democrat.
numTimeSteps = time/dt;                    % number of time steps to simulate the model for

S = zeros(numTimeSteps+1,numM,nrand);      % Undecided/other voters
R = zeros(numTimeSteps+1,numM,nrand);      % Republican voters
mu = zeros(1,numM);                        % covariance for noise
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Setting up election-specific variables: population sizes, and and 
%%% some demographic information (used to correlate noise in some
%%% settings). Note that in all cases but '2016s_demo', running this code
%%% requires first running formattingPollData.m and then
%%% parameterFitting.R.
if strcmp(election,'2018s') == 1  % 2018 Senate election
    
    %%% Formatting demographic data for correlated noise. We obtain this
    %%% data from "Annual Estimates of the Resident Population by Sex, 
    %%% Race, and Hispanic Origin for the United States, States, and 
    %%% Counties: April 1, 2010 to July 1, 2016" Source: U.S. Census 
    %%% Bureau, Population Division (https://factfinder.census.gov/faces/
    %%% tableservices/jsf/pages/productview.xhtml?pid=PEP_2015_PEPSR6H&prod
    %%% Type=table) (2017) and from Comen E, Frohlich TC, and Sauter MB: 
    %%% 247WallSt.com: "America?s Most and Least Educated States: A Survey 
    %%% of All 50" (https://247wallst.com/special-report/2016/09/16/america
    %%% s-most-and- least-educated-states-a-survey-of-all-50/2/). Last 
    %%% accessed: 02-11-2018. (2016).
    [demoN, ~] = xlsread('dataset_correlationsSenate2018.xlsx');
    total = demoN(1:numM,1) + demoN(1:numM,3);
    HispanicPercent = demoN(1:numM,3)./total;
    NHBlackPercent = demoN(1:numM,2)./total;
    woCollegePercent = (100-demoN(1:numM,4))/100;

    %%% Number of voters per state or superstate. We calculate this using
    %%% data from "Estimates of the Voting Age Population for 2017. A 
    %%% Notice by the Commerce Department on 2/20/2018". Agency: Office of 
    %%% the Secretary, Commerce, Federal Register Notices, 83 (2018). 
    NN = [8603375,69220840,5382780,16782417,5093409,4277949,4730561,821604,2312576,7026626,579621,9053374,5208482,20938557,1446139,4512839];

%%% If forecasting the 2018 Senate elections on July 8, uncomment the
%%% following lines and comment lines 129-153
% if strcmp(election, '2018s') == 1  % 2018 Senate election without the Minnesota special election
%     
%     %%% Formatting demographic data for correlated noise. We obtain this
%     %%% data from "Annual Estimates of the Resident Population by Sex,
%     %%% Race, and Hispanic Origin for the United States, States, and
%     %%% Counties: April 1, 2010 to July 1, 2016" Source: U.S. Census
%     %%% Bureau, Population Division (https://factfinder.census.gov/faces/
%     %%% tableservices/jsf/pages/productview.xhtml?pid=PEP_2015_PEPSR6H&prod
%     %%% Type=table) (2017) and from Comen E, Frohlich TC, and Sauter MB:
%     %%% 247WallSt.com: "America?s Most and Least Educated States: A Survey
%     %%% of All 50" (https://247wallst.com/special-report/2016/09/16/america
%     %%% s-most-and- least-educated-states-a-survey-of-all-50/2/). Last
%     %%% accessed: 02-11-2018. (2016).
%     [demoN, ~] = xlsread('dataset_correlationsSenate2018_NoMNS.xlsx');
%     total = demoN(1:numM,1) + demoN(1:numM,3);
%     HispanicPercent = demoN(1:numM,3)./total;
%     NHBlackPercent = demoN(1:numM,2)./total;
%     woCollegePercent = (100-demoN(1:numM,4))/100;
%     
%     %%% Number of voters per state or superstate. We calculate this using
%     %%% data from "Estimates of the Voting Age Population for 2017. A
%     %%% Notice by the Commerce Department on 2/20/2018". Agency: Office of
%     %%% the Secretary, Commerce, Federal Register Notices, 83 (2018).
%     NN = [8603375,69220840,5382780,16782417,5093409,4730561,821604,2312576,7026626,579621,9053374,5208482,20938557,1446139,4512839];
%     
    
elseif strcmp(election, '2018g') == 1  % 2018 governor election
    
    %%% Formatting demographic data for correlated noise. We obtain this
    %%% data from "Annual Estimates of the Resident Population by Sex, 
    %%% Race, and Hispanic Origin for the United States, States, and 
    %%% Counties: April 1, 2010 to July 1, 2016" Source: U.S. Census 
    %%% Bureau, Population Division (https://factfinder.census.gov/faces/
    %%% tableservices/jsf/pages/productview.xhtml?pid=PEP_2015_PEPSR6H&prod
    %%% Type=table) (2017) and from Comen E, Frohlich TC, and Sauter MB: 
    %%% 247WallSt.com: "America?s Most and Least Educated States: A Survey 
    %%% of All 50" (https://247wallst.com/special-report/2016/09/16/america
    %%% s-most-and- least-educated-states-a-survey-of-all-50/2/). Last 
    %%% accessed: 02-11-2018. (2016).
    [demoN, ~] = xlsread('dataset_correlationsGovernor2018.xlsx');
    total = demoN(1:numM,1) + demoN(1:numM,3);
    HispanicPercent = demoN(1:numM,3)./total;
    NHBlackPercent = demoN(1:numM,2)./total;
    woCollegePercent = (100-demoN(1:numM,4))/100;

    %%% Number of voters per state or superstate. We calculate this using
    %%% data from "Estimates of the Voting Age Population for 2017. A 
    %%% Notice by the Commerce Department on 2/20/2018". Agency: Office of 
    %%% the Secretary, Commerce, Federal Register Notices, 83 (2018). 
    NN = [56473242,86200292,554867,2844358,16782417,7914681,2413764,2200585,1083273,2312576,9053374,2971579,3269157,654810,4512839];

elseif strcmp(election, '2016s') == 1   || strcmp(election, '2016s_demo') == 1% 2016 Senate election

    %%% Number of voters per state or superstate. We calculate this using
    %%% data from "Estimates of the Voting Age Population for 2016. A 
    %%% Notice by the Commerce Department on 1/30/2017". Agency: Office of 
    %%% the Secretary, Commerce, Federal Register Notices, 82 (2017). 
    NN = [33860361,37844627,5299579,16465727,9875430,5057601,3567717,4706137,2262631,1074207,7848068,9002201,10109422,4491015];

elseif strcmp(election, '2016g') == 1   % 2016 governor election

    %%% Number of voters per state or superstate. We calculate this using
    %%% data from "Estimates of the Voting Age Population for 2016. A 
    %%% Notice by the Commerce Department on 1/30/2017". Agency: Office of 
    %%% the Secretary, Commerce, Federal Register Notices, 82 (2017). 
    NN = [747791,5057601,4706137,814909,1074207,7848068,581641,3224738,2129444,506066,5658502,1456034]; 
    
elseif strcmp(election, '2016p') == 1   % 2016 president election

    %%% Formatting demographic data for when correlated noise is used to
    %%% simulate 2016 president forecasts. We obtain this
    %%% data from "Annual Estimates of the Resident Population by Sex, 
    %%% Race, and Hispanic Origin for the United States, States, and 
    %%% Counties: April 1, 2010 to July 1, 2016" Source: U.S. Census 
    %%% Bureau, Population Division (https://factfinder.census.gov/faces/
    %%% tableservices/jsf/pages/productview.xhtml?pid=PEP_2015_PEPSR6H&prod
    %%% Type=table) (2017) and from Comen E, Frohlich TC, and Sauter MB: 
    %%% 247WallSt.com: "America?s Most and Least Educated States: A Survey 
    %%% of All 50" (https://247wallst.com/special-report/2016/09/16/america
    %%% s-most-and- least-educated-states-a-survey-of-all-50/2/). Last 
    %%% accessed: 02-11-2018. (2016).
    [demoN, ~] = xlsread('dataset_correlationsPresident2016.xlsx');
    total = demoN(1:numM,1) + demoN(1:numM,3);
    HispanicPercent = demoN(1:numM,3)./total;
    NHBlackPercent = demoN(1:numM,2)./total;
    woCollegePercent = (100-demoN(1:numM,4))/100;

    %%% Number of voters per state or superstate. We calculate this using
    %%% data from "Estimates of the Voting Age Population for 2016. A 
    %%% Notice by the Commerce Department on 1/30/2017". Agency: Office of 
    %%% the Secretary, Commerce, Federal Register Notices, 82 (2017). 
    NN = [82223613, 90814662, 4279173, 16465727, 2403962, 7737243, 4231619, 2262631, 1074207, 7848068, 9002201, 10109422, 6541685, 4491015];

elseif strcmp(election, '2012s') == 1   % 2012 Senate election

    %%% Number of voters per state or superstate. We calculate this using
    %%% data from "Estimates of the Voting Age Population for 2012. A 
    %%% Notice by the Commerce Department on 1/30/2013". Agency: Office of 
    %%% the Secretary, Commerce, Federal Register Notices, 78 (2013). 
    NN = [30075741,78196443,4932361,2796789,15315088,4945857,5244729,4618513,783161,2095348,545020,8880551,10024150,6329130,4408841];
    
elseif strcmp(election, '2012g') == 1   % 2012 governor election

    %%% Number of voters per state or superstate. We calculate this using
    %%% data from "Estimates of the Voting Age Population for 2012. A 
    %%% Notice by the Commerce Department on 1/30/2013". Agency: Office of 
    %%% the Secretary, Commerce, Federal Register Notices, 78 (2013). 
    NN = [4945857,4618513,783161,1045878,7465545,545020,1967315,502060,5312045]; 

elseif strcmp(election, '2012p') == 1   % 2012 president election

    %%% Number of voters per state or superstate. We calculate this using
    %%% data from "Estimates of the Voting Age Population for 2012. A 
    %%% Notice by the Commerce Department on 1/30/2013". Agency: Office of 
    %%% the Secretary, Commerce, Federal Register Notices, 78 (2013). 
    NN = [78609279,87985204,3956224,15315088,2351233,7616490,4102991,2095348,1045878,7465545,8880551,10024150,6329130,4408841];

end

%%% Total number of voters
N = sum(NN);

%%% Initial conditions - these are copied from the first line of the
%%% .csv files (['RepPercentages' election '.csv'] and
%%% ['DemPercentages' election '.csv'] generated by formattingPollData.m.
%%% Unless running election = 2016s_demo, this requires first running
%%% formattingPollData.m
if strcmp(election, '2016s_demo') == 1
    formattedRepPolls = csvread(['./DemoFiles/RepPercentages' election '.csv'],0,1);
    formattedDemPolls = csvread(['./DemoFiles/DemPercentages' election '.csv'],0,1);
else
    formattedRepPolls = csvread(['RepPercentages' election '.csv'],0,1);
    formattedDemPolls = csvread(['DemPercentages' election '.csv'],0,1);
end
RepIC = formattedRepPolls(1,:)/100;
DemIC = formattedDemPolls(1,:)/100;
for i = 1:nrand
    R(1,:,i) = RepIC;
    S(1,:,i) = 1 - RepIC - DemIC;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Specifying model parameters (unless running election = 2016s_demo, this 
%%% code requires first running formattingPollData.m and
%%% parameterFitting.R)
if strcmp(election, '2016s_demo') == 1
    numbers = csvread(['./DemoFiles/modelParameters' election '.csv'],1,1);
else
    numbers = csvread(['modelParameters' election '.csv'],1,1);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Specifying parameters that appear in Eqns. 3-8. Note that we convert
%%% our parameters to units of 1/day assuming 30-day months.
gamma_R = numbers(numM*2+1,1:numM)/30;          % Republican turnover
gamma_D = numbers(numM*2+2,1:numM)/30;          % Democrat turnover
beta_R = numbers(1:numM,1:numM)/30;             % Republican transmission
beta_D = numbers(numM+1:numM*2,1:numM)/30;      % Democrat transmission
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% There are two cases: simulating one election without noise or
%%% simulating nrand elections with noise
if strcmp(election, '2012s') == 1  || strcmp(election, '2012g') == 1 || ...
        strcmp(election, '2012p') == 1 || strcmp(election, '2016s') == 1 ...
        || strcmp(election, '2016g') == 1  || strcmp(election, '2016s_demo') == 1    % no noise
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% This for loop simulates a single election. Note that we use that S
    %%% + D + R = 1 for each state to reduce the number of equations that
    %%% need ot be simulated to two per state.
    for time_step = 2:numTimeSteps+1
        
        [rhsS,rhsR] = electionModelRHS(NN,N,S(time_step-1,:,1),R(time_step-1,:,1),gamma_R,gamma_D,beta_R,beta_D,numM);
        S(time_step,:,1) = S(time_step-1,:,1) + rhsS*dt;
        R(time_step,:,1) = R(time_step-1,:,1) + rhsR*dt;
        
        
    end %%% end of for loop simulating one election without noise
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
else    % noise is added for the 2018 elections and can be added for the 2016 presidential elections (set sigma = 0 and nrand = 1 to avoid this)
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Calculating the covariance to use for our noise in Eqns. 6-8 in the
    %%% manuscript. We correlate on the fraction of Hispanic 
    %%% individuals, the fraction of Non-Hispanic Black individuals, and
    %%% the fraction of individuals without a college education in each 
    %%% state.
    J_H = zeros(numM,numM);
    J_B = zeros(numM,numM);
    J_E = zeros(numM,numM);
    
    for i = 1:numM
        for j =1:numM
            J_H(i,j) = min(HispanicPercent(i),HispanicPercent(j))./(max(HispanicPercent(i),HispanicPercent(j)));
            J_B(i,j) = min(NHBlackPercent(i),NHBlackPercent(j))./(max(NHBlackPercent(i),NHBlackPercent(j)));
            J_E(i,j) = min(woCollegePercent(i),woCollegePercent(j))./(max(woCollegePercent(i),woCollegePercent(j)));
        end
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Main for loop used to simulate nrand elections. We use nrand =
    %%% 10,000 for the 2018 elections
    for randi = 1:nrand
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% For each simulated election, we choose one Jaccard index 
        %%% uniformly at random to use the covariance of our noise. 
        randomNum = rand(1);
        if randomNum <= 1/3
            covariance = J_E;
        elseif randomNum > 2/3
            covariance = J_B;
        else
            covariance = J_H;
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% This for loop simulates a single election. Note that we use 
        %%% that S + D + R = 1 for each state to reduce the number of 
        %%% equations that need ot be simulated to two per state.
        for time_step = 2:numTimeSteps+1
            
            %%% Correlated random noise
            dWt1 = sqrt(dt)*mvnrnd(mu,covariance);
            dWt2 = sqrt(dt)*mvnrnd(mu,covariance);
            
            %%% Uncorrelated random noise -- uncomment the two lines below to
            %%% simulate elections with uncorrelated random noise
            %dWt1 = sqrt(dt)*randn(1,numM);
            %dWt2 = sqrt(dt)*randn(1,numM);
            
            [rhsS,rhsR] = electionModelRHS(NN,N,S(time_step-1,:,randi),R(time_step-1,:,randi),gamma_R,gamma_D,beta_R,beta_D,numM);
            S(time_step,:,randi) = S(time_step-1,:,randi) + rhsS*dt + sigma*dWt1;
            R(time_step,:,randi) = R(time_step-1,:,randi) + rhsR*dt + sigma*dWt2;
            
            
        end %%% end of for loop simulating one election
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
    end %%% end of main for loop simulating nrand elections
    
end %%% main if loop for determining whether simulations are random or not


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Final processing to produce some images and report results in
%%% reproduceTablesFigures.m


%%% First we calculate our marginForecast on election day (the percentage 
%%% point margin lead of Republicans over Democrats) for each state or 
%%% superstate per simulation. Note that this is negative for a given state
%%% if a Democrat wins, and positive if a Republican wins (0 indicates a
%%% tie).
marginForecast = squeeze((R(numTimeSteps+1,:,:)- 1 + S(numTimeSteps+1,:,:) + R(numTimeSteps+1,:,:))*100);


%%% Here we calculate, for each state or superstate, the fraction of
%%% simulated elections in which the Republican candidate won. This is
%%% used to categorize states as "Likely Rep.", "Lean Dem.", etc.
chanceRWinState = zeros(1,numM);
chanceRTie = zeros(1,numM);
if nrand > 1
    for i = 1:numM
        
        %%% Placeholder
        temporary = marginForecast(i,:);
        
        %%% Number of simulations in which the Republican candidate won
        %%% (note you need to divide by nrand to express this as a
        %%% percent).
        chanceRWinState(i) = sum(temporary > 0);
        
        %%% Number of simulations in which the Republican and Democrat
        %%% candidates tied.
        chanceRTie(i) = sum(temporary == 0);
        
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Saving data and simulation conditions. Note that we only save the final
%%% election results rather than the whole trajectory of opinion dynamics.
finalR = squeeze(R(time/dt+1,:,:));
finalS = squeeze(S(time/dt+1,:,:));
save([folderName '/Results' election '.mat'], 'finalR','finalS','sigma','nrand','dt','time', 'chanceRWinState','chanceRTie');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


end  %%% end of main function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Helper funciton holding the righthand side of our model differential
%%% equations
function [rhsS,rhsR] = electionModelRHS(NN,N,S,R,gamma_R,gamma_D,beta_R,beta_D,numM)
    rhsS = zeros(1,numM);
    rhsR = zeros(1,numM);
    
    for i = 1:numM
        rhsS(i) = gamma_R(i)*R(i) + gamma_D(i)*(1-S(i)-R(i)) -...
            (S(i)/N)*(NN.*R)*beta_R(1:numM,i) -...
            (S(i)/N)*(NN.*(1-S-R))*beta_D(1:numM,i);
        rhsR(i) = -gamma_R(i)*R(i) + (S(i)/N)*(NN.*R)*beta_R(1:numM,i);
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




