%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Results processing code for "Forecasting elections using compartmental
%%% models of infection" by Alexandria Volkening, Daniel F. Linder, 
%%% Mason A. Porter, and Grzegorz A. Rempala (2020), to appear in SIAM
%%% Review
%%%
%%% If you use this code, please cite the reference above and email
%%% Alexandria Volkening at alexandria_volkening@alumni.brown.edu and
%%% alexandria.volkening@northwestern.edu.
%%%
%%% Copyright 2020 Alexandria Volkening, Daniel F. Linder, Mason A. Porter,
%%% and Grzegorz A. Rempala
%%% Licensed under the Apache License, Version 2.0 (the "License"); you may
%%% not use this file except in compliance with the License. You may obtain
%%% a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
%%% Unless required by applicable law or agreed to in writing, software
%%% distributed under the License is distributed on an "AS IS" BASIS,
%%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
%%% implied. See the License for the specific language governing 
%%% permissions and limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Purpose: This code computes all quantitative measurements reported in
%%% Tables 1 and 2 and reproduces Figures 2-4.
%%%
%%% This code calls several files containing data on election results,
%%% population sizes, and the forecasts of popular analysts. Data sources
%%% are given in each such .xlsx file.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [] = reproduceTablesFigures(folderName,election)

%%% Loading results 
load(['./' folderName '/Results' election],'nrand','finalR','finalS','dt','time','sigma','chanceRWinState','chanceRTie')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Accessing election-specific data (number of states forecast, voting-age
%%% population size by state, state color, and election results.
if strcmp(election, '2018s') == 1  % 2018 Senate election

    numM = 16;                                              % number of states and superstates that we forecast

    %%% Creating some strings to use in our figures
    stateNames = {'Red','Blue','AZ','FL','IN','MN*','MO','MT','NV','NJ','ND','OH','TN','TX','WV','WI'};
    stateNamesRev = reordercats(categorical(stateNames),{'WI','WV','TX','TN','OH','ND','NJ','NV','MT','MO','MN*','IN','FL','AZ','Blue','Red'});
    figureTitle = '2018 Senatorial Elections';
    
    %%% Importing information about state races and results (state color,
    %%% election results, and forecasts of popular analysts). Note that
    %%% state color is 'R' if a state is assigned to our Red superstate,
    %%% 'B' if assigned to our Blue superstate, and 'U' otherwise.
    [stateNumData, stateTextData] = xlsread('/dataset_Index.xlsx',1,'A2:N35');  % page 1 of the index file contains 2018s data

    %%% If forecasting the 2018 Senate races in July, comment lines 41-54
    %%% above an uncomment lines 58-72 below
% if strcmp(election, '2018s') == 1 % 2018 Senate election without Minnesota specia
%     
%     numM = 15;                                              % number of states and superstates that we forecast
% 
%     %%% Creating some strings to use in our figures
%     stateNames = {'Red','Blue','AZ','FL','IN','MO','MT','NV','NJ','ND','OH','TN','TX','WV','WI'};
%     stateNamesRev = reordercats(categorical(stateNames),{'WI','WV','TX','TN','OH','ND','NJ','NV','MT','MO','IN','FL','AZ','Blue','Red'});
%     figureTitle = '2018 Senatorial Elections';
%     
%     %%% Importing information about state races and results (state color,
%     %%% election results, and forecasts of popular analysts). Note that
%     %%% state color is 'R' if a state is assigned to our Red superstate,
%     %%% 'B' if assigned to our Blue superstate, and 'U' otherwise.
%     [stateNumData, stateTextData] = xlsread('/dataset_Index.xlsx',9,'A2:N35');  % page 9 of the index file contains 2018s data with MN special flagged as NA to remove it from our forecast due to no polling data at this time frame

elseif strcmp(election, '2018g') == 1 % 2018 governor election
   
    numM = 15;                                              % number of states and superstates that we forecast
    
    %%% Creating some strings to use in figures
    stateNames = {'Red','Blue','AK','CT','FL','GA','IA','KS','ME','NV','OH','OK','OR','SD','WI'};
    stateNamesRev = reordercats(categorical(stateNames), {'WI','SD','OR','OK','OH','NV','ME','KS','IA','GA','FL','CT','AK','Blue','Red'});
    figureTitle = '2018 Gubernatorial Elections';
    
    %%% Importing information about state races and results (state color,
    %%% election results, and forecasts of popular analysts). Note that
    %%% state color is 'R' if a state is assigned to our Red superstate,
    %%% 'B' if assigned to our Blue superstate, and 'U' otherwise.
    [stateNumData, stateTextData] = xlsread('/dataset_Index.xlsx',2,'A2:N37');  % page 2 of the index file contains 2018g data
    
elseif strcmp(election, '2016s') == 1   % 2016 Senate election
    
    numM = 14;                                              % number of states and superstates that we forecast
    
    %%% Creating some strings to use in figures
    stateNames = {'Red','Blue','AZ','FL','IL','IN','LA','MO','NV','NH','NC','OH','PA','WI'};
    stateNamesRev = reordercats(categorical(stateNames),{'WI','PA','OH','NC','NH','NV','MO','LA','IN','IL','FL','AZ','Blue','Red'});
    figureTitle = '2016 Senatorial Elections';
    
    %%% Importing information about state races and results (state color
    %%% and election results).
    [stateNumData, stateTextData] = xlsread('/dataset_Index.xlsx',3,'A2:I34');  % page 3 of the index file contains 2016s data
    
elseif strcmp(election, '2016g') == 1   % 2016 governor election

    numM = 12;                                              % number of states that we forecast
    
    %%% Creating some strings to use in figures
    stateNames = {'DE','IN','MO','MT','NH','NC','ND','OR','UT','VT','WA','WV'};
    stateNamesRev = reordercats(categorical(stateNames),{'WV','WA','VT','UT','OR','ND','NC','NH','MT','MO','IN','DE'});
    figureTitle = '2016 Gubernatorial Elections';
    
    %%% Importing information about state races and results (state color
    %%% and election results).
    [stateNumData, stateTextData] = xlsread('/dataset_Index.xlsx',4,'A2:I13');  % page 4 of the index file contains 2016g data
    
elseif strcmp(election, '2016p') == 1   % 2016 president election

    numM = 14;                                              % number of states and superstates that we forecast
    
    %%% Creating some strings to use in figures
    stateNames = {'Red','Blue','CO','FL','IA','MI','MN','NV','NH','NC','OH','PA','VA','WI'};
    stateNamesRev = reordercats(categorical(stateNames),{'WI','VA','PA','OH','NC','NH','NV','MN','MI','IA','FL','CO','Blue','Red'});
    figureTitle = '2016 Presidential Elections';
    
    %%% Importing information about state races and results (state color
    %%% and election results).
    [stateNumData, stateTextData] = xlsread('/dataset_Index.xlsx',5,'A2:I52');  % page 5 of the index file contains 2016p data
    
elseif strcmp(election, '2012s') == 1   % 2012 Senate election
    
    numM = 15;                                              % number of states and superstates that we forecast
    
    %%% Creating some strings to use in figures
    stateNames = {'Red','Blue','AZ','CT','FL','IN','MA','MO','MT','NV','ND','OH','PA','VA','WI'};
    stateNamesRev = reordercats(categorical(stateNames),{'WI','VA','PA','OH','ND','NV','MT','MO','MA','IN','FL','CT','AZ','Blue','Red'});
    figureTitle = '2012 Senatorial Elections';
    
    %%% Importing information about state races and results (state color
    %%% and election results).
    [stateNumData, stateTextData] = xlsread('/dataset_Index.xlsx',6,'A2:I29');  % page 6 of the index file contains 2012s data
    
    
elseif strcmp(election, '2012g') == 1   % 2012 governor election
    
    numM = 9;                                               % number of states that we forecast

    %%% Creating some strings to use in figures
    stateNames = {'IN','MO','MT','NH','NC','ND','UT','VT','WA'};
    stateNamesRev = reordercats(categorical(stateNames),{'WA','VT','UT','ND','NC','NH','MT','MO','IN'});
    figureTitle = '2012 Gubernatorial Elections';
    
    %%% Importing information about state races and results (state color
    %%% and election results).
    [stateNumData, stateTextData] = xlsread('/dataset_Index.xlsx',7,'A2:I10');  % page 7 of the index file contains 2012g data
    
    
elseif strcmp(election, '2012p') == 1   % 2012 president election

    numM = 14;                                              % number of states and superstates that we forecast
    
    %%% Creating some strings to use in figures
    stateNames = {'Red','Blue','CO','FL','IA','MI','MN','NV','NH','NC','OH','PA','VA','WI'};
    stateNamesRev = reordercats(categorical(stateNames),{'WI','VA','PA','OH','NC','NH','NV','MN','MI','IA','FL','CO','Blue','Red'});
    figureTitle = '2012 Presidential Elections';
    
    %%% Importing information about state races and results (state color
    %%% and election results).
    [stateNumData, stateTextData] = xlsread('/dataset_Index.xlsx',8,'A2:I52');  % page 8 of the index file contains 2012p data
    
end
stateColor = stateTextData(:,5);      % state color ('R' or 'B') if it belongs in a superstate ('U' otherwise)
population = stateNumData(:,4);       % voting-age population size for given state
result = stateNumData(:,5);           % outcome (margin) for each state. Note this is negative if a Democrat wins, positive if a Republican wins, and 0 if there is a tie.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Formatting the election results to account for superstates (when we use
%%% superstates). This allows us to compare our forecasts directly with the
%%% election results. We calculate the election result for the Red (Blue)
%%% superstate by calculating the mean election result of all the states in
%%% the Red (Blue) superstate, weighted by state size (number of voting-age
%%% individuals).
identifyRed = (string(stateColor) == 'R');            % logical is 1 if a state belongs in our Red superstate, 0 otherwise
identifyBlue = (string(stateColor) == 'B');           % logical is 1 if a state belongs in our Blue superstate, 0 otherwise
populationTotalRed = sum(population(identifyRed));      % total number of voting-age individuals in the Red superstate
populationTotalBlue = sum(population(identifyBlue));    % total number of voting-age individuals in the Blue superstate
redSuperstateResult = sum(result(identifyRed).*population(identifyRed)/populationTotalRed);  % Red superstate result (vote margin)
blueSuperstateResult = sum(result(identifyBlue).*population(identifyBlue)/populationTotalBlue);  % Blue superstate result (vote margin)

identifyIndividual = (string(stateColor) == 'U');     % logical is 1 if a state does not belong to a superstate, 0 otherwise
numIndividual = sum(identifyIndividual);
numElecs = numIndividual + sum(identifyRed) + sum(identifyBlue);
%%% In some cases (do to lack of polling data), states are included in our
%%% forecast as part of the superstates but not counted in numElecs yet; we
%%% add these special cases here.
if strcmp(election,'2012s') == 1
    numElecs = numElecs + 3;    % 3 states without polling data included in superstates
end
    

%%% Election results for comparison with our forecasts formatted with
%%% superstates and individual states
if sum(identifyRed) + sum(identifyBlue) > 0     % superstates are used
    formattedResult = [redSuperstateResult; blueSuperstateResult; result(identifyIndividual)];
else    % no superstates are used
    formattedResult = result(identifyIndividual);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Calculating our mean forecast as the percentage point margin leads.
%%% This is in the form Rep percent - Dem percent (noting that Dem percent
%%% = 1 - undecided percent - Rep percent).
if nrand > 1
    modelForecast = mean((finalR - 1 + finalS + finalR)*100,2)';
else
    modelForecast = (finalR - 1 + finalS + finalR)*100';
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Calculating model accuracy
%%%
%%% Calculating our accuracy as the number of states that we correctly
%%% called as Republican or Democrat. Note that  signProductMargins is 
%%% positive if our model forecast and the election result have the same 
%%% sign (both are negative to indicate a Democrat win and both are 
%%% positive to indicate a Republican win).
signProductMargins = (modelForecast'.*formattedResult > 0);
modelPerformanceCalling = sum(signProductMargins);

%%% Calculating our error in vote margin for the states we forecast
%%% individually
modelMarginError = abs(modelForecast' - formattedResult);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Different cases based on the election to account for the fact that we
%%% produce different types of figures for different elections. This
%%% if-else statement handles the reproduction of all figures and the
%%% quantitative measurements in Tables 1 or 2 associated with a given 
%%% election. 
if strcmp(election, '2018s') == 1 || strcmp(election, '2018g') == 1 
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% This portion of the code generates Figure 4 and S4. Note that 80th
    %%% percent confidence bars are generated in a separate figure (the
    %%% yellow bars in Figure 4A and S4).
    
    %%% Accessing FiveThirtyEight's forecast since we compare our results
    %%% to this in Figures 4A S4 for the 2018 races.
    forecast538 = stateNumData(:,6);        % final margin forecasted by FiveThirtyEight
    temp = stateNumData(:,7)/100;   % likeliness of each forecast outcome given by FiveThirtyEight
    probability538 = temp(identifyIndividual == 1);
    
    %%% Similarly, accessing the forecasts of the Cook Political Report,
    %%% Inside Elections, FiveThirtyEight, and Sabato's Crystal Ball (only
    %%% for the elections that we forecast individually in our model).
    temp = stateNumData(:,8)/100;
    probabilitySabato = temp(identifyIndividual == 1);  % forecasted chance for the winning candidate to win (see manuscript for how we transform qualitative forecasts to probabilities)
    temp = stateNumData(:,9)/100;
    probabilityCook = temp(identifyIndividual == 1);
    temp = stateNumData(:,10)/100;
    probabilityIE = temp(identifyIndividual == 1);
    temp = stateNumData(:,11)/100;
    probabilityRCP = temp(identifyIndividual == 1);
    
    %%%  Calculating FiveThirtyEight's forecasts in our superstates
    redSuperstate538 = sum(forecast538(identifyRed).*population(identifyRed)/populationTotalRed);      % Red superstate forecast by 538 (vote margin)
    blueSuperstate538 = sum(forecast538(identifyBlue).*population(identifyBlue)/populationTotalBlue);  % Blue superstate forecast by 538 (vote margin)
    
    %%% FiveThirtyEight's forecasts for comparison with our forecasts 
    %%% formatted with superstates and individual states
    formatted538 = [redSuperstate538; blueSuperstate538; forecast538(identifyIndividual)];
    
    %%% Creating Figure 4A (if election = '2018g') or Figure S4 (if
    %%% election = '2018s')
    figure
    barh(stateNamesRev,[formattedResult,formatted538,modelForecast']);
    hold on
    xlim([-30 30])
    xticks(-30:10:30)
    title(figureTitle)
    xlabel('Dem margin lead to the left; Rep margin lead to the right (percentage points)')
    legend('Result','FiveThirtyEight','Model')
    xticklabels({'+30','+20','+10','0','+10','+20','+30'})
    set(gca,'FontSize',14,'FontName','Helvetica')
    hold off
    
    %%% Plotting bars describing 80% confidence intervals associated with
    %%% Figure 4A or Figure S4.
    %%% We  calculate the forecast margin at the 10th and 90th percentiles
    %%% of our forecast for each state or super state.
    percent10 = prctile((finalR - 1 + finalS + finalR)*100, 10, 2);
    percent90 = prctile((finalR - 1 + finalS + finalR)*100, 90, 2);

    figure
    for i = 1:numM
        plot([percent10(i), percent90(i)],[i, i],'k','LineWidth',3)
        hold on
    end
    title('Middle range in which 80% of simulated elections fall')
    xlim([-50 50])
    xticks(-50:10:50)
    xticklabels({'+50','+40','+30','+20','+10','0','+10','+20','+30','+40','+50'})
    yticklabels(stateNames)
    yticks(1:1:numM);
    set(gca,'FontSize',14,'FontName','Helvetica')
    hold off
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   
    

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Calculating 538's accuracy as the number of states that they called
    %%% correctly as Republican or Democrat. Note that signProductMargins 
    %%% is positive if their forecast and the election result have the same
    %%% sign (both are negative to indicate a Democrat win and both are
    %%% positive to indicate a Republican win).
    signProductMargins = (formatted538.*formattedResult > 0);
    performanceCalling538 = sum(signProductMargins);
    
    %%% Similarly, calculating the accuracy of the Cook Political Report,
    %%% RealClearPolitics, Sabato's Crystal Ball, and Inside Elections as
    %%% the number of states that they called correctly as Republican or
    %%% Democrat. In this case, a forecaster called the election correctly
    %%% if they assigned the winning candidate more than a 50% chance of
    %%% winning the election. Note that all these forecasters called the
    %%% states in the safe red and safe blue states correctly (with one 
    %%% exception, which we describe next), so we add these. The exception
    %%% is that RealClearPolitics left NH as a toss-up for Governor 2018,
    %%% so we subtract 1 from RealClearPolitics' performance for this
    %%% special case.
    performanceCallingCook = sum(probabilityCook > 0.5) + (numM - numIndividual);
    if strcmp(election,'2018g') == 1 || strcmp(election,'2018g_demo') == 1
        performanceCallingRCP = sum(probabilityRCP > 0.5) + (numM - numIndividual) - 1;
    else
        performanceCallingRCP = sum(probabilityRCP > 0.5) + (numM - numIndividual);
    end
    performanceCallingSabato = sum(probabilitySabato > 0.5) + (numM - numIndividual);
    performanceCallingIE = sum(probabilityIE > 0.5) + (numM - numIndividual);
    
    %%% Displaying the number of states that we missed and the number of
    %%% states that other forecasters missed
    disp(['The number of states that we missed (Table 2) for the ' figureTitle ' is ' num2str(numM-modelPerformanceCalling) '.'])
    disp(['In comparison, the number of states that FiveThirtyEight missed is ' num2str(numM-performanceCalling538) '.'])
    disp(['The number of states that Sabatos Crystal Ball missed or did not call is ' num2str(numM-performanceCallingSabato) '.'])
    disp(['The number of states that the Cook Political Report missed or did not call is ' num2str(numM-performanceCallingCook) '.'])
    disp(['The number of states that Inside Elections missed or did not call is ' num2str(numM-performanceCallingIE) '.'])
    disp(['The number of states that RealClearPolitics missed or did not call is ' num2str(numM-performanceCallingRCP) '.'])
    
    %%% Computing FiveThirtyEight's vote margin error for the states that
    %%% we forecast individually
    marginError538 = abs(formatted538 - formattedResult);
    
    %%% Displaying our average vote margin error and FiveThirtyEight's 
    %%% average vote margin error for the states that we forecast 
    %%% individually
    disp(['Our average margin error (Table 2) for the ' figureTitle ' is ' num2str(sum(modelMarginError(3:numM))/(numM-2))  '.'])
    disp(['In comparison, FiveThirtyEight has a margin error of ' num2str(sum(marginError538(3:numM))/(numM-2)) '.'])
    
    %%% Computing our log loss error for the states that we forecast 
    %%% individually according to the equation for log loss (involving y 
    %%% and p) given in the manuscript.
    %%% Note that chanceRWinState is the chance we assign to the
    %%% Republican candidate of winning (and so 1 - chanceRWinState,
    %%% assuming no ties, is our chance of a Democrat winnning). Thus, we
    %%% break up our computation of y and p in the log loss equation into
    %%% two separate cases to handle our formatting of chanceRWinState.
    yR = (modelForecast(3:numM)' > 0);              % logical indicating states correctly forecast as Republican
    yD = (modelForecast(3:numM)' < 0);              % logical indicating states correctly forecast as Democrat
    pR = chanceRWinState(3:numM)'./nrand;           % probability of our projected candidate winning (if we are projecting a Republican)
    pD = 1 - (chanceRWinState(3:numM)'./nrand);     % probability of our projected candidate winning (if we are projecting a Democrat)
    p = (formattedResult(3:numM).*modelForecast(3:numM)' > 0);
    modelLogLoss = -sum( p.*(yR.*log(pR) + yD.*log(pD)) + (1-p).*(yR.*log(pD) + yD.*log(pR)))/(numM-2);


    %%% Computing FiveThirtyEight's log loss error for the states that we 
    %%% forecast individually according to the equation for log loss 
    %%% (involving y and p) given in the manuscript (Table 2).
    p = probability538;                                             % probability assigned to the true outcome by 538
    logLoss538 = -sum(log(p))/(numM - 2);
    
    %%% Computing the log loss error for the Cook Political Report,
    %%% RealClearPolitics, Sabato's Crystal Ball, and Inside Elections for
    %%% the states that we forecast individually (Table 2).
    p = probabilityCook;                                % probability assigned to the true outcome by the Cook Political Report
    logLossCook = -sum(log(p))/(numM - 2);
    p = probabilityRCP;                                 % probability assigned to the true outcome by RealClearPolitics
    logLossRCP = -sum(log(p))/(numM - 2);
    p = probabilitySabato;                              % probability assigned to the true outcome by Sabato's Crystall Ball
    logLossSabato = -sum(log(p))/(numM - 2);
    p = probabilityIE;                                  % probability assigned to the true outcome by Inside Elections
    logLossIE = -sum(log(p))/(numM - 2);
    
    %%% Displaying our model log loss and FiveThirtyEight's log loss error
    %%% for the states that we forecast individually
    disp(['Our log loss error (Table 2) for the ' figureTitle ' is ' num2str(modelLogLoss)  '.'])
    disp(['In comparison, FiveThirtyEight has a log loss error of ' num2str(logLoss538) '.'])
    disp(['Sabatos Crystal Ball has a log loss error of ' num2str(logLossSabato) '.'])
    disp(['The Cook Political Report has a log loss error of ' num2str(logLossCook) '.'])
    disp(['Inside Elections has a log loss error of ' num2str(logLossIE) '.'])
    disp(['RealClearPolitics has a log loss error of ' num2str(logLossRCP) '.'])
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% This portion of the code displays the data that appears
    %%% in the Nov. 3 column of Figure 4 for our model (depending
    %%% on if election = '2018s' or '2018g').
    for i = 3:numM
        disp(['The model probability of a Republican winning the ' figureTitle ' in ' char(stateNames(i)) ' is: ' num2str(chanceRWinState(i)*100/nrand) '%.'])
        disp(['The model probability of a Democrat winning the ' figureTitle ' in ' char(stateNames(i)) ' is: ' num2str((1-chanceRWinState(i)/nrand)*100) '%.'])
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
elseif strcmp(election, '2016p') == 1 && sigma > 0 && nrand > 1
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% This portion of the code generates the results in Figure 3.
    
    %%% For each simulated election, we find the number of electoral votes
    %%% for Trump and Clinton
    RepElectoral = zeros(1,nrand);
    DemElectoral = zeros(1,nrand);
    for i = 1:nrand
        [RepElectoral(i),DemElectoral(i)] = electoralVotes(finalR(:,i),finalS(:,i));
    end
    
    %%% Plotting distribution of electoral votes for Trump (Figure 3)
    figure
    histogram(RepElectoral,'BinWidth',1, 'Normalization', 'probability')
    hold on
    xlim([0 538])
    ylabel('Fraction of simulations with given outcome')
    xlabel('Electoral votes for Trump with noise')
    title('Figure 3: Electoral votes for Trump')
    legend(['Trump wins chance: ' num2str(100*sum(RepElectoral >= 270)/nrand) '%'])
    set(gca,'FontSize',14,'FontName','Helvetica')
    hold off
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
else
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% This portion of the code generates the different subfigures in
    %%% Figure 2 and the quantitative data in Table 1.

    %%% Creating a subfigure in Figure 2. For Fig 2a, use election = 
    %%% '2012g'. For Fig 2b, use '2012g'. For Fig 2c, use '2012p'. For Fig 
    %%% 2d, use '2016g'. For Fig 2e, use '2016s'. For Fig 2f, use '2016p'.
    figure
    barh(stateNamesRev,[formattedResult,modelForecast']);
    hold on
    xlim([-60 60])
    xticks(-60:10:60)
    title(figureTitle)
    xlabel('Dem margin lead to the left; Rep margin lead to the right (percentage points)')
    legend('Result','Model')
    xticklabels({'+60','+50','+40','+30','+20','+10','0','+10','+20','+30','+40','+50','+60'})
    set(gca,'FontSize',14,'FontName','Helvetica')
    hold off
 
    %%% Our model called all the states in the superstates correctly, so we
    %%% add the number of states in the superstates (numElecs -
    %%% numIndividual) and subtract 2 so we do not double count the
    %%% superstates themselves if superstates are used.
    if sum(identifyRed) + sum(identifyBlue) > 0         % superstates are used
        disp(['Our model success (Table 1) for the ' figureTitle ' is ' num2str(100*(modelPerformanceCalling - 2 + numElecs - numIndividual)/numElecs) '%.'])
    else
        disp(['Our model success (Table 1) for the ' figureTitle ' is ' num2str(100*(modelPerformanceCalling)/numElecs) '%.'])
    end
    
    %%% We compare our forecasts to those of Sabato's Crystal Ball and
    %%% FiveThirtyEight (when available) by calulating the fraction of
    %%% states called correctly by party. Note that for the 2016 and 2012
    %%% elections, columsn 7 and 8 in our stateNumData contain 1's if the
    %%% election was called correctly and 0 if it was called incorrectly.
    %%% If a prediction is not available from FiveThirtyEight, the column
    %%% is empty.
    calledCorrectly538 = sum(stateNumData(:,6));
    calledCorrectlySabato = sum(stateNumData(:,7));
    
    %%% We have predictions from 538 only for the 2016 senate, 2016
    %%% president, and 2012 president races.
    if strcmp(election,'2016s') == 1 || strcmp(election,'2016p') == 1 || strcmp(election, '2012p') == 1
        disp(['In comparison, FiveThirtyEight had a success rate (Table 1) for the ' figureTitle ' of ' num2str(100*calledCorrectly538/numElecs) '%.'])
        disp(['Sabatos Crystal Ball had a success rate (Table 1) for the ' figureTitle ' of ' num2str(100*calledCorrectlySabato/numElecs) '%.'])
    elseif strcmp(election, '2012s') == 1
        %%% Special case due to 3 states without polling data being called
        %%% correctly
        disp(['Sabatos Crystal Ball had a success rate (Table 1) for the ' figureTitle ' of ' num2str(100*(calledCorrectlySabato+3)/numElecs) '%.'])
    else
        disp(['Sabatos Crystal Ball had a success rate (Table 1) for the ' figureTitle ' of ' num2str(100*calledCorrectlySabato/numElecs) '%.'])
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
end     % end of if-else statement to handle different elections

end     % end of main function



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Helper function for calculating the number of electoral votes for Trump
%%% and Clinton in the 2016 presidential race. We use this to reproduce 
%%% Figure 3. In the unlikely case that a state has equal Republican and 
%%% Democrat votes, we give half of that state's electoral votes to each 
%%% party.
function [repElectoral,demElectoral] = electoralVotes(RepFrac,undecidedFrac)
    
    %%% electoralVotesPerState lists the number of electoral votes per
    %%% superstate and state in our usual order (Red, Blue, followed by
    %%% individual states forecast listed alphabetically)
    electoralVotesPerState = [191,191,9,29,6,16,10,6,4,15,18,20,13,10];
    
    RepWinner = logical(RepFrac > (1-undecidedFrac-RepFrac));
    tied = logical(RepFrac == (1-undecidedFrac-RepFrac));
    repElectoral = sum(electoralVotesPerState.*RepWinner' + 0.5*electoralVotesPerState.*tied');
    demElectoral = 538 - repElectoral;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
