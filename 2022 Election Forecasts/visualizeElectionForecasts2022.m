%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Purpose: Create some of our visulations (Senate seat outcome histogram,
%%% bar graphs with vote margins, and alt text) of the 2022 election 
%%% results as shown at https://c-r-u-d.gitlab.io/2022/
%%%
%%% Command used to visualize our election forecasts:
%%% visualizeElectionForecasts2022(folderName, election, date)
%%%
%%% Input:
%%% election is the election year and month, date is the date of data 
%%% collection
%%%
%%% Output:
%%% Visualizations of our election forecasts
%%%
%%% To run, for example:
%%% visualizeElectionForecasts2022('2022s_8', '07-24-2022')
%%%
%%% 2022
%%%
%%% 2022 authors: Ryan Branstetter, Mengqi Liu, Manas Paranjape, and
%%% Alexandria Volkening (avolkening@purdue.edu).
%%%
%%% Adapted from code by 2020 authors: William L. He, Samuel Chian, 
%%% Christopher M. Lee, and Alexandria Volkening.
%%%
%%% Note: Check the names of folders when reading and writing data below
%%% (you may need to change these based on your naming conventions and
%%% folder structure)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [] = visualizeElectionForecasts2022(election, date)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Some initial processing for figure titles
%%% The election type is either s or g (for senatorial or gubernatorial)
electionType = election(5);
if strcmp(electionType, 's') == 1       % senatorial
    electionTitle = [election(1:4) ' Forecasts on ' date];
    %%% Accessing the states and superstates taht we forecast for this
    %%% election.
    electionData = readtable(['./ModelDataFiles/dataset_correlations' election '_' date '.xlsx']);
elseif strcmp(electionType, 'g') == 1       % gubernatorial
    electionTitle = [election(1:4) ' Forecasts on ' date];
    %%% Accessing the states and superstates taht we forecast for this
    %%% election.
    electionData = readtable(['./ModelDataFiles/dataset_correlations' election '_' date '.xlsx']);
else
    error('Please make sure that your election variable is correct.')
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Loading results (note that finalR is an nrand-long matrix
%%% containing the final Republican fractions that we forecast;
%%% finalS is an nrand-long matrix containing the final other fractions
%%% that we forecast; nrand is the number of simulated elections,
%%% usually 10,000; and you can find the Democratic fractions by using
%%% that finalD = 1 - finalR - finalS)
load(['./ModelDataFiles/Results' election '_' date],'finalR','finalS','nrand')

%%% Pulling out the specific data that is useful for image processing
stateNames = table2cell(electionData(:,1));
stateAbbrev = table2cell(electionData(:,2));
numM = length(stateAbbrev);      % number of regions forecast

%%% Adding spaces into the state names as appropriate and designating
%%% special elections with an asterisk
stateNames = strrep(stateNames, 'DistrictOfColumbia', 'District of Columbia');
stateNames = strrep(stateNames, 'NewHampshire', 'New Hampshire');
stateNames = strrep(stateNames, 'NorthDakota', 'North Dakota');
stateNames = strrep(stateNames, 'NewJersey', 'New Jersey');
stateNames = strrep(stateNames, 'NewMexico', 'New Mexico');
stateNames = strrep(stateNames, 'NewYork', 'New York');
stateNames = strrep(stateNames, 'NorthCarolina', 'North Carolina');
stateNames = strrep(stateNames, 'SouthCarolina', 'South Carolina');
stateNames = strrep(stateNames, 'SouthDakota', 'South Dakota');
stateNames = strrep(stateNames, 'RhodeIsland', 'Rhode Island');
stateNames = strrep(stateNames, 'WestVirginia', 'West Virginia');
stateNames = strrep(stateNames, 'Special', '*');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Calculating the endpoints of the range in which the middle 80 percent
%%% of our simulate elections lie, as well as our forecast vote margins
percent10 = prctile((finalR - 1 + finalS + finalR)*100, 10, 2);
percent90 = prctile((finalR - 1 + finalS + finalR)*100, 90, 2);
finalD = 1 - finalR - finalS;
modelForecast = 100*mean(finalR - finalD,2);        % vote margin is positive if a Republican victory and negative if a Democratic victory
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Creating vote-margin bar graphs with 80% confidence intervals
figure('units','normalized','outerposition',[0 0 1 1])
modelForecastDem = modelForecast;
modelForecastDem(modelForecast > 0) = 0;
modelForecastRep = modelForecast;
modelForecastRep(modelForecast < 0) = 0;
bh = barh(1:numM, flipud(modelForecastRep), 'FaceColor', [236/255,65/255,37/255], 'EdgeColor','none');
bh.FaceColor = 'flat';
hold on
barh(1:numM, flipud(modelForecastDem), 'FaceColor',  [20/255,49/255,244/255], 'EdgeColor','none');
for i = 1:numM
    plot([percent10(i), percent90(i)],[numM-i+1, numM-i+1],'Color', [247/255 204/255 73/255], 'LineWidth',3)
    hold on
end
tickCutOff = ceil(max(abs([percent10;percent90]))/10)*10;
xlim([-tickCutOff tickCutOff])
xticks(-tickCutOff:10:tickCutOff)
title(electionTitle)
legend({'Forecast (Rep. win)','Forecast Dem. win)', '80% of results'}, 'Location','southeast', 'FontSize', 24)
xlabel('Margin lead by party (percentage points)')
ticklabels = get(gca,'XTickLabel');
xticklabels_new = cell(size(ticklabels));
for i = 1:(length(xticklabels)-1)/2
    xticklabels_new{i} = ['\color{blue} +' ticklabels{length(xticklabels)-i+1}];
    xticklabels_new{length(xticklabels)-i+1} = ['\color{red} +' ticklabels{length(xticklabels)-i+1}];
end
xticklabels_new{(length(xticklabels)-1)/2 + 1} = ['\color{black}' abs(ticklabels{(length(xticklabels)-1)/2 + 1})];
set(gca, 'XTickLabel', xticklabels_new);
yticks(1:1:numM)
box('off')
set(groot,{'DefaultAxesXColor','DefaultAxesYColor','DefaultAxesZColor'},{'k','k','k'})
yticklabels(flipud(stateNames))
set(gca,'FontSize',26,'FontName','Helvetica', 'LineWidth', 3)
saveas(gcf, ['./ModelOutput/voteMargins' election(1:5) '_' date '.fig'])
saveas(gcf, ['./ModelOutput/voteMargins' election(1:5) '_' date '.png'])
hold off
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Creating an alt text file to accompany bar graphs
altText = "Description of bar graph diagram showing vote margins:";
stateNames = strrep(stateNames, 'Red', 'the Red superstate');
stateNames = strrep(stateNames, 'Blue', 'the Blue superstate');

%%% Looping over regions forecast
for i = 1:numM
    if modelForecast(i) < 0
        party = "Democratic";
    else
        party = "Republican";
    end
    
    %%% Adding a new line to altText (each line is a region forecast
    %%% followed by the party and vote margin). For example, we might say
    %%% "We forecast a Democratic outcome in California by a margin of 10
    %%% percentage points."
    altText = altText + newline + ...
        strcat("We forecast a ", party, " outcome in ", stateNames(i), ...
        " by a margin of ", string(abs(modelForecast(i))), " percentage points.");
    
end %%% end of loop over regions forecast

%%% Writing our alt text to a file
fileID = fopen(strcat("./ModelOutput/AltText", election(1:5), "_", date, ".txt"), 'w');
fprintf(fileID, altText);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Generating sample of 30 simulations to create gub or sen wheel
%%% on website (feeds into electionWheel2022.r) if forecasting the
%%% 2022 elections. Note: To do this, we need to assign every
%%% state that we forecast (including those in the superstates) its
%%% forecast margin
if strcmp(election(1:5), '2022g') == 1 || strcmp(election(1:5), '2022s') == 1
    sampleWheel(election, date);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Plotting distribution of Senate seat outcomes
if strcmp(electionType, 's') == 1
    [chanceRepWinS, chanceDemWinS] = SenateOutcomeDistribution(election, date);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Generating number of pie slices to color red or blue for the Senate
%%% 2022 website if forecasting the 2022 senatorial elections. Note there
%%% are 24 total pie slices in our plot
if strcmp(election(1:5), '2022s') == 1
    numDemPieSlices = round(24*chanceDemWinS/100);
    numRepPieSlices = round(24*chanceRepWinS/100);
    
    altText = strcat("Pie chart shown with ", num2str(numDemPieSlices), " slices in blue and ", ...
        num2str(numRepPieSlices), " slices in red.");
    
    %%% Writing our associated alt text to a file
    fileID = fopen(strcat("./ModelOutput/AltTextPie", election(1:5), "_", date, ".txt"), 'w');
    fprintf(fileID, altText);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


end     % end of main function


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Helper function for generating an excel document with a sample of 30
%%% simulations (for the 2022 election) to use in the 2022 gub or sen
%%% wheels(every election taking place must be assined its
%%% margin; for states without an election, we assign them 1000; for any
%%% individual ("U") states that we do not forecast, we assign them a zero
%%% margin. Note that this code assumes there is a red superstate present
%%% and a blue superstate present.
function [] = sampleWheel(election, dateOfData)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Reading in the file with model results and setting some variables
load(['./ModelDataFiles/Results' election '_' dateOfData],'finalR','finalS', 'nrand')
finalD = 1 - finalR - finalS;

%%% Sampling 30 elections uniformly at random to plot
randomSimToSample = randsample(nrand,30);
marginSample = 100*(finalR(:,randomSimToSample) - finalD(:,randomSimToSample));

%%% Accessing the states and superstates that we forecast for this election
electionData = readtable(['./ModelDataFiles/dataset_correlations' election '_' dateOfData '.xlsx']);

%%% Pulling out the specific data that is useful for image processing
stateAbbrev = table2cell(electionData(:,2));

%%% Gathering index data to determine states within superstates, as well as
%%% check if any individual states are missing forecasts
indexData = readtable('./dataset_index.xlsx','Sheet',election(1:5),'Range','A:J');
allGubAbbrevs = table2cell(indexData(:,2));        % abbreviated names
allGubColors = table2cell(indexData(:,3));         % race colors (R for Red superstate, B for Blue superstate, U for swing/individually forecast)

%%% Defining full list of all 50 states and DC
allStates = {'AL';'AK';'AZ';'AR';'CA';'CO';'CT';'DE';'DC';'FL';'GA';'HI';'ID';'IL';'IN';'IA';'KS';'KY';'LA';'ME';'MD';'MA';'MI';'MN';'MS';'MO';'MT';'NE';'NV';'NH';'NJ';'NM';'NY';'NC';'ND';'OH';'OK';'OR';'PA';'RI';'SC';'SD';'TN';'TX';'UT';'VT';'VA';'WA';'WV';'WI';'WY'};
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Assigning margins to all states and the District of Columbia
allMargins = zeros(51,30);
for statei = 1:51

    if sum(cellfun(@(x) strcmp(x, allStates{statei}), allGubAbbrevs, 'UniformOutput', 1)) == 0
        %%%$ state does not have an election
        allMargins(statei,:) = 1000*ones(1,30);
    elseif strcmp(allGubColors{cellfun(@(x) strcmp(x, allStates{statei}), allGubAbbrevs, 'UniformOutput', 1)}, 'R') == 1
        %%% statei is a member of the Red superstate
        allMargins(statei,:) = marginSample(1,:);
    elseif strcmp(allGubColors{cellfun(@(x) strcmp(x, allStates{statei}), allGubAbbrevs, 'UniformOutput', 1)}, 'B') == 1
        %%% statei is a member of the Blue superstate
        allMargins(statei,:) = marginSample(2,:);
    elseif strcmp(allGubColors{cellfun(@(x) strcmp(x, allStates{statei}), allGubAbbrevs, 'UniformOutput', 1)}, 'U') == 1
        
        %%% statei is forecast indivividually
        if isempty(find(strcmp(stateAbbrev, allStates(statei)))) ~= 1
            %%% statei has data and is forecast (we found it in
            %%% stateAbbrev)
            allMargins(statei,:) = marginSample(find(strcmp(stateAbbrev, allStates(statei))),:);
        elseif isempty(find(strcmp(allGubAbbrevs, allStates(statei)))) ~= 1
            %%% statei has an election but is not forecast (e.g., due to
            %%% missing data)
            allMargins(statei,:) = zeros(1,30);
        else
            
        end
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Writing this data to a file for use in electionWheel2022.r
allMargins = [(1:51)',allMargins];      % appending 1 to 50 in front
dlmwrite(['./ModelDataFiles/prelimInputWheel' election '_' dateOfData '.csv'], allMargins, 'delimiter', ',', 'precision', 4)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end %%% end of helper function for visualizing Senate seat outcomes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Helper function for plotting the distribution of Senate seat outcomes
%%% (includes associated alt text file). Produces a histogram with the odds
%%% of each outcome of the Senate by seats for each party
function [chanceRepWinS,chanceDemWinS] = SenateOutcomeDistribution(election, dateOfData)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Reading in the file with model results and setting some variables
load(['./ModelDataFiles/Results' election '_' dateOfData],'finalR','finalS', 'nrand')
finalD = 1 - finalR - finalS;
RepSeatShare = zeros(1,100);

%%% Accessing the states and superstates that we forecast for this
%%% election and their Senate seats
electionData = readtable(['./ModelDataFiles/dataset_correlations' election '_' dateOfData '.xlsx']);

%%% Pulling out the specific data that is useful for image processing
seats = cell2mat(table2cell(electionData(:,8)));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Specifying the seats that are not up for election in 2022
DemSeats = 36;      % includes 2 Independent seats
RepSeats = 29;      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Computing number of individually forecast seats that are missing
%%% polling data and not forecast
numMissing = 100 - DemSeats - RepSeats - sum(seats);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Computing the Republican seats forecast per simulated election
RepSeatsSim = RepSeats + sum((finalD < finalR).*seats) + numMissing*randi([0,1],[1,nrand]);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Binning different outcomes
for ii = 1:100
    RepSeatShare(ii) = sum(RepSeatsSim == ii);  % chance of ii number of Republican seats (and 100-ii Democratic seats)
end

%%% Calculating chance of Rep control
RepControl = (sum(RepSeatShare > 50))/nrand;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Creating histogram and associated alt text
figure('units','normalized','outerposition',[0 0 1 1])
set(groot,{'DefaultAxesXColor','DefaultAxesYColor','DefaultAxesZColor'},{'k','k','k'})
hold on
bar(1:50, 100*RepSeatShare(1:50)/nrand, 'FaceColor',  [20/255,49/255,244/255], 'EdgeColor','none')      % Blue for Democratic outcome
bar(51:100, 100*RepSeatShare(51:100)/nrand, 'FaceColor', [236/255,65/255,37/255], 'EdgeColor','none')   % Red for Republican outcome
xlim([25 75])
xticks([25 30 35 40 45 50 55 60 65 70 75])
ticklabels = get(gca,'XTickLabel');
xticklabels_new = cell(size(ticklabels));

%%% Making the tick labels red or blue (for Rep or Dem victories)
for i = 1:(length(xticklabels)-1)/2
    xticklabels_new{i} = ['\color{blue}' ticklabels{length(xticklabels)-i+1}];
    xticklabels_new{length(xticklabels)-i+1} = ['\color{red}' ticklabels{length(xticklabels)-i+1}];
end
xticklabels_new{(length(xticklabels)-1)/2 + 1} = ['\color{black}' abs(ticklabels{(length(xticklabels)-1)/2 + 1})];
set(gca, 'XTickLabel', xticklabels_new);
ylim([0 100*(max(RepSeatShare)/10000+.03)])
title(['Forecast of ' election(1:4) ' Senatorial Seats on ' dateOfData])
xlabel('Seats by party')
box('off')
ylabel('Chance of this outcome (%)') 
set(gca,'FontSize', 28, 'FontName', 'Helvetica','LineWidth',3)
legend(['Democrats maintain control: ' num2str(100*(1-RepControl)) '%'], ...
    ['Republicans gain control: ' num2str(100*RepControl) '%']);
saveas(gcf, ['./ModelOutput/senDistribution' election(1:5) '_' date '.fig'])
saveas(gcf, ['./ModelOutput/senDistribution' election(1:5) '_' date '.png'])

%%% Writing the associated alt text file
altText = "Histogram showing the distribution of outcomes for Senate seats:";
alText = altText + newline + strcat("Probability Democrats will maintain control of the Senate: ", num2str(100*(1-RepControl)), '%');
alText = altText + newline + strcat("Probability Republicans will gain control of the Senate: ", num2str(100*RepControl), '%');
fileID = fopen(strcat("./ModelOutput/AltTextSenDistribution_", election, "_", dateOfData, ".txt"), 'w');
fprintf(fileID, altText);

chanceDemWinS = 100*(1-RepControl);
chanceRepWinS = 100*RepControl;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end %%% end of helper function for visualizing Senate seat outcomes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

