Code and data for forecasting the 2022 U.S. senatorial and gubernatorial elections, reproducing or building on the forecasts posted at https://c-r-u-d.gitlab.io/2022/.

This folder contains updated 2022-election code (with 2020 options) that implements the compartmental model introduced in "Forecasting elections using compartmental models of infection" by Alexandria Volkening, Daniel F. Linder, Mason A. Porter, and Grzegorz A. Rempala (2020), in _SIAM Review_, Vol. 62, No. 4: 837-865 (https://epubs.siam.org/doi/abs/10.1137/19M1306658). Our updated programs that apply these methods to the 2020 U.S. elections are with additional collaborators Samuel Chian, William L. He, and Christopher M. Lee. Our updated programs that apply these methods to the 2022 U.S. elections are with additional collaborators Ryan T. Bransteter, Mengqi Liu, and Manas Paranjape.

For questions or comments, please contact Alexandria Volkening at avolkening@purdue.edu.

To reproduce our forecasts of the 2022 elections, follow these steps:

(1) Download the appropriate polling data set (gubernatorial, senatorial, or presidential) from FiveThirtyEight at https://projects.fivethirtyeight.com/polls/. FiveThirtyEight aggregates polling data and their data is publicly available under a CC BY 4.0 license (https://creativecommons.org/licenses/by/4.0/). Save this data with the date of download: for example, if you download the data on 24 July 2022, save the data as dataset_gubPolls_07-24-2022.xlsx and dataset_senPolls_07-24-2022.xlsx in a folder called 'PollingData'.

(2) Format the polling data (sort the polls by state, take the mean of the polls by month, etc) to obtain one data point (representing the percent of people inclined to vote for a Democrat, inclined to vote for a Republican, and undecided/other) for each 30-day interval spanning backward from the election day. To format the polling data, for the 2022 gubernatorial elections with polling data downloaded on 24 July, for example, use: 
[populations, regions, numMonths, numRegions] = formatting538polls_2022('dataset_gubPolls_07-24-2022','07/24/22')
in Matlab. This produces one Excel document, containing the data we will use to correlate election outcomes later, and two csv files, containing the formatted polling data (all should be saved in a folder called 'ModelDataFiles', which you may need to create first).

(3) Run parameterFitting2022.R to determine our model parameters. First adjust time (at line 51) to the appropriate value (time equals numMonths output in step (2)) and date (at line 53) to the appropriate value (e.g., "07-24" in our example). Uncomment the appropriate lines from line 70 to 85 in the code, depending on if you are forecasting the 2022 senatorial or gubernatorial elections. Replace Pop and stateNames with the values output in step (2) for the "populations" and "regions" variable as necessary. Replace numM with value of "numRegions" from the last step. The result of this step is one csv file, containing the model parameters, saved in the folder 'ModelDataFiles'.

(4) Run electionModel2022.m to simulate our model of election dynamics. First change numMonths at line 80 and dateAndTime at line 81 as necessary. (In our example, numMonths = 8 and dateAndTime = '07-24-2022'). To forecast the gubernatorial elections, run:
electionModel2022(308,10000,0.0015,'2022g')
in Matlab. Change the final input to '2022s' to forecast the senatorial elections. This produces one .mat file containing the results of 10,000 simulated stochastic elections (saved in the folder 'ModelDataFiles').

(5) Run visualizeElectionForecasts2022.m to produce some images and alt text showing our forecasts (like those on our website here: https://c-r-u-d.gitlab.io/2022/). For example, to show forecasts of the gubernatorial elections in our current example, run:
visualizeElectionForecasts2022('2022g_8', '07-24-2022')
in Matlab. The results will be saved in 'ModelOutput' (you may need to create this folder first). electionWheel.R can be run to produce 30 US map plots of 30 randomly sampled election outcomes from our results (this requires specifying numM (the number of months; 8 in our example), date (e.g., "07-24-2022"), and election (e.g., "2022g") at line 42 in the code).

Steps (1)-(5) must be done in order. Note that Steps (1), (2), and (5) are quick (< 5 minutes). Step (3) can take between about 8 hours and about 3 days, depending on how many states are forecast individually. Step (4) takes a few hours. You many need to create the folders 'ModelOutput' and 'ModelDataFiles' in the folder containing the code that you download here, as these folders will be filled with results from steps (2)-(5).

See https://gitlab.com/alexandriavolkening/forecasting-elections-using-compartmental-models/-/tree/master/Original%20Programs for the programs that implement the same model to forecast the 2012-2018 U.S. elections and https://gitlab.com/alexandriavolkening/forecasting-elections-using-compartmental-models/-/tree/master/2020%20Election%20Forecasts for the programs that implement the model for the 2020 U.S. elections (some of this code is also included in the current 2022 code). 

