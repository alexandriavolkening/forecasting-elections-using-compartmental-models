%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Program for simulating the SDE and ODE models introduced in 
%%% "Forecasting elections using compartmental  models of infection" by 
%%% Alexandria Volkening, Daniel F. Linder, Mason A. Porter, and Grzegorz 
%%% A. Rempala (SIAM Review, 2022, 
%%% https://epubs.siam.org/doi/10.1137/19M1306658), adapted
%%% for the 2022 U.S. elections.
%%%
%%% 2022 authors: Ryan Branstetter, Mengqi Liu, Manas Paranjape, and
%%% Alexandria Volkening
%%%
%%% 2020 authors: Samuel Chian, William L. He, Christopher M. Lee,
%%% Alexandria Volkening, Daniel F. Linder, Mason A. Porter, and 
%%% Grzegorz A. Rempala
%%%
%%% Contact: avolkening@purdue.edu
%%%
%%% If you use this code, please cite the reference above, cite the
%%% GitLab repository https://c-r-u-d.gitlab.io/2022/
%%% and https://gitlab.com/alexandriavolkening/forecasting-elections-using-compartmental-models
%%%
%%% Note: Check the names of folders when reading and writing data below
%%% (you may need to change these based on your naming conventions and
%%% folder structure)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Purpose: This code simulates elections using parameters fit to polling
%%% data (through the parameterFitting.R or the parameterFitting2022.R code). 
%%% Note that this code relies on polling data and initial conditions 
%%% formatted with formattingPollData.m or formatting538polls_2020.R or
%%% formatting538polls_2022.R and parameters generated with 
%%% parameterFitting.R or parameterFitting2020.R or parameterFitting2022.R.
%%%
%%% For 2012-2018 elections, see the "Original Programs" subfolder at 
%%% https://gitlab.com/alexandriavolkening/forecasting-elections-using-compartmental-models
%%% for the remaining files and directions needed to run this code.
%%%
%%% For the 2020 elections, see the "2020 Election Forecasts" subfolder at
%%% https://gitlab.com/alexandriavolkening/forecasting-elections-using-compartmental-models
%%% for the remaining files and directions needed to run this code.
%%%
%%% For the 2022 elections, see the "2022 Election Forecasts" subfolder at
%%% https://gitlab.com/alexandriavolkening/forecasting-elections-using-compartmental-models
%%% for the remaining files and directions needed to run this code.
%%%
%%% Before running this program to simulate elections, first run the 
%%% appropriate formattingPollData and parameterFitting to generate the
%%% files (initial conditions and model parameters) called by this code. 
%%%
%%% Commands used to simulate each election:
%%%
%%% 2022 Senate elections:
%%% electionModel2022(308,10000,0.0015,'2022s')
%%%
%%% 2022 Goverorner elections:
%%% electionModel2022(308,10000,0.0015,'2022g')
%%%
%%% 2020 president elections:
%%% electionModel2020(303,10000,0.0015,'2020p')
%%%
%%% 2020 Senate elections:
%%% electionModel2020(303,10000,0.0015,'2020s')
%%%
%%% 2020 governor elections:
%%% electionModel2020(303,10000,0.0015,'2020g')
%%%
%%% 2018 Senate elections:
%%% electionModel(306,10000,0.0015,'2018s')       % To forecast the 2018 Senate Elections in July, follow the directions in reproducingDataFiguresTables.txt to adjust the code. 
%%% 
%%% 2018 governor elections:
%%% electionModel(306,10000,0.0015,'2018g')
%%%
%%% 2016 Senate elections:
%%% electionModel(308,1,0,'2016s')
%%%
%%% 2016 governor elections:
%%% electionModel(308,1,0,'2016g')
%%%
%%% 2016 president elections:
%%% electionModel(308,1,0,'2016p')                % without noise
%%% electionModel(308,10000,0.0015,'2016p')       % with noise
%%%
%%% 2012 Senate elections:
%%% electionModel(306,1,0,'2012s')
%%%
%%% 2012 governor elections:
%%% electionModel(306,1,0,'2012g')
%%%
%%% 2012 president elections:
%%% electionModel(306,1,0,'2012p')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [] = electionModel2022(time, nrand, sigma, election)

%%% Here we define the number of months used to produce the forecast (this
%%% is only used in 2020 and 2022)
numMonths = 9;
dateAndTime = '08-24-2022';  % date of data collection

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Preliminaries. Establishing folder and file names to save results.
folderName = 'ModelDataFiles';  % name of folder (within the current folder) that results will be saved in
mkdir(folderName);              % creating the folder
dt = 0.1;                       % time step (in days) for ODEs and SDEs
electionYT = election(1:5);     % just the election year and type (p, s, or g) for president, senator, or governor (e.g., '2020p')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% numM is the number of states and superstates used to simulate a given
%%% election. For the 2020 and 2022 elections, we determine this from the
%%% correlation data files.
if strcmp(electionYT, '2018s') == 1      % 2018 Senate election
    numM = 16;          % If forecasting the Senate races on July 8, replace this with numM = 15;
elseif strcmp(electionYT, '2018g') == 1  % 2018 governor election
    numM = 15;
elseif strcmp(electionYT, '2016s') == 1 || strcmp(election, '2016s_demo') == 1  % 2016 Senate election
    numM = 14;
elseif strcmp(electionYT, '2016g') == 1   % 2016 governor election
    numM = 12;
elseif strcmp(electionYT, '2016p') == 1   % 2016 president election
    numM = 14;
elseif strcmp(electionYT, '2012s') == 1   % 2012 Senate election
    numM = 15;
elseif strcmp(electionYT, '2012g') == 1   % 2012 governor election
    numM = 9;
elseif strcmp(electionYT, '2012p') == 1   % 2012 president election
    numM = 14;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Setting up election-specific variables: population sizes, and and 
%%% some demographic information (used to correlate noise in some
%%% settings). Note that in all cases but '2016s_demo', running this code
%%% requires first running formattingPollData.m or formattingPollData2020.m
%%% or formattingPollData2022.m and then parameterFitting.R or 
%%% parameterFitting2020.R or parameterFittig2022.R
if strcmp(electionYT, '2022s') == 1     % 2022 Senate election
    
    %%% Formatting demographic data for when correlated noise is used to
    %%% simulate 2022 Senate elections. We obtain this
    %%% data from Annual Estimates of the Resident Population by Sex, Race, 
    %%% and Hispanic Origin: April 1, 2020 to July 1, 2021. Source: U.S. 
    %%% Census Bureau, 2022. Accessed: 07-24-2022, We obtain the percent 
    %%% college data from Data on education (percent Bachelor's Degree or
    %%% Higher by State) for 2020, FRED Economic Data, Federal Reserve Bank
    %%% of St. Louis. Accessed: 07-24-2022
    
    %%% If forecasting before July 2022 it might be necessary to remove the 
    %%% "Red" and "Blue" superstates from the correlations file
    corrData = readtable(['./ModelDataFiles/dataset_correlations'  electionYT '_' num2str(numMonths) '_' dateAndTime '.xlsx']);
    numM = size(corrData,1);
    total = table2array(corrData(:,3)) + table2array(corrData(:,5));
    HispanicPercent = table2array(corrData(:,5))./total;
    NHBlackPercent = table2array(corrData(:,4))./total;
    woCollegePercent = (100-table2array(corrData(:,6)))/100;

    %%% Number of voters per state or superstate.
    NN = table2array(corrData(:,7))';
    
elseif strcmp(electionYT, '2022g') == 1
    
    %%% Formatting demographic data for when correlated noise is used to
    %%% simulate 2022 Senate elections. We obtain this
    %%% data from Annual Estimates of the Resident Population by Sex, Race, 
    %%% and Hispanic Origin: April 1, 2020 to July 1, 2021. Source: U.S. 
    %%% Census Bureau, 2022. Accessed: 07-24-2022, We obtain the percent 
    %%% college data from Data on education (percent Bachelor's Degree or
    %%% Higher by State) for 2020, FRED Economic Data, Federal Reserve Bank
    %%% of St. Louis. Accessed: 07-24-2022
    corrData = readtable(['./ModelDataFiles/dataset_correlations' electionYT '_' num2str(numMonths) '_' dateAndTime '.xlsx']);
    numM = size(corrData,1);
    total = table2array(corrData(:,3)) + table2array(corrData(:,5));
    HispanicPercent = table2array(corrData(:,5))./total;
    NHBlackPercent = table2array(corrData(:,4))./total;
    woCollegePercent = (100-table2array(corrData(:,6)))/100;

    %%% Number of voters per state or superstate.
    NN = table2array(corrData(:,7))';
    
elseif strcmp(electionYT, '2020p') == 1   % 2020 president election

    %%% Formatting demographic data for when correlated noise is used to
    %%% simulate 2020 president forecasts. We obtain this
    %%% data from Annual Estimates of the Resident Population by Sex, Race, 
    %%% and Hispanic Origin: April 1, 2010 to July 1, 2019. Source: U.S. 
    %%% Census Bureau, 2020. Accessed: 26-07-2020, 2018 American Community 
    %%% Survey 1-Year Estimates: Table R1502: Percent Of People 25 Years 
    %%% And Over Who Have Completed A Bachelor's Degree. Source: U.S. 
    %%% Census Bureau. Accessed: 26-07-2020.
    corrData = readtable(['./ModelDataFiles/dataset_correlations' electionYT '_' num2str(numMonths) '_' dateAndTime '.xlsx']);
    numM = size(corrData,1);
    total = table2array(corrData(:,3)) + table2array(corrData(:,5));
    HispanicPercent = table2array(corrData(:,5))./total;
    NHBlackPercent = table2array(corrData(:,4))./total;
    woCollegePercent = (100-table2array(corrData(:,6)))/100;

    %%% Number of voters per state or superstate.
    NN = table2array(corrData(:,7))';

elseif strcmp(electionYT, '2020s') == 1   % 2020 Senate election

    %%% Formatting demographic data for when correlated noise is used to
    %%% simulate 2020 president forecasts. We obtain this
    %%% data from Annual Estimates of the Resident Population by Sex, Race, 
    %%% and Hispanic Origin: April 1, 2010 to July 1, 2019. Source: U.S. 
    %%% Census Bureau, 2020. Accessed: 26-07-2020, 2018 American Community 
    %%% Survey 1-Year Estimates: Table R1502: Percent Of People 25 Years 
    %%% And Over Who Have Completed A Bachelor's Degree. Source: U.S. 
    %%% Census Bureau. Accessed: 26-07-2020.
    [demoN, ~] = xlsread(['./ModelDataFiles/dataset_correlations' electionYT '_' num2str(numMonths) '_' dateAndTime '.xlsx']);
    numM = size(demoN,1);
    total = demoN(1:numM,1) + demoN(1:numM,3);
    HispanicPercent = demoN(1:numM,3)./total;
    NHBlackPercent = demoN(1:numM,2)./total;
    woCollegePercent = (100-demoN(1:numM,4))/100;

    %%% Number of voters per state or superstate.
    NN = demoN(1:numM,5)';
    
elseif strcmp(electionYT, '2020g') == 1   % 2020 governor election

    %%% Formatting demographic data for when correlated noise is used to
    %%% simulate 2020 president forecasts. We obtain this
    %%% data from Annual Estimates of the Resident Population by Sex, Race, 
    %%% and Hispanic Origin: April 1, 2010 to July 1, 2019. Source: U.S. 
    %%% Census Bureau, 2020. Accessed: 26-07-2020, 2018 American Community 
    %%% Survey 1-Year Estimates: Table R1502: Percent Of People 25 Years 
    %%% And Over Who Have Completed A Bachelor's Degree. Source: U.S. 
    %%% Census Bureau. Accessed: 26-07-2020.
    [demoN, ~] = xlsread(['./ModelDataFiles/dataset_correlations' electionYT '_' num2str(numMonths) '_' dateAndTime '.xlsx']);
    numM = size(demoN,1);
    total = demoN(1:numM,1) + demoN(1:numM,3);
    HispanicPercent = demoN(1:numM,3)./total;
    NHBlackPercent = demoN(1:numM,2)./total;
    woCollegePercent = (100-demoN(1:numM,4))/100;

    %%% Number of voters per state or superstate.
    NN = demoN(1:numM,5)';
    
elseif strcmp(electionYT,'2018s') == 1  % 2018 Senate election
    
    %%% Formatting demographic data for correlated noise. We obtain this
    %%% data from "Annual Estimates of the Resident Population by Sex, 
    %%% Race, and Hispanic Origin for the United States, States, and 
    %%% Counties: April 1, 2010 to July 1, 2016" Source: U.S. Census 
    %%% Bureau, Population Division (https://factfinder.census.gov/faces/
    %%% tableservices/jsf/pages/productview.xhtml?pid=PEP_2015_PEPSR6H&prod
    %%% Type=table) (2017) and from Comen E, Frohlich TC, and Sauter MB: 
    %%% 247WallSt.com: "America's Most and Least Educated States: A Survey 
    %%% of All 50" (https://247wallst.com/special-report/2016/09/16/america
    %%% s-most-and- least-educated-states-a-survey-of-all-50/2/). Last 
    %%% accessed: 02-11-2018. (2016).
    [demoN, ~] = xlsread('dataset_correlationsSenate2018.xlsx');
    total = demoN(1:numM,1) + demoN(1:numM,3);
    HispanicPercent = demoN(1:numM,3)./total;
    NHBlackPercent = demoN(1:numM,2)./total;
    woCollegePercent = (100-demoN(1:numM,4))/100;

    %%% Number of voters per state or superstate. We calculate this using
    %%% data from "Estimates of the Voting Age Population for 2017. A 
    %%% Notice by the Commerce Department on 2/20/2018". Agency: Office of 
    %%% the Secretary, Commerce, Federal Register Notices, 83 (2018). 
    NN = [8603375,69220840,5382780,16782417,5093409,4277949,4730561,821604,2312576,7026626,579621,9053374,5208482,20938557,1446139,4512839];

%%% If forecasting the 2018 Senate elections on July 8, uncomment the
%%% following lines and comment lines 129-153
% if strcmp(election, '2018s') == 1  % 2018 Senate election without the Minnesota special election
%     
%     %%% Formatting demographic data for correlated noise. We obtain this
%     %%% data from "Annual Estimates of the Resident Population by Sex,
%     %%% Race, and Hispanic Origin for the United States, States, and
%     %%% Counties: April 1, 2010 to July 1, 2016" Source: U.S. Census
%     %%% Bureau, Population Division (https://factfinder.census.gov/faces/
%     %%% tableservices/jsf/pages/productview.xhtml?pid=PEP_2015_PEPSR6H&prod
%     %%% Type=table) (2017) and from Comen E, Frohlich TC, and Sauter MB:
%     %%% 247WallSt.com: "America's Most and Least Educated States: A Survey
%     %%% of All 50" (https://247wallst.com/special-report/2016/09/16/america
%     %%% s-most-and- least-educated-states-a-survey-of-all-50/2/). Last
%     %%% accessed: 02-11-2018. (2016).
%     [demoN, ~] = xlsread('dataset_correlationsSenate2018_NoMNS.xlsx');
%     total = demoN(1:numM,1) + demoN(1:numM,3);
%     HispanicPercent = demoN(1:numM,3)./total;
%     NHBlackPercent = demoN(1:numM,2)./total;
%     woCollegePercent = (100-demoN(1:numM,4))/100;
%     
%     %%% Number of voters per state or superstate. We calculate this using
%     %%% data from "Estimates of the Voting Age Population for 2017. A
%     %%% Notice by the Commerce Department on 2/20/2018". Agency: Office of
%     %%% the Secretary, Commerce, Federal Register Notices, 83 (2018).
%     NN = [8603375,69220840,5382780,16782417,5093409,4730561,821604,2312576,7026626,579621,9053374,5208482,20938557,1446139,4512839];
%     
    
elseif strcmp(electionYT, '2018g') == 1  % 2018 governor election
    
    %%% Formatting demographic data for correlated noise. We obtain this
    %%% data from "Annual Estimates of the Resident Population by Sex, 
    %%% Race, and Hispanic Origin for the United States, States, and 
    %%% Counties: April 1, 2010 to July 1, 2016" Source: U.S. Census 
    %%% Bureau, Population Division (https://factfinder.census.gov/faces/
    %%% tableservices/jsf/pages/productview.xhtml?pid=PEP_2015_PEPSR6H&prod
    %%% Type=table) (2017) and from Comen E, Frohlich TC, and Sauter MB: 
    %%% 247WallSt.com: "America's Most and Least Educated States: A Survey 
    %%% of All 50" (https://247wallst.com/special-report/2016/09/16/america
    %%% s-most-and- least-educated-states-a-survey-of-all-50/2/). Last 
    %%% accessed: 02-11-2018. (2016).
    [demoN, ~] = xlsread('dataset_correlationsGovernor2018.xlsx');
    total = demoN(1:numM,1) + demoN(1:numM,3);
    HispanicPercent = demoN(1:numM,3)./total;
    NHBlackPercent = demoN(1:numM,2)./total;
    woCollegePercent = (100-demoN(1:numM,4))/100;

    %%% Number of voters per state or superstate. We calculate this using
    %%% data from "Estimates of the Voting Age Population for 2017. A 
    %%% Notice by the Commerce Department on 2/20/2018". Agency: Office of 
    %%% the Secretary, Commerce, Federal Register Notices, 83 (2018). 
    NN = [56473242,86200292,554867,2844358,16782417,7914681,2413764,2200585,1083273,2312576,9053374,2971579,3269157,654810,4512839];

elseif strcmp(electionYT, '2016s') == 1   || strcmp(election, '2016s_demo') == 1% 2016 Senate election

    %%% Number of voters per state or superstate. We calculate this using
    %%% data from "Estimates of the Voting Age Population for 2016. A 
    %%% Notice by the Commerce Department on 1/30/2017". Agency: Office of 
    %%% the Secretary, Commerce, Federal Register Notices, 82 (2017). 
    NN = [33860361,37844627,5299579,16465727,9875430,5057601,3567717,4706137,2262631,1074207,7848068,9002201,10109422,4491015];

elseif strcmp(electionYT, '2016g') == 1   % 2016 governor election

    %%% Number of voters per state or superstate. We calculate this using
    %%% data from "Estimates of the Voting Age Population for 2016. A 
    %%% Notice by the Commerce Department on 1/30/2017". Agency: Office of 
    %%% the Secretary, Commerce, Federal Register Notices, 82 (2017). 
    NN = [747791,5057601,4706137,814909,1074207,7848068,581641,3224738,2129444,506066,5658502,1456034]; 
    
elseif strcmp(electionYT, '2016p') == 1   % 2016 president election

    %%% Formatting demographic data for when correlated noise is used to
    %%% simulate 2016 president forecasts. We obtain this
    %%% data from "Annual Estimates of the Resident Population by Sex, 
    %%% Race, and Hispanic Origin for the United States, States, and 
    %%% Counties: April 1, 2010 to July 1, 2016" Source: U.S. Census 
    %%% Bureau, Population Division (https://factfinder.census.gov/faces/
    %%% tableservices/jsf/pages/productview.xhtml?pid=PEP_2015_PEPSR6H&prod
    %%% Type=table) (2017) and from Comen E, Frohlich TC, and Sauter MB: 
    %%% 247WallSt.com: "America's Most and Least Educated States: A Survey 
    %%% of All 50" (https://247wallst.com/special-report/2016/09/16/america
    %%% s-most-and- least-educated-states-a-survey-of-all-50/2/). Last 
    %%% accessed: 02-11-2018. (2016).
    [demoN, ~] = xlsread('dataset_correlationsPresident2016.xlsx');
    total = demoN(1:numM,1) + demoN(1:numM,3);
    HispanicPercent = demoN(1:numM,3)./total;
    NHBlackPercent = demoN(1:numM,2)./total;
    woCollegePercent = (100-demoN(1:numM,4))/100;

    %%% Number of voters per state or superstate. We calculate this using
    %%% data from "Estimates of the Voting Age Population for 2016. A 
    %%% Notice by the Commerce Department on 1/30/2017". Agency: Office of 
    %%% the Secretary, Commerce, Federal Register Notices, 82 (2017). 
    NN = [82223613, 90814662, 4279173, 16465727, 2403962, 7737243, 4231619, 2262631, 1074207, 7848068, 9002201, 10109422, 6541685, 4491015];

elseif strcmp(electionYT, '2012s') == 1   % 2012 Senate election

    %%% Number of voters per state or superstate. We calculate this using
    %%% data from "Estimates of the Voting Age Population for 2012. A 
    %%% Notice by the Commerce Department on 1/30/2013". Agency: Office of 
    %%% the Secretary, Commerce, Federal Register Notices, 78 (2013). 
    NN = [30075741,78196443,4932361,2796789,15315088,4945857,5244729,4618513,783161,2095348,545020,8880551,10024150,6329130,4408841];
    
elseif strcmp(electionYT, '2012g') == 1   % 2012 governor election

    %%% Number of voters per state or superstate. We calculate this using
    %%% data from "Estimates of the Voting Age Population for 2012. A 
    %%% Notice by the Commerce Department on 1/30/2013". Agency: Office of 
    %%% the Secretary, Commerce, Federal Register Notices, 78 (2013). 
    NN = [4945857,4618513,783161,1045878,7465545,545020,1967315,502060,5312045]; 

elseif strcmp(electionYT, '2012p') == 1   % 2012 president election
    
    
    %%% Formatting demographic data for when correlated noise is used to
    %%% simulate 2016 president forecasts. We obtain this
    %%% data from "Annual Estimates of the Resident Population by Sex, 
    %%% Race, and Hispanic Origin for the United States, States, and 
    %%% Counties: April 1, 2010 to July 1, 2016" Source: U.S. Census 
    %%% Bureau, Population Division (https://factfinder.census.gov/faces/
    %%% tableservices/jsf/pages/productview.xhtml?pid=PEP_2015_PEPSR6H&prod
    %%% Type=table) (2017) and from Comen E, Frohlich TC, and Sauter MB: 
    %%% 247WallSt.com: "America's Most and Least Educated States: A Survey 
    %%% of All 50" (https://247wallst.com/special-report/2016/09/16/america
    %%% s-most-and- least-educated-states-a-survey-of-all-50/2/). Last 
    %%% accessed: 02-11-2018. (2016).
    %%% Note on Jan 31 2020: we added the functionality to use correlated
    %%% noise in the 2012 pres elections as well (but note that we use the
    %%% same 2016 demographic data for this)
    [demoN, ~] = xlsread('dataset_correlationsPresident2016.xlsx');
    total = demoN(1:numM,1) + demoN(1:numM,3);
    HispanicPercent = demoN(1:numM,3)./total;
    NHBlackPercent = demoN(1:numM,2)./total;
    woCollegePercent = (100-demoN(1:numM,4))/100;


    %%% Number of voters per state or superstate. We calculate this using
    %%% data from "Estimates of the Voting Age Population for 2012. A 
    %%% Notice by the Commerce Department on 1/30/2013". Agency: Office of 
    %%% the Secretary, Commerce, Federal Register Notices, 78 (2013). 
    NN = [78609279,87985204,3956224,15315088,2351233,7616490,4102991,2095348,1045878,7465545,8880551,10024150,6329130,4408841];

end

%%% Total number of voters
N = sum(NN);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Initializations. Note that S + R + D = 1 for each state, so we track
%%% only the number of Republicans and undecided/other voters per state, as
%%% the remaining voters are Democrat.
numTimeSteps = time/dt;                    % number of time steps to simulate the model for

S = zeros(numTimeSteps+1,numM,nrand);      % Undecided/other voters
R = zeros(numTimeSteps+1,numM,nrand);      % Republican voters
mu = zeros(1,numM);                        % covariance for noise
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Initial conditions - these are copied from the first line of the
%%% .csv files (['RepPercentages' election '.csv'] and
%%% ['DemPercentages' election '.csv'] generated by formattingPollData.m.
%%% Unless running election = 2016s_demo, this requires first running
%%% formattingPollData.m of formattingPollData2020.m
if strcmp(election, '2016s_demo') == 1
    formattedRepPolls = csvread(['./DemoFiles/RepPercentages' election '.csv'],0,1);
    formattedDemPolls = csvread(['./DemoFiles/DemPercentages' election '.csv'],0,1);
elseif strcmp(electionYT(1:4), '2020') || strcmp(electionYT(1:4), '2022')    % 2020 or 2022 elections
    formattedRepPolls = csvread(['./ModelDataFiles/RepPercentages' electionYT '_' num2str(numMonths) '_' dateAndTime '.csv'],0,1);
    formattedDemPolls = csvread(['./ModelDataFiles/DemPercentages' electionYT '_' num2str(numMonths) '_' dateAndTime '.csv'],0,1);
else  
    formattedRepPolls = csvread(['RepPercentages' election '.csv'],0,1);
    formattedDemPolls = csvread(['DemPercentages' election '.csv'],0,1);
end
RepIC = formattedRepPolls(1,:)/100;
DemIC = formattedDemPolls(1,:)/100;
for i = 1:nrand
    R(1,:,i) = RepIC;
    S(1,:,i) = 1 - RepIC - DemIC;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Specifying model parameters (unless running election = 2016s_demo, this 
%%% code requires first running formattingPollData.m and
%%% parameterFitting.R or formattingPollData2020.m or
%%% formattingPollData2022.m and
%%% parameterFitting2020.R)
if strcmp(election, '2016s_demo') == 1
    numbers = csvread(['./DemoFiles/modelParameters' election '.csv'],1,1);
elseif strcmp(electionYT(1:4),'2020') == 1 || strcmp(electionYT(1:4),'2022') == 1  % 2020 or 2022 elections
    numbers = csvread(['./ModelDataFiles/modelParameters' electionYT '_' num2str(numMonths) '_' dateAndTime '.csv'],1,1);
else
    numbers = csvread(['modelParameters' election '.csv'],1,1);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Specifying parameters that appear in Eqns. 3-8. Note that we convert
%%% our parameters to units of 1/day assuming 30-day months.
gamma_R = numbers(numM*2+1,1:numM)/30;          % Republican turnover
gamma_D = numbers(numM*2+2,1:numM)/30;          % Democrat turnover
beta_R = numbers(1:numM,1:numM)/30;             % Republican transmission
beta_D = numbers(numM+1:numM*2,1:numM)/30;      % Democrat transmission
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% There are two cases: simulating one election without noise or
%%% simulating nrand elections with noise
if strcmp(electionYT, '2012s') == 1  || strcmp(electionYT, '2012g') == 1 || ...
        strcmp(electionYT, '2012p') == 1 || strcmp(electionYT, '2016s') == 1 ...
        || strcmp(electionYT, '2016g') == 1  || strcmp(electionYT, '2016s_demo') == 1    % no noise

    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% This for loop simulates a single election. Note that we use that S
    %%% + D + R = 1 for each state to reduce the number of equations that
    %%% need ot be simulated to two per state.
    for time_step = 2:numTimeSteps+1
        
        [rhsS,rhsR] = electionModelRHS(NN,N,S(time_step-1,:,1),R(time_step-1,:,1),gamma_R,gamma_D,beta_R,beta_D,numM);
        S(time_step,:,1) = S(time_step-1,:,1) + rhsS*dt;
        R(time_step,:,1) = R(time_step-1,:,1) + rhsR*dt;
        
        
    end %%% end of for loop simulating one election without noise
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
else    % noise is added for the 2018-2022 elections and can be added for the 2016 presidential elections (set sigma = 0 and nrand = 1 to avoid this)
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Calculating the covariance to use for our noise in Eqns. 6-8 in the
    %%% manuscript. We correlate on the fraction of Hispanic 
    %%% individuals, the fraction of Non-Hispanic Black individuals, and
    %%% the fraction of individuals without a college education in each 
    %%% state.
    J_H = zeros(numM,numM);
    J_B = zeros(numM,numM);
    J_E = zeros(numM,numM);
    
    for i = 1:numM
        for j =1:numM
            J_H(i,j) = min(HispanicPercent(i),HispanicPercent(j))./(max(HispanicPercent(i),HispanicPercent(j)));
            J_B(i,j) = min(NHBlackPercent(i),NHBlackPercent(j))./(max(NHBlackPercent(i),NHBlackPercent(j)));
            J_E(i,j) = min(woCollegePercent(i),woCollegePercent(j))./(max(woCollegePercent(i),woCollegePercent(j)));
        end
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Main for loop used to simulate nrand elections. We use nrand =
    %%% 10,000 for the 2018 elections and 2020 elections.
    for randi = 1:nrand
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% For each simulated election, we choose one Jaccard index 
        %%% uniformly at random to use the covariance of our noise. 
        randomNum = rand(1);
        if randomNum <= 1/3
            covariance = J_E;
        elseif randomNum > 2/3
            covariance = J_B;
        else
            covariance = J_H;
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% This for loop simulates a single election. Note that we use 
        %%% that S + D + R = 1 for each state to reduce the number of 
        %%% equations that need ot be simulated to two per state.
        for time_step = 2:numTimeSteps+1
            
            %%% Correlated random noise
            dWt1 = sqrt(dt)*mvnrnd(mu,covariance);
            dWt2 = sqrt(dt)*mvnrnd(mu,covariance);
            
            %%% Uncorrelated random noise -- uncomment the two lines below to
            %%% simulate elections with uncogamma_Related random noise
            %dWt1 = sqrt(dt)*randn(1,numM);
            %dWt2 = sqrt(dt)*randn(1,numM);
            
            [rhsS,rhsR] = electionModelRHS(NN,N,S(time_step-1,:,randi),R(time_step-1,:,randi),gamma_R,gamma_D,beta_R,beta_D,numM);
            S(time_step,:,randi) = S(time_step-1,:,randi) + rhsS*dt + sigma*dWt1;
            R(time_step,:,randi) = R(time_step-1,:,randi) + rhsR*dt + sigma*dWt2;
            
            
        end %%% end of for loop simulating one election
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
    end %%% end of main for loop simulating nrand elections
    
end %%% main if loop for determining whether simulations are random or not


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Final processing to produce some images and report results in
%%% reproduceTablesFigures.m


%%% First we calculate our marginForecast on election day (the percentage 
%%% point margin lead of Republicans over Democrats) for each state or 
%%% superstate per simulation. Note that this is negative for a given state
%%% if a Democrat wins, and positive if a Republican wins (0 indicates a
%%% tie).
marginForecast = squeeze((R(numTimeSteps+1,:,:)- 1 + S(numTimeSteps+1,:,:) + R(numTimeSteps+1,:,:))*100);


%%% Here we calculate, for each state or superstate, the fraction of
%%% simulated elections in which the Republican candidate won. This is
%%% used to categorize states as "Likely Rep.", "Lean Dem.", etc.
chanceRWinState = zeros(1,numM);
chanceRTie = zeros(1,numM);
if nrand > 1
    for i = 1:numM
        
        %%% Placeholder
        temporary = marginForecast(i,:);
        
        %%% Number of simulations in which the Republican candidate won
        %%% (note you need to divide by nrand to express this as a
        %%% percent).
        chanceRWinState(i) = sum(temporary > 0);
        
        %%% Number of simulations in which the Republican and Democrat
        %%% candidates tied.
        chanceRTie(i) = sum(temporary == 0);
        
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Saving data and simulation conditions. Note that we only save the final
%%% election results rather than the whole trajectory of opinion dynamics.
finalR = squeeze(R(time/dt+1,:,:));
finalS = squeeze(S(time/dt+1,:,:));
meanR = mean(R,3);
meanD = mean(1-R-S,3);
stdR = std(R,[],3);
stdD = std(1-R-S,[],3);
save([folderName '/Results' electionYT '_' num2str(numMonths) '_' dateAndTime '.mat'], 'finalR','finalS','sigma','nrand','dt','time', 'chanceRWinState','chanceRTie',...
    'meanR', 'meanD','stdR','stdD');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


end  %%% end of main function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Helper funciton holding the righthand side of our model differential
%%% equations
function [rhsS,rhsR] = electionModelRHS(NN,N,S,R,gamma_R,gamma_D,beta_R,beta_D,numM)
    rhsS = zeros(1,numM);
    rhsR = zeros(1,numM);
    
    for i = 1:numM
        rhsS(i) = gamma_R(i)*R(i) + gamma_D(i)*(1-S(i)-R(i)) -...
            (S(i)/N)*(NN.*R)*beta_R(1:numM,i) -...
            (S(i)/N)*(NN.*(1-S-R))*beta_D(1:numM,i);
        rhsR(i) = -gamma_R(i)*R(i) + (S(i)/N)*(NN.*R)*beta_R(1:numM,i);
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


